function LoadingShow() {
    var result = KTUtil.getByTagName('body');
    if (result && result[0]) {
        KTUtil.addClass(result[0], 'page-loading');
    }
}

function LoadingHide() {
    var result = KTUtil.getByTagName('body');
    if (result && result[0]) {
        KTUtil.removeClass(result[0], 'page-loading');
    }
}

/***********************************
 *          Sortable Input         *
 ***********************************/
function sortableData(nameID) {
    // alert($('#sale_team_9-sortable').attr('data-key'));
    var UlArray = $('#'+nameID+' li').map(function(){ 
        return $(this).data('key'); 
    });

    var acc = [];
    $.each(UlArray, function(index, value) {
        acc.push(value);
    });

    var str = JSON.stringify(acc);
    var res = str.replace('[', '');
    var res = res.replace(']', '');

    var inputName = nameID.replace('-sortable', '');
    $('#'+inputName).val(res);
}

function refreshData() {
    var ulID = [];
    $.each($('ul'), function() {
        ulID.push(this.id);
    });

    var dataUl = [];
    $.each(ulID, function(index, value) {
        if (value != '') {
            sortableData(value);
        }
    });
}

function addCommas(x,showDigit = true) {
    x = x.toString().split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '.00';
    var rgx = /(\d+)(\d{3})/;

    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }

    if(showDigit)
    {
        return x1 + x2;
    }

    return x1;    
}