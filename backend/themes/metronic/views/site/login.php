<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
backend\assets\AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';
//Yii::$app->clientScript->registerCoreScript('jquery.ui');
$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<!--begin::Main-->
<div class="d-flex flex-column flex-root">
    <!--begin::Login-->
    <div class="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
        <!--begin::Aside-->
        <div class="login-aside d-flex flex-column flex-row-auto" style="background-color: #F2C98A;">
            <!--begin::Aside Top-->
            <div class="d-flex flex-column-auto flex-column pt-lg-40 pt-15">
                <!--begin::Aside header-->
                <a href="#" class="text-center mb-10">
                    <img src="/buzz_sure/backend/web/themes/metronic/media/logos/logo-letter-1.png" class="max-h-70px" alt="" />
                </a>
                <!--end::Aside header-->
                <!--begin::Aside title-->
                <h3 class="font-weight-bolder text-center font-size-h4 font-size-h1-lg" style="color: #986923;">Discover Amazing Metronic
                <br />with great build tools</h3>
                <!--end::Aside title-->
            </div>
            <!--end::Aside Top-->
            <!--begin::Aside Bottom-->
            <div class="aside-img d-flex flex-row-fluid bgi-no-repeat bgi-position-y-bottom bgi-position-x-center" style="background-image: url(/buzz_sure/backend/web/themes/metronic/media/svg/illustrations/login-visual-1.svg)"></div>
            <!--end::Aside Bottom-->
        </div>
        <!--begin::Aside-->
        <!--begin::Content-->
        <div class="login-content flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden p-7 mx-auto">
            <!--begin::Content body-->
            <div class="d-flex flex-column-fluid flex-center">
                <!--begin::Signin-->
                <div class="login-form login-signin">
                    <!--begin::Form-->
                    <?php $form = ActiveForm::begin([
                            'id'                     => 'login-form',
                            'enableClientValidation' => false,
                        ]); ?>  
                        <!--begin::Title-->
                        <div class="pb-13 pt-lg-0 pt-5">
                            <h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg"><b>BUZZ</b>Sure</h3>
                            <span class="text-muted font-weight-bold font-size-h4">Sign in to start work.
                        </div>
                        <!--begin::Title-->
                        <!--begin::Form group-->
                        <div class="form-group">
                            <label class="font-size-h6 font-weight-bolder text-dark">Username</label>
                            <?php 
                                echo $form->field($model, 'username', $fieldOptions1)
                                ->label(false)
                                ->textInput(['class'=>'form-control form-control-solid h-auto py-6 px-6 rounded-lg','placeholder' => $model->getAttributeLabel('username'),'autocomplete'=>'off']);
                            ?>
                        </div>
    
                        <div class="form-group fv-plugins-icon-container"> 
                            <div class="d-flex justify-content-between mt-n5">
                                <label class="font-size-h6 font-weight-bolder text-dark pt-5">Password</label>
                            </div>
                            <?php 
                                echo $form->field($model, 'password', $fieldOptions2)
                                ->label(false)
                                ->passwordInput(['class'=>'form-control form-control-solid h-auto py-6 px-6 rounded-lg','placeholder' => $model->getAttributeLabel('password'),'autocomplete'=>'off']);
                            ?>
                        </div>
                        <?php echo $form->field($model, 'rememberMe')->checkbox() ?>
                        <!--end::Form group-->
                        <!--begin::Form group-->
                        <!--end::Form group-->
                        <!--begin::Action-->
                        <div class="pb-lg-0 pb-5">
                            <?= Html::submitButton('Sign in', ['id' => 'kt_login_signin_submit','class' => 'btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3', 'name' => 'login-button']) ?>
                        </div>
                        <!--end::Action-->
                    <?php ActiveForm::end(); ?>
                    <!--end::Form-->
                </div>
                <!--end::Signin-->
            </div>
            <!--end::Content body-->
            <!--begin::Content footer-->
            <div class="d-flex justify-content-lg-start justify-content-center align-items-end py-7 py-lg-0">
                <div class="text-dark-50 font-size-lg font-weight-bolder mr-10">
                    <span class="mr-1"><?php echo date('Y') ?>©</span>
                    <a href="http://keenthemes.com/metronic" target="_blank" class="text-dark-75 text-hover-primary">TOYOTA BUZZ.</a>
                </div>
            </div>
            <!--end::Content footer-->
        </div>
        <!--end::Content-->
    </div>
    <!--end::Login-->
</div>
<!--end::Main-->
<script> console.log($.fn.jquery);</script>
