<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */
if (Yii::$app->controller->action->id === 'login') { 
	/**
	 * Do not use this code in your template. Remove it. 
	 * Instead, use the code  $this->layout = '//main-login'; in your controller.
	 */
	echo $this->render(
		'main-login',
		['content' => $content]
	);
} else {
	backend\assets\MetronicAsset::register($this);
	
	if (class_exists('backend\assets\AppAsset')) {
		backend\assets\AppAsset::register($this);
	} else {
		//app\assets\AppAsset::register($this);
	}
	// add class body tag
	$bodyClass = [];
	if (!empty($this->context->module->id)) {
		$bodyClass[] = 'module-'.$this->context->module->id;
	}
	if (!empty(Yii::$app->controller->id)) {
		$bodyClass[] = Yii::$app->controller->id.'-controller';
	}
	
	if (!empty(Yii::$app->controller->action->id)) {
		$bodyClass[] = Yii::$app->controller->id.'-'.Yii::$app->controller->action->id.'-page';
	}
	// กำหนด Class มาจาก Contrller
	if (!empty($this->context->bodyClass)) {
		$bodyClass[] = $this->context->bodyClass;
	}
?>
	<?php $this->beginPage() ?>
		<!DOCTYPE html>
		<html lang="<?= Yii::$app->language ?>">
			<head>
				<meta charset="<?= Yii::$app->charset ?>">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<?php $this->registerCsrfMetaTags() ?>
				<title><?= Html::encode($this->title) ?></title>
				<?php $this->head() ?>
				<script>
					var baseUrl = '<?php echo Url::base(); ?>';
				</script>
			</head>	

			<div class="page-loader">
				<div class="spinner spinner-primary"></div>
			</div>

			<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading <?= empty($bodyClass)?'':implode(' ', $bodyClass); ?>">
				<?php $this->beginBody() ?>
					<?php echo  $this->render('header-mobile.php'); ?>
					<?php 
						 echo $this->render( 
						 	'content.php',
						 	['content' => $content]); 
					?>
					
					<?php echo $this->render('_user-profile.php'); ?>
					<?php //echo $this->render('_sticky-toolbar.php'); ?>
					<?php //echo $this->render('_chat.php'); ?>
					
					<!--end::Demo Panel-->
					<!--begin::Global Config(global config for global JS scripts)-->
					<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
					<!--end::Global Config-->
				<?php $this->endBody() ?>
			</body>
		</html>
	<?php $this->endPage() ?>
<?php } ?>