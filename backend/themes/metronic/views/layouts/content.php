<?php
    use yii\helpers\Html;
	use yii\widgets\Breadcrumbs;
?>  
    <!--begin::Main-->	
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="d-flex flex-row flex-column-fluid page">
                <?php echo  $this->render('menu.php'); ?>
				<!--begin::Wrapper-->
				<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                    <?php echo  $this->render('header.php'); ?>
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
						<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
							<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
								<!--begin::Info-->
								<div class="d-flex align-items-baseline flex-wrap mr-5">
									
									<!--begin::Actions-->
									<!-- <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
									<span class="text-muted font-weight-bold mr-4">#XRS-45670</span> -->
									<!--end::Actions-->
									<?php if (isset($this->blocks['content-header'])) { ?>
										<h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"><?= $this->blocks['content-header'] ?></h5>
									<?php } else { ?>
										<h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
											<?php
											if ($this->title !== null) {
												echo \yii\helpers\Html::encode($this->title);
											} else {
												echo \yii\helpers\Inflector::camel2words(
													\yii\helpers\Inflector::id2camel($this->context->module->id)
												);
												echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
											} ?>
										</h5>
									<?php } ?>
										
									<?php 
										echo Breadcrumbs::widget([
											'itemTemplate' => "<li class=\"breadcrumb-item text-muted\">{link}</li>\n",
											'activeItemTemplate' => "<li class=\"breadcrumb-item text-muted active\">{link}</li>\n",
											'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
											'options' => ['class' => 'breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm'],
										]) 
									?>
								</div>
								<!--end::Info-->
								<!--begin::Toolbar-->
								<div class="d-flex align-items-center">
									
								</div>
								<!--end::Toolbar-->
							</div>
						</div>
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container-fluid"> <!--  container -->
								<?php echo $content ?>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
						<!--begin::Container-->
						<div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
							<!--begin::Copyright-->
							<div class="text-dark order-2 order-md-1">
								<span class="text-muted font-weight-bold mr-2"><?php echo date('Y') ?>©</span>
								<a href="http://keenthemes.com/metronic" target="_blank" class="text-dark-75 text-hover-primary">TOYOTA BUZZ.</a>
							</div>
							<!--end::Copyright-->
							<!--begin::Nav-->
							<div class="nav nav-dark">
							
							</div>
							<!--end::Nav-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Main-->