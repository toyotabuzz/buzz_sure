<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;
/**
 * Main backend application asset bundle.
 */
class MetronicAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public function init() {

        $this->cssOptions['position'] = View::POS_HEAD;
        $this->jsOptions['position'] = View::POS_HEAD;

        parent::init();

    }

    public $css = [
        'themes/metronic/css/plugins.fonts.css',
        'themes/metronic/plugins/global/plugins.bundle.css',
        'themes/metronic/plugins/custom/prismjs/prismjs.bundle.css',
        'themes/metronic/css/style.bundle.css',
        'themes/metronic/css/themes/layout/header/base/light.css',
        'themes/metronic/css/themes/layout/header/menu/light.css',
        'themes/metronic/css/themes/layout/brand/light.css',
        'themes/metronic/css/themes/layout/aside/light.css'
    ];
    public $js = [
        'themes/metronic/plugins/global/plugins.bundle.js',
        'themes/metronic/plugins/custom/prismjs/prismjs.bundle.js',
        'themes/metronic/js/scripts.bundle.js',
        'themes/metronic/plugins/custom/gmaps/gmaps.js',
        'themes/metronic/js/pages/widgets.js',
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap4\BootstrapAsset',
    ];
}
