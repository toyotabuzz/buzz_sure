<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

class JsoninsureController extends Controller
{
	protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id'=>$value->{$fieldId}, 'name'=>$value->{$fieldName}]);
        }
        return $obj;
    }

    public function actionGetPolicyStatusSecond()
    {
        $out = [];
		if (isset($_POST['depdrop_parents'])) {
			$parents = $_POST['depdrop_parents'];
			if ($parents != null) {
                $status = empty($parents[0])?null:$parents[0];
				$out    = $this->MapData(Yii::$app->ModelUtilities->getItemReferance('POLICY', 'STATUS_SECOND_'.$status, 'Y')->all(), 'ref_value', 'ref_desc');
			}
		}
		return Json::encode(['output'=>$out, 'selected'=>'']);
    }
}
