<div class="accordion accordion-solid accordion-panel accordion-svg-toggle" id="accordionExample8">
    <div class="card">
        <div class="card-header" id="headingOne8">
            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseOne8">
                <div class="card-label"> <i class="far fa-file-alt text-primary"></i> รายละเอียดประกัน</div>
                <span class="svg-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                            <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                            <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                        </g>
                    </svg>
                </span>
            </div>
        </div>
        <div id="collapseOne8" class="collapse" data-parent="#accordionExample8">
            <div class="card-body">
                <div class="row overflow-auto" style="height: auto; max-height: 660px">
                    <div class="col-sm-12 col-md-12">
                        <table class="table table-bordered table-hover margin-bottom-0">
                            <thead>
                                <tr class="card-header text-white bg-primary">
                                    <td class="text-center" style="width: 40%;">รายการ</td>
                                    <td class="text-center" style="border: 1px solid;">ภาคสมัครใจ</td>
                                    <td class="text-center" style="border: 1px solid;">ภาคบังคับ</td>
                                </tr>
                            </thead>
                            <tbody id="partoption_table">
                                <tr class="text-left">
                                    <td>บริษัทประกัน</td>
                                    <?php
                                        $sureK = empty($model->tInsureCompanyK) ? null : $model->tInsureCompanyK->title;
                                        $sureP = empty($model->tInsureCompanyP) ? null : $model->tInsureCompanyP->title;
                                    ?>
                                    <td class="text-center"><?php echo empty($model->tInsureBrokerK->title) ? (empty($sureK) ? null :  'ขายตรง, ' . $sureK) : $model->tInsureBrokerK->title .', ' . $sureK; // K ภาคสมัครใจ ?></td>
                                    <td class="text-center"><?php echo empty($model->tInsureBrokerP->title) ? (empty($sureP) ? null :  'ขายตรง, ' . $sureP) : $model->tInsureBrokerP->title .', ' . $sureP; // P ภาคบังคับ ?></td>
                                </tr>
                                <tr class="text-left">
                                    <td>หมายเลขกรรมธรรม์</td>
                                    <td class="text-center"><?php echo $model->k_number; ?></td>
                                    <td class="text-center"><?php echo $model->p_number; ?></td>
                                </tr>
                                <tr class="text-left">
                                    <td>วันที่คุ้มครอง</td>
                                    <?php
                                        $k_start_date =  (empty($model->k_start_date) || $model->k_start_date == '0000-00-00') ? null : date('d-m-Y', strtotime($model->k_start_date));
                                        $k_end_date =  (empty($model->k_end_date) || $model->k_end_date == '0000-00-00') ? null : date('d-m-Y', strtotime($model->k_end_date));
                                        $p_start_date =  (empty($model->p_start_date) || $model->p_start_date == '0000-00-00') ? null : date('d-m-Y', strtotime($model->p_start_date));
                                        $p_end_date =  (empty($model->p_end_date) || $model->p_end_date == '0000-00-00') ? null : date('d-m-Y', strtotime($model->p_end_date));
                                        $textTo_K = null;
                                        $textTo_P = null;
                                        if(!empty($k_start_date) && !empty($k_end_date)) {
                                            $textTo_K = " ถึง ";
                                        }
                                        if(!empty($p_start_date) && !empty($p_end_date)) {
                                            $textTo_P = " ถึง ";
                                        }
                                    ?>
                                    <td class="text-center"><?php echo "<font color='success'>".$k_start_date."</font>".$textTo_K."<font color='red'>".$k_end_date."</font>"; ?></td>
                                    <td class="text-center"><?php echo "<font color='success'>".$p_start_date."</font>".$textTo_P."<font color='red'>".$p_end_date."</font>"; ?></td>
                                </tr>
                                <tr class="text-left">
                                    <td>ประเภทการขาย</td>
                                    <?php 
                                        $textK = "";
                                        $textP = "";
                                        if(!empty($model->k_frees)) {
                                            if($model->k_frees == 1) {
                                                $textK = "Dealer แถม";
                                            } else if($model->k_frees == 2) {
                                                $textK = "TMT แถม";
                                            } else if($model->k_frees == 3) {
                                                $textK = "F/N แถม";
                                            } else if($model->k_frees == 4) {
                                                $textK = "ลูกค้าซื้อ";
                                            } else if($model->k_frees == 5) {
                                                $textK ="ประกันภัยแถม";
                                            }
                                        }
                                        if(!empty($model->p_frees)) {
                                            if($model->p_frees == 1) {
                                                $textP = "Dealer แถม";
                                            } else if($model->p_frees == 2) {
                                                $textP = "TMT แถม";
                                            } else if($model->p_frees == 3) {
                                                $textP = "F/N แถม";
                                            } else if($model->p_frees == 4) {
                                                $textP = "ลูกค้าซื้อ";
                                            } else if($model->p_frees == 5) {
                                                $textP ="ประกันภัยแถม";
                                            }
                                        }
                                    ?>
                                    <td class="text-center"><?php echo $textK; ?></td>
                                    <td class="text-center"><?php echo $textP; ?></td>
                                </tr>
                                <tr class="text-left">
                                    <td>ประเภทรถและขนาดรถยนต์</td>
                                    <td class="text-center"><?php echo empty($model->tInsureFee1K) ? "" : $model->tInsureFee1K->code.', '.$model->tInsureFee1K->title; ?></td>
                                    <td class="text-center"><?php echo empty($model->tInsureFeeP) ? "" : $model->tInsureFeeP->code.', '.$model->tInsureFeeP->title; ?></td>
                                </tr>
                                <tr class="text-left">
                                    <td>การประกันภัย</td>
                                    <td class="text-center"><?php echo empty($model->k_new) ? "" : $model->k_new; ?></td>
                                    <td class=""></td>
                                </tr>
                                <tr class="text-left">
                                    <td>ปีที่ต่อประกัน</td>
                                    <td class="text-center"><?php echo empty($model->p_year) ? "" : $model->p_year; ?></td>
                                    <td class=""></td>
                                </tr>
                                <tr class="text-left">
                                    <td>ประเภทประกันภัย</td>
                                    <td class="text-center"><?php echo empty($model->tInsureType) ? "" : $model->tInsureType->title; ?></td>
                                    <td class=""></td>
                                </tr>
                                <tr class="text-left">
                                    <td>ประเภทการซ่อม</td>
                                    <td class="text-center"><?php echo empty($model->k_fix) ? "" : $model->k_fix; ?></td>
                                    <td class=""></td>
                                </tr>
                                <tr class="text-left">
                                    <td>1) เบี้ย พรบ. สุทธิ </td>
                                    <td class="text-right"><?php echo number_format($model->k_num02,2) ?></td>
                                    <td class="text-right"><?php echo number_format($model->p_total,2) ?></td>
                                </tr>
                                <tr class="text-left">
                                    <td>2) อากร</td>
                                    <td class="text-right"><?php echo number_format($model->k_num03,2) ?></td>
                                    <td class="text-right"><?php echo number_format($model->p_argon,2) ?></td>
                                </tr>
                                <tr class="text-left">
                                    <td>เบี้ย พรบ. สุทธิ + อากร</td>
                                    <td class="text-right"><?php echo number_format($model->k_num06,2) ?></td>
                                    <td class="text-right"><?php echo number_format($model->k_num09,2) ?></td>
                                </tr>
                                <tr class="text-left">
                                    <td>หักภาษี ณ ที่จ่าย 1%</td>
                                    <td class="text-right"><?php echo number_format($model->k_num07,2) ?></td>
                                    <td class="text-right"><?php echo number_format($model->p_vat,2) ?></td>
                                </tr>
                                <tr class="text-left">
                                    <td>3) ค่าภาษี</td>
                                    <td class="text-right"><?php echo number_format($model->k_num04,2) ?></td>
                                    <td class="text-right"><?php echo number_format($model->p_insure_vat,2) ?></td>
                                </tr>
                                <tr class="text-left">
                                    <td>4) เบี้ยรวมภาษีอากร</td>
                                    <td class="text-right"><?php echo number_format($model->k_num05,2) ?></td>
                                    <td class="text-right"><?php echo number_format($model->p_prb_price,2) ?></td>
                                </tr>
                                <tr class="text-left">
                                    <td><b>เบี้ยหลังหัก ณ ที่จ่าย 1%</b></td>
                                    <td class="text-right"><?php echo number_format($model->k_num08,2) ?></td>
                                    <td class="text-right"><?php echo number_format($model->k_num11,2) ?></td>
                                </tr>
                                <tr class="text-left">
                                    <td><b>รวมก่อนหักส่วนลด</b></td>
                                    <td class="text-right"><?php echo number_format($model->k_num12,2) ?></td>
                                    <td class=""></td>
                                </tr>
                                <tr class="text-left">
                                    <td><b>ส่วนลดพิเศษ</b></td>
                                    <td class="text-right"><?php echo number_format($model->k_num10,2) ?></td>
                                    <td class=""></td>
                                </tr>
                                <tr class="text-left">
                                    <td><b>ยอดชำระสุทธิ</b></td>
                                    <td class="text-right"><?php echo number_format($model->k_num13,2) ?></td>
                                    <td class=""></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>