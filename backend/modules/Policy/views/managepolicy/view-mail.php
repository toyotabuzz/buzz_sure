<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\widgets\DateTimePicker;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TInsure */

$this->title = empty($model->tInsureCar->car_number) ? null : $model->tInsureCar->car_number;
$this->params['breadcrumbs'][] = ['label' => 'รายการที่ต้องการนำส่งเอกสาร', 'url' => ['send-ems']];
$this->params['breadcrumbs'][] = $this->title;
// \yii\web\YiiAsset::register($this);
?>
<div class="tinsure-view">

    <?php $form = ActiveForm::begin([
        'id' => 'form-ems',
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => ['deviceSize' => ActiveForm::SIZE_SMALL],
    ]);
    ?>
    <div class="row">
        <div class="col-md-12 gutter-b">
            <?php echo $this->render('_view-detail-customer', ['model' => $modelInsureForm]); ?>
        </div>

        <div class="col-md-6 gutter-b">
            <?php echo $this->render('view-detail-insure', ['form' => $form, 'model' => $model]); ?>
        </div>

        <div class="col-md-6 gutter-b">
            <div class="gutter-b">
                <?php echo $this->render('_list-insure-payment', ['dataProvider' => $dataProviderInsureItemDetail]); ?>
            </div>

            <div class="card card-custom example example-compact">
                <div class="card-header bg-success">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="fas fa-plus-circle text-light"></i>
                        </span>
                         <h3 class="card-label text-light">นำส่ง</h3> 
                    </div>
                </div>
                <div class="card-body">
                    <div class="row col-md-12">
                        <div class="col-md-6">
                            <?php
                            $modelMailItem->from_date = date('d-m-Y H:i:m');
                            echo $form->field($modelMailItem, 'from_date')->widget(DateTimePicker::classname(), [
                                'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                                'options' => ['placeholder' => 'วัน-เดือน-ปี ค.ศ.', 'autocomplete' => 'off'],
                                'disabled' => true,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd-mm-yyyy H:i:m'
                                ]
                            ])->label('วันที่นำส่ง');
                            ?>
                            <input type="hidden" name="TInsureMailItem[from_date]" value="<?php echo $modelMailItem->from_date; ?>">
                        </div>
                    </div>
                    <div class="row col-md-12 gutter-b">
                        <div class="col-md-6">
                            ผู้นำส่ง :
                            <input type="hidden" name="TInsureMailItem[from_officer]" value="<?php echo $modelMember->id; ?>">
                            <?php
                            $Name = '';
                            if (!empty($modelMember)) {
                                $Name = $modelMember->firstname . ' ' . $modelMember->lastname;
                            }
                            echo $Name; ?>
                        </div>
                    </div>
                    <div class="row col-md-12 gutter-b">
                        <div class="col-md-6">
                            <?php echo $form->field($modelMailItem, 'code')->textInput()->label('เลขนำส่ง'); ?>
                        </div>
                    </div>

                    <div class="row col-md-12 gutter-b">
                        <div class="col-md-12">
                            <b>สถานะนำส่ง</b>
                        </div>
                    </div>
                    <div class="row col-md-12 gutter-b">
                        <div class="col-md-4 gutter-b">
                            <label class="checkbox checkbox-danger">
                                <!-- <?php echo $form->field($modelMailItemDetail, 'insure_stock_id')->checkbox() ?> -->
                                <?php
                                $checkKom = '';
                                if (!empty($modelMailItemDetail->insure_stock_id) && $modelMailItemDetail->insure_stock_id != 0) {
                                    $checkKom = 'checked';
                                }
                                ?>
                                <input type="checkbox" <?php echo $checkKom; ?> name="TInsureMailItemDetail[insure_stock_id]" value="<?php echo empty($modelMailItemDetail->insure_stock_id) ? null : $modelMailItemDetail->insure_stock_id; ?>">
                                <span></span>
                                &nbsp;กรมธรรม์
                            </label>
                        </div>

                        <div class="col-md-4 gutter-b">
                            <label class="checkbox checkbox-danger">
                                <!-- <?php echo $form->field($modelMailItemDetail, 'stock_insure_id')->checkbox() ?> -->
                                <?php
                                $checkPrb = '';
                                if (!empty($modelMailItemDetail->stock_insure_id) && $modelMailItemDetail->stock_insure_id != 0) {
                                    $checkPrb = 'checked';
                                }
                                ?>
                                <input type="checkbox" <?php echo $checkPrb; ?> name="TInsureMailItemDetail[stock_insure_id]" value="<?php echo empty($modelMailItemDetail->stock_insure_id) ? null : $modelMailItemDetail->stock_insure_id; ?>">
                                <span></span>
                                &nbsp;พรบ
                            </label>
                        </div>

                        <div class="col-md-4 gutter-b">
                            <label class="checkbox checkbox-danger">
                                <!-- <?php echo $form->field($modelMailItemDetail, 'bill')->checkbox() ?> -->
                                <?php
                                $checkBill = '';
                                if (!empty($modelMailItemDetail->bill)) {
                                    $checkBill = 'checked';
                                }
                                ?>
                                <input type="checkbox" <?php echo $checkBill; ?> name="TInsureMailItemDetail[bill]" value="<?php echo empty($modelMailItemDetail->bill) ? null : $modelMailItemDetail->bill; ?>">
                                <span></span>
                                &nbsp;ใบเสร็จ
                            </label>
                        </div>
                    </div>
                    <br>
                    <div class="row col-md-12 gutter-b">
                        <div class="col-md-4 gutter-b">
                            <label class="checkbox checkbox-danger">
                                <!-- <?php echo $form->field($modelMailItemDetail, 'pasee')->checkbox() ?> -->
                                <?php
                                $checkPasee = '';
                                if (!empty($modelMailItemDetail->pasee)) {
                                    $checkPasee = 'checked';
                                }
                                ?>
                                <input type="checkbox" <?php echo $checkPasee; ?> name="TInsureMailItemDetail[pasee]" value="<?php echo empty($modelMailItemDetail->pasee) ? null : $modelMailItemDetail->pasee; ?>">
                                <span></span>
                                &nbsp;ใบต่อภาษี
                            </label>
                        </div>

                        <div class="col-md-4 gutter-b">
                            <label class="checkbox checkbox-danger">
                                <!-- <?php echo $form->field($modelMailItemDetail, 'saluk')->checkbox() ?> -->
                                <?php
                                $checkSaluk = '';
                                if (!empty($modelMailItemDetail->saluk)) {
                                    $checkSaluk = 'checked';
                                }
                                ?>
                                <input type="checkbox" <?php echo $checkSaluk; ?> name="TInsureMailItemDetail[saluk]" value="<?php echo empty($modelMailItemDetail->saluk) ? null : $modelMailItemDetail->saluk; ?>">
                                <span></span>
                                &nbsp;ใบสลักหลัง
                            </label>
                        </div>
                    </div>

                    <div class="row col-md-12 gutter-b">
                        <div class="col-md-12">
                            <?php echo $form->field($modelMailItemDetail, 'code')->textInput(['class' => 'scanner'])->label('รหัสนำส่ง'); ?>
                            <!-- <input lang="en" class="my-input" id="my-input" type="text" tabindex="0"> -->
                        </div>
                    </div>

                    <div class="row col-md-12 gutter-b">
                        <div class="col-md-12">
                            <?php echo $form->field($modelMailItem, 'from_remark')->textarea(['rows' => 3])->label('หมายเหตุ'); ?>
                        </div>
                    </div>
                    <div class="box-footer text-right">
                        <?php echo Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-success', 'onClick' => 'btnSubmit();']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    function btnSubmit() {
        var msg = '';
        var error = 0;
        var mailitemCode = $("#tinsuremailitem-code").val();
        var mailitemDetailCode = $("#tinsuremailitemdetail-code").val();
        if (mailitemCode == '') {
            error++;
            msg += error + ". กรุณาระบุเลขนำส่ง <br>";
        }
        if ($("input[name='TInsureMailItemDetail[insure_stock_id]']:checked").length == 0 && $("input[name='TInsureMailItemDetail[stock_insure_id]']:checked").length == 0 && $("input[name='TInsureMailItemDetail[bill]']:checked").length == 0 && $("input[name='TInsureMailItemDetail[pasee]']:checked").length == 0 && $("input[name='TInsureMailItemDetail[saluk]']:checked").length == 0) {
            error++;
            msg += error + ". กรุณาเลือกสถานะนำส่ง <br>";
        }
        if (mailitemDetailCode == '') {
            error++;
            msg += error + ". กรุณาระบุรหัสนำส่ง <br>";
        }

        //Data สถานะนำส่ง หาก ติ๊กมาตามสถานะ
        if ($("input[name='TInsureMailItemDetail[insure_stock_id]']:checked").length != 0) {
            $("input[name='TInsureMailItemDetail[insure_stock_id]']").val(1);
        }
        if ($("input[name='TInsureMailItemDetail[stock_insure_id]']:checked").length != 0) {
            $("input[name='TInsureMailItemDetail[stock_insure_id]']").val(1);
        }
        if ($("input[name='TInsureMailItemDetail[bill]']:checked").length != 0) {
            $("input[name='TInsureMailItemDetail[bill]']").val(1);
        }
        if ($("input[name='TInsureMailItemDetail[pasee]']:checked").length != 0) {
            $("input[name='TInsureMailItemDetail[pasee]']").val(1);
        }
        if ($("input[name='TInsureMailItemDetail[saluk]']:checked").length != 0) {
            $("input[name='TInsureMailItemDetail[saluk]']").val(1);
        }

        if (error) {
            Swal.fire({
                icon: 'warning',
                title: 'กรุณาตรวจสอบข้อมูลต่อไปนี้',
                html: msg,
                confirmButtonColor: '#d33',
                confirmButtonText: 'ปิด',
                allowOutsideClick: false,
                focusConfirm: false,
            });
            return false;
        } else {
            Swal.fire({
                title: 'คุณต้องการบันทึกข้อมูล ใช่หรือไม่',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ตกลง',
                cancelButtonText: 'ยกเลิก',
                allowOutsideClick: false,
                focusConfirm: false,
            }).then((result) => {
                if (result.value) { // กรณีกดปุ่ม "ตกลง" หรือ confirm
                    LoadingShow();
                    $('form[id="form-ems"]').submit();
                }
            });
        }
    }
</script>