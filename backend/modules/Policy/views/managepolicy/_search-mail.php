<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

?>

<div class="managepolicy-search gutter-b">
    <?php 
        $form = ActiveForm::begin([
            'action' => ['send-ems'],
            'method' => 'get',
        ]); 
    ?>
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label"><i class="fa fa-search"></i> ค้นหา </h3>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <?php 
                            echo $form->field($model, 'car_number', 
                                [
                                    'template' => '{beginLabel}{labelTitle}{endLabel}{beginWrapper}{input}{hint}{error}{endWrapper}<small class="text-muted">ค้าหา เลขถัง.</small>'
                                ]
                            )->label('เลขถัง');
                        ?>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <?php 
                            echo $form->field($model, 'car_plate', 
                                [
                                    'template' => '{beginLabel}{labelTitle}{endLabel}{beginWrapper}{input}{hint}{error}{endWrapper}<small class="text-muted">ค้าหา เลขทะเบียน.</small>'
                                ]
                            )->label('เลขทะเบียน'); 
                        ?>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-sm-6 col-md-6 text-left"></div>
                    <div class="col-sm-6 col-md-6 text-right">
                        <?php echo Html::submitButton('<i class="fa fa-search"></i> ค้นหา', ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        </div>


    <?php ActiveForm::end(); ?>
</div>