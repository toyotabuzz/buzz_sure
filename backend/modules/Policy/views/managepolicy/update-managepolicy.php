<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TInsure */

$this->title = 'ข้อมูลรอเลขกรมธรรม์ประกันภัย';
$this->params['breadcrumbs'][] = ['label' => 'จัดการกรมธรรม์', 'url' => ['search-policy']];
$this->params['breadcrumbs'][] = 'ข้อมูลรอเลขกรมธรรม์ประกันภัย';
?>

<h1><?php echo Html::encode($this->title) ?></h1>

<div class="row update-managepolicy">
    <div class="col-lg-12 gutter-b">
        <?php echo $this->render('_view-detail-customer', ['model' => $modelInsureForm]); ?>
    </div>

    <div class="col-lg-6 gutter-b">
        <div class="gutter-b">
            <?php echo $this->render('view-detail-insure', ['model' => $modelInsure]); ?>
        </div>

        <div class="gutter-b">
            <?php echo $this->render('_list-policy-status-log', ['dataProvider' => $dataProviderJmpolicyStatusLog]); ?>
        </div>

    </div>
    
    <div class="col-lg-6 gutter-b">
        <div class="gutter-b">
            <?php echo $this->render('_list-insure-payment', ['dataProvider' => $dataProviderInsureItemDetail]); ?>
        </div>

        
        <div class="gutter-b">
            <?php echo $this->render('_form-managepolicy', ['model' => $modelInsure,'modelTInsurePayment' => $modelTInsurePayment]);?>
        </div>

    </div>
</div>