<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model common\models\Tumenusub */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="TInsure-policy-form">

    <?php 
        $form = ActiveForm::begin([
            'id' => 'form-policy', 
            'type' => ActiveForm::TYPE_VERTICAL
        ]);
        echo $form->field($model, 'insure_id')->hiddenInput()->label(false);
    ?>
    <?php
        if($modelTInsurePayment['premium_received'] < $modelTInsurePayment['premium']){
    ?>
        <div class="alert alert-danger" role="alert">
            <a href="#" class="btn btn-icon btn-light-danger pulse pulse-danger mr-5">
                <i class="flaticon2-information"></i>
                <span class="pulse-ring"></span>
            </a>

            มียอดค้างชำระ ไม่สามารถ พร้อมส่ง EMS
        </div>
    <?php
        }
    ?>

	<div class="card card-custom example example-compact">
        <div class="card-header bg-success">
            <div class="card-title">
                <span class="card-icon">
                    <i class="fas fa-plus-circle text-light"></i>
                </span>
                <h3 class="card-label text-light">ข้อมูลรอเลขกรมธรรม์ประกันภัย</h3>
            </div>
        </div>
            
        <div class="card-body">
            
            <div class="row">
                <div class="col-md-4">
                    <?php echo $form->field($model, 'k_number')->label('ระบุหมายเลขกรมธรรม์') ?>
                </div>
                <div class="col-md-4">
                    <label class="is-required has-star" for="tinsure-policy_status_id">สถานะ</label>
                    <?php 
                        $data = Yii::$app->ModelUtilities->getListReferance('POLICY', 'STATUS',false,'Y');
                        if($modelTInsurePayment['premium_received'] < $modelTInsurePayment['premium']){
                            $data = array_diff($data, ['พร้อมส่ง']);
                        }
                        echo  $form->field($model, 'policy_status_id')->widget(Select2::classname(), [
                            'data'          => $data,
                            'theme'         => Select2::THEME_KRAJEE,
                            'options'       => ['placeholder' => 'Select a state ...','onchange' => 'checkStatusSecond();'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                    ?>
                </div>
                <div id="div-status-second" class="col-md-4">
                    <?php 
                        echo $form->field($model, 'policy_status_second_id')->widget(DepDrop::classname(), [
                            'data'           => [],
                            'options'        => ['placeholder' => 'Select ...'],
                            'type'           => DepDrop::TYPE_SELECT2,
                            'select2Options' => ['theme' => Select2::THEME_KRAJEE,'pluginOptions' => ['allowClear' => true]],
                            'pluginOptions'  => [
                                'depends'     => ['tinsure-policy_status_id'],
                                'url'         => Url::to(['/jsoninsure/get-policy-status-second']),
                                'loadingText' => 'Loading ...',
                                'initialize' => true
                            ]
                        ]);
                    ?>
                </div>
                <div class="col-md-12">
                    <?php echo $form->field($model, 'k_ins_remark')->textarea(['rows' => 4])->label('หมายเหตุ') ?>
                </div>
            </div>

            
        </div>
	
        <div class="card-footer text-right">
            <?php echo Html::Button('<i class="fas fa-save"></i> บันทึก', ['class' => 'btn btn-success','onclick'=> 'btnSibmit();']) ?>
        </div>

	</div>

    <?php ActiveForm::end(); ?>
</div>
<script defer>
    function checkStatusSecond(){
        var policy_status_id = $('#tinsure-policy_status_id').val();
        if(policy_status_id == 1){
            $('#div-status-second').hide();
        }else{
            $('#div-status-second').show();
        }
    }
    $('#div-status-second').hide();

    function btnSibmit(){
        var textMsg = '<div style="text-align: left;padding-left: 20px;">';
        var error = 0;
        
        if ($('#tinsure-k_number').val() == "") {
            if ($('#tinsure-policy_status_second_id').val() == "") {
                error++;
                textMsg += error + '. กรุณาระบุหมายเลขกรมธรรม์<br/>';
            }
        }

        if ($('#tinsure-policy_status_id').val() == "") {
            if ($('#tinsure-policy_status_second_id').val() == "") {
                error++;
                textMsg += error + '. กรุณาระบุสถานะ<br/>';
            }
        }else{
            if ($('#tinsure-policy_status_id').val() != 1) {
                if ($('#tinsure-policy_status_second_id').val() == "") {
                    error++;
                    textMsg += error + '. กรุณาระบุเหตุผล<br/>';
                }
            }
        }

        

        textMsg += '</div>';
        if (error > 0) {
            Swal.fire({
                icon: 'warning',
                title: 'กรุณาตรวจสอบข้อมูลดังต่อไปนี้ และลองใหม่อีกครั้ง',
                width: '480px',
                html: textMsg,
                confirmButtonColor: '#d33',
                confirmButtonText: 'ปิด',
            });
            return false;
        } else {

            Swal.fire({
                title: 'ยืนยันการบันทึกข้อมูล',
                icon: 'warning',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'ยืนยัน',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'ยกเลิก',
                allowOutsideClick: false,
                focusConfirm: false,
            }).then((result) => {
                if (result.value) {
                    $('#form-policy').submit();
                }
            });
        }
    }
</script>
