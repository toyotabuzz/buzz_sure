<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

?>
<?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
        [
            'label' => 'รายการ',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-left'],
            'pageSummary'=>'รวม',
            'pageSummaryOptions' => ['class' => 'kv-align-right'],
            'value' => function ($model) {
                return empty($model->tPaymentSubject->title)?null:$model->tPaymentSubject->title.' '.$model->remark;
            },
        ],
        [
            'label' => 'จำนวนเงิน ที่ต้องชำระ',
            'format' => ['decimal', 2],
            'pageSummary' => true,
            'pageSummaryOptions' => ['class' => 'kv-align-right'],
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-right'],
            'value' => function ($model) {
                return empty($model->price)?null:($model->price*$model->qty);
            },
        ],
        [
            'label' => 'จำนวนเงิน ที่ชำระจริง',
            'format' => ['decimal', 2],
            'pageSummary' => true,
            'pageSummaryOptions' => ['class' => 'kv-align-right'],
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-right'],
            'value' => function ($model) {
                return empty($model->price)?null:($model->price_acc*$model->qty);
            },
        ],
        [
            'label' => 'คงค้าง',
            'format' => ['decimal', 2],
            'pageSummary' => true,
            'pageSummaryOptions' => ['class' => 'kv-align-right'],
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-right'],
            'value' => function ($model) {
                return  empty($model->price)?null:(($model->price-$model->price_acc)*$model->qty);
            },
        ]
    ];
?>
<div class="accordion accordion-solid accordion-panel accordion-svg-toggle" id="accordionPayment">
    <div class="card">
        <div class="card-header" id="headingPayment"> 
            <div class="card-title" data-toggle="collapse" data-target="#collapsePayment"> 
                <div class="card-label"> <i class="far fa-money-bill-alt text-primary"></i> ข้อมูลการชำระเงิน</div>
                    <span class="svg-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                        </g>
                        </svg>
                    </span>
            </div>
            
            <div id="collapsePayment" class="collapse show" data-parent="#accordionPayment">
                <div class="card-body">
                    <?php 
                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            //filterModel' => $searchModel,
                            'panel' => [
                                'type' => GridView::TYPE_PRIMARY,
                                'heading' => '<i class="fas fa-list text-light"></i> รายละเอียดการรับชำระ',
                                'before' => false,
                                'after' => false,
                            ],
                            'toolbar' => [
                                'content' => ExportMenu::widget([
                                    'dataProvider' => $dataProvider,
                                    'columns' => $gridColumns,
                                    'fontAwesome' => true,
                                    'target' => ExportMenu::TARGET_BLANK,
                                    'exportConfig' => [
                                        ExportMenu::FORMAT_HTML => false,
                                        ExportMenu::FORMAT_TEXT => false,
                                        ExportMenu::FORMAT_PDF  => false,
                                    ],
                                    'filename' => 'export_data_' . date('Ymd'),
                                ]),
                                //'{toggleData}',
                            ],
                            'bordered' => true,
                            'striped' => true,
                            'responsive' => false,
                            'responsiveWrap' => false,
                            'floatHeader' => false,
                            // 'floatOverflowContainer'=>true, //only set floatHeader = true for fix header
                            'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false,        
                            'headerRowOptions' => ['class' => 'info'],
                            'filterRowOptions' => ['class' => 'info'],
                            'columns' => $gridColumns,
                            'showPageSummary' => true,
                        ]); 
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>