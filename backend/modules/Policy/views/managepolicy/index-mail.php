<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TInsureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการที่ต้องการนำส่งเอกสาร';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tinsure-index">
<?php echo $this->render('_search-mail', ['model' => $searchModel]); ?>

<?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
        [
            'label' => 'รุ่นรถ',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'value' => function ($model) {
                return empty($model->tInsureCar->tCarModel->title) ? null : $model->tInsureCar->tCarModel->title;
            },
        ],
        [
            'label' => 'เลขทะเบียน',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'value' => function ($model) {
                return empty($model->tInsureCar->code) ? null : $model->tInsureCar->code;
            },
        ],
        [
            'label' => 'เลขถัง',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'value' => function ($model) {
                return empty($model->tInsureCar->car_number) ? null : $model->tInsureCar->car_number;
            },
        ],
        [
            'label' => 'เลขเครื่อง',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'value' => function ($model) {
                return empty($model->tInsureCar->engine_number) ? null : $model->tInsureCar->engine_number;
            },
        ],
        [
            'label' => 'ชื่อลูกค้า',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'value' => function ($model) {
                $fullname = null;
                if(!empty($model->tInsureCar->tInsureCustomer->firstname)){
                    if(!empty($model->tInsureCar->tInsureCustomer->title)){
                        $fullname = $model->tInsureCar->tInsureCustomer->title;
                    }
                    if(!empty($model->tInsureCar->tInsureCustomer->firstname)){
                        if(empty($name)){
                            $fullname .= $model->tInsureCar->tInsureCustomer->firstname;
                        }else{
                            $fullname .= ' '.$model->tInsureCar->tInsureCustomer->firstname;
                        }
                    }
                    if(!empty($model->tInsureCar->tInsureCustomer->lastname)){
                        $fullname .= ' '.$model->tInsureCar->tInsureCustomer->lastname;
                    }
                }
                return $fullname;
            },
        ],

        [
            'label' => 'ภาคสมัครใจ',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-left'],
            'format' =>'raw',
            'value' => function ($model) {
                $Data_K = "";
                $sureK = empty($model->tInsureCompanyK) ? null : 'บ.ประกันภัย : '.$model->tInsureCompanyK->short_title;
                $k_number =  empty($model->k_number) ? null : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เลขกรมธรรม์ : '.$model->k_number;
                $k_start_date =  (empty($model->k_start_date) || $model->k_start_date == '0000-00-00') ? null : 'วันที่เริ่มต้น : '.date('d-m-Y', strtotime($model->k_start_date));
                $k_end_date =  (empty($model->k_end_date) || $model->k_end_date == '0000-00-00') ? null : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วันที่สิ้นสุด : '.date('d-m-Y', strtotime($model->k_end_date));

                $Data_K = $sureK.$k_number.'<br>'.$k_start_date.$k_end_date;
                return  $Data_K;
            },
        ],
        [
            'label' => 'ภาคบังคับ',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'format' =>'raw',
            'value' => function ($model) {
                $Data_P = "";
                $sureP = empty($model->tInsureCompanyP) ? null : 'บ.ประกันภัย : '.$model->tInsureCompanyP->short_title;
                $p_number =  empty($model->p_number) ? null : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เลขกรมธรรม์ : '.$model->p_number;
                $p_start_date =  (empty($model->p_start_date) || $model->p_start_date == '0000-00-00') ? null : 'วันที่เริ่มต้น : '.date('d-m-Y', strtotime($model->p_start_date));
                $p_end_date =  (empty($model->p_end_date) || $model->p_end_date == '0000-00-00') ? null : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วันที่สิ้นสุด : '.date('d-m-Y', strtotime($model->p_end_date));

                $Data_P = $sureP.$p_number.'<br>'.$p_start_date.$p_end_date;
                return  $Data_P;
            },
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{view-ems}',
            'buttons' => [
                'view-ems' => function ($url, $model) {
                    return Html::a('<i class="fas fa-eye text-primary"></i>', $url, [ 
                        'title' => 'View',
                        'class' => 'btn btn-icon btn-light btn-hover-primary  btn-sm'
                    ]);
                },

            ],
        ],
    ];
?>
<?php 
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        //filterModel' => $searchModel,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<i class="fas fa-list text-light"></i>',
            'before' => false,
            'after' => false,
        ],
        'toolbar' => [
            'content' => ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'fontAwesome' => true,
                'target' => ExportMenu::TARGET_BLANK,
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_PDF  => false,
                ],
                'filename' => 'export_data_' . date('Ymd'),
            ]),
            //'{toggleData}',
        ],
        'bordered' => true,
        'striped' => true,
        'responsive' => false,
        'responsiveWrap' => false,
        'floatHeader' => false,
        // 'floatOverflowContainer'=>true, //only set floatHeader = true for fix header
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false,        
        'headerRowOptions' => ['class' => 'info'],
        'filterRowOptions' => ['class' => 'info'],
        'columns' => $gridColumns,
    ]); 
?>


</div>
