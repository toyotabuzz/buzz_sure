<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

?>
<?php
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],

        [
            'label' => 'วันที่แจ้งงาน',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'value' => function ($model) {
                return empty($model->tInsure->insure_date)?null:date('d/m/Y', strtotime($model->tInsure->insure_date));
            },
        ],
        [
            'label' => 'เลขที่รับแจ้ง',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'value' => function ($model) {
                return empty($model->tInsure->p_rnum)?null:$model->tInsure->p_rnum;
            },
        ],
        [
            'label' => 'หมายเลข พรบ.',
            'format' => 'raw',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'value' => function ($model) {
                return empty($model->tInsure->p_number)?null:$model->tInsure->p_number;
            },
        ],
        [
            'label' => 'ปีต่ออายุ',
            'format' => 'raw',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'value' => function ($model) {
                return empty($model->tInsure->p_year)?null:$model->tInsure->p_year;
            },
        ],
        [
            'label' => 'วันคุ้มครอง กรมธรรม์ (พรบ.)',
            'format' => 'raw',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'value' => function ($model) {
                $date = empty($model->tInsure->k_start_date)?null:date('d/m/Y', strtotime($model->tInsure->k_start_date));
                if(!empty($model->tInsure->p_start_date)){
                    if(($model->tInsure->k_start_date != $model->tInsure->p_start_date) && $model->tInsure->p_check == 1) {
                        if($model->tInsure->p_start_date != '0000-00-00'){
                            $p_start_date = date('d/m/Y', strtotime($model->tInsure->p_start_date));
                            $date = '<span class="text-danger">('.$p_start_date.')</span>';
                        }
                    }
                }
                return  $date;
            },
        ],
        [
            'label' => 'บ.ประกันภัย',
            'format' => 'raw',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'value' => function ($model) {
                return empty($model->tInsure->tInsureCompanyK->short_title)?null:$model->tInsure->tInsureCompanyK->short_title;
            },
        ],
        [
            'label' => 'ชื่อ-นามสกุล',
            'format' => 'raw',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-left'],
            'value' => function ($model) {
                $fullname = null;
                if(!empty($model->tInsure->tInsureCar->tInsureCustomer->firstname)){
                    if(!empty($model->tInsure->tInsureCar->tInsureCustomer->title)){
                        $fullname = $model->tInsure->tInsureCar->tInsureCustomer->title;
                    }
                    if(!empty($model->tInsure->tInsureCar->tInsureCustomer->firstname)){
                        if(!empty($fullname)){
                            $fullname .= ' '.$model->tInsure->tInsureCar->tInsureCustomer->firstname;
                        }else{
                            $fullname .= 'คุณ '.$model->tInsure->tInsureCar->tInsureCustomer->firstname;
                        }
                    }
                    if(!empty($model->tInsure->tInsureCar->tInsureCustomer->lastname)){
                        $fullname .= ' '.$model->tInsure->tInsureCar->tInsureCustomer->lastname;
                    }
                }
                return $fullname;
            },
        ],
        [
            'label' => 'เลขทะเบียน',
            'format' => 'raw',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'value' => function ($model) {
                return empty($model->tInsure->tInsureCar->code)?null:$model->tInsure->tInsureCar->code;
            },
        ],
        [
            'label' => 'พนักงานขาย',
            'format' => 'raw',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'value' => function ($model) {
                $name = null;
                if(!empty($model->tInsure->sale->firstname)){
                    $name = $model->tInsure->sale->firstname; 
                    if(!empty($model->tInsure->sale->lastname)){
                        $name .= ' '.$model->tInsure->sale->lastname;
                    }
                }
                return $name;
            },
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{update-managepolicy}',
            'buttons' => [
                'update-managepolicy' => function ($url, $model) {
                    return Html::a('<i class="fas fa-eye text-primary"></i>', $url, [ 
                        'title' => 'แก้ไข',
                        'class' => 'btn btn-icon btn-light btn-hover-primary  btn-sm'
                    ]);
                },

            ],
        ],
    ];
?>
<?php 
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        //filterModel' => $searchModel,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<i class="fas fa-list text-light"></i>',
            'before' => false,
            'after' => false,
        ],
        'toolbar' => [
            'content' => ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'fontAwesome' => true,
                'target' => ExportMenu::TARGET_BLANK,
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_PDF  => false,
                ],
                'filename' => 'export_data_' . date('Ymd'),
            ]),
            //'{toggleData}',
        ],
        'bordered' => true,
        'striped' => true,
        'responsive' => false,
        'responsiveWrap' => false,
        'floatHeader' => false,
        // 'floatOverflowContainer'=>true, //only set floatHeader = true for fix header
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false,        
        'headerRowOptions' => ['class' => 'info'],
        'filterRowOptions' => ['class' => 'info'],
        'columns' => $gridColumns,
    ]); 
?>