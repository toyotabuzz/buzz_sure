<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;

?>

<div class="managepolicy-search gutter-b">
    <?php 
        $form = ActiveForm::begin([
            'action' => ['search-policy'],
            'method' => 'get',
        ]); 
    ?>
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label"><i class="fas fa-search"></i> ค้นหา </h3>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <?php 
                            echo $form->field($model, 'h_keyword_car', 
                                [
                                    'template' => '{beginLabel}{labelTitle}{endLabel}{beginWrapper}{input}{hint}{error}{endWrapper}<small class="text-muted">ค้าหา เลขทะเบียน, หมายเลขเครื่อง, เลขถัง.</small>'
                                ]
                            )->label('ข้อมูลรถ');
                        ?>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <?php 
                            echo $form->field($model, 'h_keyword_customer', 
                                [
                                    'template' => '{beginLabel}{labelTitle}{endLabel}{beginWrapper}{input}{hint}{error}{endWrapper}<small class="text-muted">ค้าหา ชื่อ, นามสกุล, รหัสบัตรประชาชน.</small>'
                                ]
                            )->label('ข้อมูลลูกค้า'); 
                        ?>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <?php 
                            echo $form->field($model, 'h_keyword_insure', 
                                [
                                    'template' => '{beginLabel}{labelTitle}{endLabel}{beginWrapper}{input}{hint}{error}{endWrapper}<small class="text-muted">ค้าหา เลขที่รับแจ้ง, หมายเลข พรบ.</small>'
                                ]
                            )->label('ข้อมูลกรมธรรม์');
                        ?>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-sm-6 col-md-6 text-left"></div>
                    <div class="col-sm-6 col-md-6 text-right">
                        <?php echo Html::submitButton('<i class="fa fa-search"></i> ค้นหา', ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        </div>


    <?php ActiveForm::end(); ?>
</div>