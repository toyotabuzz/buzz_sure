<?php
    use yii\helpers\Html;
?>
<?php 
    $fullname = null;
    if(!empty($model->tInsure->tInsureCar->tInsureCustomer->firstname)){
        if(!empty($model->tInsure->tInsureCar->tInsureCustomer->title)){
            $fullname = $model->tInsure->tInsureCar->tInsureCustomer->title;
        }
        if(!empty($model->tInsure->tInsureCar->tInsureCustomer->firstname)){
            if(empty($name)){
                $fullname .= $model->tInsure->tInsureCar->tInsureCustomer->firstname;
            }else{
                $fullname .= ' '.$model->tInsure->tInsureCar->tInsureCustomer->firstname;
            }
        }
        if(!empty($model->tInsure->tInsureCar->tInsureCustomer->lastname)){
            $fullname .= ' '.$model->tInsure->tInsureCar->tInsureCustomer->lastname;
        }
    }
    $sale = null;
    if(!empty($model->sale->firstname)){
        $sale = $model->sale->firstname; 
        if(!empty($model->sale->lastname)){
            $sale .= ' '.$model->sale->lastname;
        }
    }
    $buy_type = '-';
    if(!empty($model->tInsure->tInsureCar->buy_type)){
        if($model->tInsure->tInsureCar->buy_type == 1){
            $buy_type = 'ซื้อสด';
        }
        if($model->tInsure->tInsureCar->buy_type == 2){
            $buy_type = 'ไฟแนนซ์';
        }
    }

?>
<div class="t-insure-customer01-view">
    <div class="accordion accordion-solid accordion-panel accordion-svg-toggle" id="accordionCustomer">
        <div class="card">
            <div class="card-header" id="headingCustomer">
                <div class="card-title" data-toggle="collapse" data-target="#collapseCustomer">
                    <div class="card-label"> <i class="far fa-user text-primary"></i> ข้อมูลเบื้องต้นลูกค้า</div>
                        <span class="svg-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                            <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                            <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                            </g>
                            </svg>
                        </span>
                </div>
                
                <div id="collapseCustomer" class="collapse show" data-parent="#accordionCustomer">
                    <div class="card-body">
                        <div class="row">
                            
                            <!-- START ROW 1 -->
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> ชื่อ </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark"> 
                                            <?php echo $fullname; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> เบอร์โทรศัพท์ 1 </span>  
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->tInsureCar->tInsureCustomer->mobile)?'-':$model->tInsure->tInsureCar->tInsureCustomer->mobile; ?>
                                        </span>
                                    </div>  
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> เบอร์โทรศัพท์ 2 </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->tInsureCar->tInsureCustomer->office_tel)?'-':$model->tInsure->tInsureCar->tInsureCustomer->office_tel; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!-- END ROW 1 -->
                            <!-- START ROW 2 -->
                            <div class="col-sm-12 col-lg-6 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> ที่อยู่ </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->tInsureCar->tInsureCustomer->address01)?'-':$model->tInsure->tInsureCar->tInsureCustomer->address01; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!-- END ROW 2 -->
                            <!-- START ROW 3 -->
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> Sale </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($sale)?'-':$sale; ?>
                                        </span>
                                    </div>         
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> วันที่เซลขอแจ้งงาน </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php 
                                                $form_date = empty($model->form_date)?'-':date('d/m/Y', strtotime($model->form_date)); 
                                                echo Yii::$app->Helpers::getDateTHDMY($form_date, "/");
                                            ?>
                                        </span>
                                    </div>    
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> วันที่หัวหน้าทีมอนุมัติ </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php 
                                                $sup_date = empty($model->sup_date)?'-':date('d/m/Y', strtotime($model->sup_date)); 
                                                echo Yii::$app->Helpers::getDateTHDMY($sup_date, "/");
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> วันที่ธุรการแจ้งงาน </span>  
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php 
                                                $insure_date = empty($model->tInsure->insure_date)?'-':date('d/m/Y', strtotime($model->tInsure->insure_date)); 
                                                echo Yii::$app->Helpers::getDateTHDMY($insure_date, "/");
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!-- END ROW 3 -->
                            <!-- START ROW 4 -->
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> วันที่รับเลขรับแจ้ง </span>  
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php 
                                                $p_rnum_date = empty($model->tInsure->p_rnum_date)?'-':date('d/m/Y', strtotime($model->tInsure->p_rnum_date)); 
                                                echo Yii::$app->Helpers::getDateTHDMY($p_rnum_date, "/");
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> เลขรับแจ้ง </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->p_rnum)?'-':$model->tInsure->p_rnum; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> ยี่ห้อ </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->tInsureCar->tCarType->title)?'-':$model->tInsure->tInsureCar->tCarType->title; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> รุ่น </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->tInsureCar->tCarModel->title)?'-':$model->tInsure->tInsureCar->tCarModel->title; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!-- END ROW 4 -->
                            <!-- START ROW 5 -->
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> แบบ </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->tInsureCar->tCarSeries->title)?'-':$model->tInsure->tInsureCar->tCarSeries->title; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> สี </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->tInsureCar->tCarColor->title)?'-':$model->tInsure->tInsureCar->tCarColor->title; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> ทะเบียน </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->tInsureCar->code)?'-':$model->tInsure->tInsureCar->code.' '.$model->tInsure->tInsureCar->province_code; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> ปี </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->tInsureCar->register_year)?'-':$model->tInsure->tInsureCar->register_year; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!-- END ROW 5 -->
                            <!-- START ROW 6 -->
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> เลขเครื่อง </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->tInsureCar->engine_number)?'-':$model->tInsure->tInsureCar->engine_number; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> เลขถัง </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->tInsureCar->car_number)?'-':$model->tInsure->tInsureCar->car_number; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> ราคารถ </span>  
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->tInsureCar->price)?'-':number_format($model->tInsure->tInsureCar->price,2); ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> ประเภทการซื้อ </span>  
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php
                                                echo $buy_type;
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!-- END ROW 6 -->
                            <!-- START ROW 7 -->
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> ไฟแนนซ์ </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php 
                                                echo empty($model->tInsure->tInsureCar->tFundCompany->title)?'-':$model->tInsure->tInsureCar->tFundCompany->title; 
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> วันที่จดทะเบียน </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php 
                                                $registry_date = empty($model->tInsure->tInsureCar->registry_date)?'-':date('d/m/Y', strtotime($model->tInsure->tInsureCar->registry_date)); 
                                                echo Yii::$app->Helpers::getDateTHDMY($registry_date, "/");
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> ค่าภาษี </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->tInsureCar->registry_tax_year)?'-':number_format($model->tInsure->tInsureCar->registry_tax_year,2); ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> ค่าภาษีประจำปี </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php echo empty($model->tInsure->tInsureCar->registry_tax_year_number)?'-':$model->tInsure->tInsureCar->registry_tax_year_number; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> วันที่รับรถจริง </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php 
                                                $get_car_date = empty($model->tInsure->tInsureCar->get_car_date)?'-':date('d/m/Y', strtotime($model->tInsure->tInsureCar->get_car_date)); 
                                                echo Yii::$app->Helpers::getDateTHDMY($get_car_date, "/");
                                            ?>
                                        </span>
                                    </div>  
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> วันที่รับรถจริง </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php 
                                                $get_car_date = empty($model->tInsure->tInsureCar->get_car_date)?'-':date('d/m/Y', strtotime($model->tInsure->tInsureCar->get_car_date)); 
                                                echo Yii::$app->Helpers::getDateTHDMY($get_car_date, "/");
                                            ?>
                                        </span>
                                    </div>  
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-4 col-lg-2 mb-2">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="font-weight-bolder font-size-h5"> วันที่คุ้มครองล่าสุด </span> 
                                    </div>
                                    <div class="col-12 d-flex align-items-center">
                                        <span class="text-dark">
                                            <?php 
                                                $date_verify = empty($model->tInsure->tInsureCar->date_verify)?'-':date('d/m/Y', strtotime($model->tInsure->tInsureCar->date_verify));
                                                echo Yii::$app->Helpers::getDateTHDMY($date_verify, "/");
                                            ?>
                                        </span>
                                    </div>  
                                </div>
                            </div>

                            
                            <!-- END ROW 7 -->

                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>
    

