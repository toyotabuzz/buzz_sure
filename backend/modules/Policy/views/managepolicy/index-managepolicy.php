<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TuroleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'จัดการกรมธรรม์';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="managepolicy-index">
    <?php echo $this->render('_search-managepolicy', ['model' => $searchModel]); ?>

    <?php echo $this->render('_list-managepolicy', ['dataProvider' => $dataProvider]); ?>
    
</div>