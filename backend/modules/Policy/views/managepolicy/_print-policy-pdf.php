<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
<div class="text-center">
    <barcode code="<?php echo $model['barcode']?>" type="C128A" size="0.8" height="1.5"/><br />
    <small><?php echo $model['barcode']?></small><br />
    <?php echo $model['date_verify'].' '.$model['car_code']; ?><br />
</div>