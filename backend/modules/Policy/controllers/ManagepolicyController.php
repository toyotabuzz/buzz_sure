<?php

namespace backend\modules\Policy\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use yii\helpers\Url;

use common\models\TInsureForm;
use common\models\TInsureFormSearch;
use common\models\TInsureSearch;

use common\models\TInsure;
use common\models\TInsureMailItem;
use common\models\TInsureMailItemDetail;
use common\models\TInsureItemDetailSearch;
use common\models\Tmember;
use common\models\TStockInsure;
use common\models\TInsureStockPrb;
use common\models\TInsureStock;
use common\models\TInsureStockSearch;
use common\models\JmpolicyStockCode;
use common\models\JmpolicyStatusLog;
use common\models\JmpolicyStatusLogSearch;
use common\models\TInsureCompany;
/**
 * Default controller for the `policy` module
 */
class ManagepolicyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index','search-policy','update-managepolicy','print-policy','send-ems', 'view-ems'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all turole models.
     * @return mixed
     */

     /**
     * Finds the Tuuser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tuuser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelTInsureForm($id)
    {
        if (($model = TInsureForm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findModelTInsure($id)
    {
        if (($model = TInsure::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSearchPolicy()
    {
        $searchModel = new TInsureFormSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-managepolicy', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionUpdateManagepolicy($id)
    {
        $modelInsureForm              = $this->findModelTInsureForm($id);
        $modelInsure                  = $this->findModelTInsure($modelInsureForm->insure_id);
        $searchModelInsureItemDetail  = new TInsureItemDetailSearch();
        $searchModelJmpolicyStatusLog = new JmpolicyStatusLogSearch();
        $searchModelTInsure           = new TInsureSearch();
  
        $ParamsInsure['TInsureItemDetailSearch']['order_extra_id'] = $modelInsure->insure_id;
        $ParamsInsureItemDetail ['TInsureItemDetailSearch']['order_extra_id'] = $modelInsure->insure_id;
        $ParamsJmpolicyStatusLog['JmpolicyStatusLogSearch']['insure_id']      = $modelInsure->insure_id;

        $modelTInsurePayment           = $searchModelTInsure->searchPayment($ParamsInsure);
        $dataProviderInsureItemDetail  = $searchModelInsureItemDetail->search($ParamsInsureItemDetail);
        $dataProviderJmpolicyStatusLog = $searchModelJmpolicyStatusLog->search($ParamsJmpolicyStatusLog);

        $dataPost = Yii::$app->request->post();
        if (!empty($dataPost)) {
            $save = false;   
            $modelInsure = $this->findModelTInsure($dataPost['TInsure']['insure_id']);
            if(!empty($modelInsure)){
                $strDate = substr($modelInsure->k_start_date,0,7);

                $searchSC = new TInsureStockSearch();
                $Params['TInsureStockSearch']['strDate'] = $strDate;
                $objSC    = $searchSC->search($Params)->asArray()->one();
        
                if(!empty($objSC)){
                    if($objSC['stock_code'] == ""){
                        $strDate = substr($strDate,2,7);
                        $scode = $strDate."-00001";
                    }else{
                        $arrCode = explode("-",$objSC['stock_code']);
                        $scode1 = $arrCode[2];
                        $scode = $arrCode[2]+1;
                        if($scode < 10 ) $scode = "0000".$scode;
                        if($scode >= 10 and $scode < 100 ) $scode = "000".$scode;
                        if($scode >= 100 and $scode < 1000 ) $scode = "00".$scode;
                        if($scode >= 1000 and $scode < 10000 ) $scode = "0".$scode;
                        if($scode >= 10000 ) $scode = $scode;
                        $strDate = substr($strDate,2,7);
                        $scode = $strDate."-".$scode;
                    }  
                }
                $transaction = Yii::$app->db->beginTransaction();

                $jmpolicyStatusLog                          = new JmpolicyStatusLog();
                $jmpolicyStatusLog->insure_id               = $dataPost['TInsure']['insure_id'];
                $jmpolicyStatusLog->k_number                = $dataPost['TInsure']['k_number'];
                $jmpolicyStatusLog->policy_status_id        = $dataPost['TInsure']['policy_status_id'];
                $jmpolicyStatusLog->policy_status_second_id = $dataPost['TInsure']['policy_status_second_id'];
                $jmpolicyStatusLog->k_ins_remark            = $dataPost['TInsure']['k_ins_remark'];
                if (!$jmpolicyStatusLog->save()) {
                    $transaction->rollBack();
                    throw new HttpException(500, Yii::$app->Helpers->GetErrorModel($jmpolicyStatusLog));
                }else{
                    $modelTInsureStock                       = new TInsureStock();
                    $modelTInsureStock->insure_id            = $dataPost['TInsure']['insure_id'];
                    $modelTInsureStock->stock_number         = $dataPost['TInsure']['k_number'];
                    $modelTInsureStock->insure_company_id    = $modelInsure->k_prb_id;
                    $modelTInsureStock->remark               = $dataPost['TInsure']['k_ins_remark'];
                    $modelTInsureStock->status               = "รับเข้าจากบ.ประกันภัย";
                    $modelTInsureStock->status_date          = date("Y-m-d H:i:s");
                    $modelTInsureStock->status_company_id    = Yii::$app->user->identity->company_id;
                    $modelTInsureStock->status_department_id = Yii::$app->user->identity->department;
                    $modelTInsureStock->status_officer_id    = Yii::$app->user->identity->id;
                    $modelTInsureStock->stock_date           = date("Y-m-d H:i:s");
                    $modelTInsureStock->stock_company_id     = Yii::$app->user->identity->company_id;
                    $modelTInsureStock->stock_department_id  = Yii::$app->user->identity->department;
                    $modelTInsureStock->stock_officer_id     = Yii::$app->user->identity->id;
                    $modelTInsureStock->run_stock            = 1;
                    $modelTInsureStock->stock_code           = $scode;
                    $modelTInsureStock->changes              = '';
                    $modelTInsureStock->change_date          = '0000-00-00';
                    $modelTInsureStock->change_form_id       = '';
                    $modelTInsureStock->cancel               = '';
                    $modelTInsureStock->cancel_date          = '0000-00-00';
                    $modelTInsureStock->cancel_form_id       = 0;
                    $modelTInsureStock->send_code            = 0;
                    $modelTInsureStock->doc_send             = 0;

                    if(!$modelTInsureStock->save()){
                        $transaction->rollBack();
                        throw new HttpException(500, Yii::$app->Helpers->GetErrorModel($modelTInsureStock));
                    }else{
                        $modelInsure->k_stock_id    = $modelTInsureStock->insure_stock_id;
                        $modelInsure->k_number      = $dataPost['TInsure']['k_number'];
                        $modelInsure->k_ins_date    = date("Y-m-d H:i:s");
                        $modelInsure->k_ins_remark  = $dataPost['TInsure']['k_ins_remark'];
                        $modelInsure->k_ins_officer = Yii::$app->user->identity->id;
                        $modelInsure->k_kom_status = 'นำเข้าประกันภัย';
                        if(!$modelInsure->save()){
                            $transaction->rollBack();
                            throw new HttpException(500, Yii::$app->Helpers->GetErrorModel($modelInsure));
                        }else{
                            if($dataPost['TInsure']['policy_status_id'] == 1){
                                $modelJmpolicyStockCode = new JmpolicyStockCode();
                                $modelJmpolicyStockCode->insure_id = $modelInsure->insure_id;
                                if(!$modelJmpolicyStockCode->save()){
                                    $transaction->rollBack();
                                    throw new HttpException(500, Yii::$app->Helpers->GetErrorModel($modelJmpolicyStockCode));
                                }else{
                                    $save = true;
                                }
                            }else{
                                $save = true;
                            }
                        }
                    }
                }
            }
            if($save){
                $transaction->commit();
                if($dataPost['TInsure']['policy_status_id'] == 1){

                    $urlView      = Yii::$app->request->hostInfo.Url::to(['view-ems', 'id'=>$modelInsure->insure_id]);
                    $printUrl      = Yii::$app->request->hostInfo.Url::to(['print-policy', 'id'=>$modelInsure->insure_id]);

                    $show_receipt = "<script type='text/javascript'>
                                    var urlpdf = ['".$printUrl."'];
                                    function newpagepdf() {
                                        if(urlpdf.length==0) return;
                                        var url = urlpdf.shift();
                                        var openWindow = window.open(url);
                                        setTimeout(function () {
                                            newpagepdf();
                                        }, 100);
                                    }
                                    newpagepdf();
                                    setTimeout(function () {
                                    location.href = '".$urlView."';
                                    }, 500);
                                    </script>";

                    return $show_receipt;
                }else{
                    return $this->redirect(['search-policy']);
                }
            }
        }

        return $this->render('update-managepolicy', [
            'modelInsureForm'               => $modelInsureForm,
            'modelInsure'                   => $modelInsure,
            'modelTInsurePayment'           => $modelTInsurePayment,
            'dataProviderInsureItemDetail'  => $dataProviderInsureItemDetail,
            'dataProviderJmpolicyStatusLog' => $dataProviderJmpolicyStatusLog
        ]);
    }
    
    public function actionPrintPolicy($id)
    {   
        $modelInsure = $this->findModelTInsure($id);

        $date_verify = empty($modelInsure->tInsureCar->date_verify)?'-':date('d-m-Y', strtotime($modelInsure->tInsureCar->date_verify));
        $registry_date = empty($modelInsure->tInsureCar->registry_date)?'-':date('d-m-Y', strtotime($modelInsure->tInsureCar->registry_date)); 
        if(!empty($modelInsure)){
            $model = array(
                'barcode'       => empty($modelInsure->policyStockCode->stock_code)?'-':$modelInsure->policyStockCode->stock_code,
                'date_verify'   => $date_verify,
                'registry_date' => $registry_date,
                'car_code'      => empty($modelInsure->tInsureCar->code)?'-':$modelInsure->tInsureCar->code.' '.$modelInsure->tInsureCar->province_code
            );

            $content = $this->renderPartial('_print-policy-pdf', [
                'model' => $model,
            ]);
            
            $title = 'print-sticker-pdf';
           // setup kartik\mpdf\Pdf component
           $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8, 
            // A4 paper format
            // 'format' => Pdf::FORMAT_A4, 
            'format' => [70 , 30.5],//กำหนดขนาด
            'marginLeft' => 3,
            'marginRight' => 2,
            'marginTop' => 1,
            'marginBottom' => 1,
            'marginHeader' => false,
            'marginFooter' => false,

            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            /*'cssInline' => '.kv-heading-2{font-size:22px}
                            .table-border-commen {border: 2px solid #ddd;}
                          
                            .table-line-double {border-bottom: 1.7px double #999999;}
                            .margin-bottom-10 {margin-bottom: 10px!important;}
                            .margin-bottom-20 {margin-bottom: 20px!important;}
                            .margin-bottom-40 {margin-bottom: 40px!important;}
                            .wrap > .container {padding: 0 15px;}
                            .table-border-order {border: 1px solid #b3b3b3;}
                            .table-pad{padding: 3px;padding-left: 4px;}
                            body{font-family: "Garuda"}
                             @media print{
                            .page-break{display: block;page-break-before: always;}
                            }
                            ',*/
            'cssInline' => 'body{font-family: "Garuda";font-size:11px}',
             // set mPDF properties on the fly
            'options' => ['title' => $title],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>false,
                'SetFooter'=>false,
            ],
            'filename' => $title.'.pdf',
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render(); 
        }

    }

    public function actionSendEms()
    {
        $searchModel = new TInsureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-mail', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewEms($id)
    {
        $model = TInsure::find()->where(['insure_id' => $id])->one();
        $modelInsureForm = TInsureForm::find()->where(['insure_id' => $model->insure_id])->one();

        $searchModelInsureItemDetail = new TInsureItemDetailSearch();
        $Params['TInsureItemDetailSearch']['order_extra_id'] = $model->insure_id;
        $dataProviderInsureItemDetail = $searchModelInsureItemDetail->search($Params);

        $modelMember =  Tmember::find()->where(['member_id' => Yii::$app->user->identity->id])->one();
        $modelMailItemDetail = TInsureMailItemDetail::find()->where(['insure_id' => $model->insure_id])->one();
        if(empty($modelMailItemDetail)) {
            $modelMailItemDetail = new TInsureMailItemDetail();
        }

        $modelMailItem = TInsureMailItem::find()->where(['insure_mail_item_id' => $modelMailItemDetail->insure_mail_item_id])->one();
        if(empty($modelMailItem)) {
            $modelMailItem = new TInsureMailItem();
        }

        $dataPost = Yii::$app->request->post();
        if(!empty($dataPost)) {
            // echo '<pre>'; print_r($dataPost); exit;
            $transaction = Yii::$app->db_buzz_control->beginTransaction();
            try
            {
                if(!empty($dataPost['TInsureMailItem'])) {
                    $modelMailItem = new TInsureMailItem();
                    $modelMailItem->mail_type = "นำส่งลูกค้า";
                    $modelMailItem->from_company = Yii::$app->user->identity->company_id;
                    $modelMailItem->from_department = Yii::$app->user->identity->department;
                    $modelMailItem->from_officer = $dataPost['TInsureMailItem']['from_officer'];
                    $modelMailItem->from_date = date('Y-m-d H:i:m', strtotime($dataPost['TInsureMailItem']['from_date']));
                    $modelMailItem->to_department = 0;
                    $modelMailItem->to_officer = 0;
                    $modelMailItem->to_check = "";
                    $modelMailItem->to_date = "0000-00-00 00:00:00";
                    $modelMailItem->to_remark = "";
                    $modelMailItem->code = $dataPost['TInsureMailItem']['code'];
                    $modelMailItem->from_remark = $dataPost['TInsureMailItem']['from_remark'];
                    if(!$modelMailItem->save()) {
                        $transaction->rollBack();
                        throw new HttpException(500, yii::$app->Helpers->GetErrorModel($modelMailItem));
                    }
                }

                if(!empty($dataPost['TInsureMailItemDetail'])) {
                    $modelMailItemDetail->insure_id = $model->insure_id;
                    $modelMailItemDetail->insure_mail_item_id = $modelMailItem->insure_mail_item_id;
                    $modelMailItemDetail->insure_stock_id = empty($dataPost['TInsureMailItemDetail']['insure_stock_id']) ? 0 : $model->k_stock_id;
                    $modelMailItemDetail->stock_insure_id = empty($dataPost['TInsureMailItemDetail']['stock_insure_id']) ? 0 : $model->p_stock_id;
                    $modelMailItemDetail->bill = empty($dataPost['TInsureMailItemDetail']['bill']) ? '' : "Yes";
                    $modelMailItemDetail->pasee = empty($dataPost['TInsureMailItemDetail']['pasee']) ? '' : "Yes";
                    $modelMailItemDetail->saluk = empty($dataPost['TInsureMailItemDetail']['saluk']) ? '' : "Yes";
                    $modelMailItemDetail->code = empty($dataPost['TInsureMailItemDetail']['code']) ? null : $dataPost['TInsureMailItemDetail']['code'];
                    $modelMailItemDetail->remark = "";
                    $modelMailItemDetail->recieve = 0;
                    $modelMailItemDetail->recieve_date = "0000-00-00 00:00:00";
                    $modelMailItemDetail->recieve_name = "";
                    $modelMailItemDetail->recieve_relate = "";
                    $modelMailItemDetail->cancel = 0;
                    $modelMailItemDetail->cancel_date = "0000-00-00 00:00:00";
                    $modelMailItemDetail->cancel_remark = "";
                    $modelMailItemDetail->car_id = 0;
                    $modelMailItemDetail->postcode = "";
                    $modelMailItemDetail->weight = 0;
                    $modelMailItemDetail->price = 0;
                    if(!$modelMailItemDetail->save()) {
                        $transaction->rollBack();
                        throw new HttpException(500, yii::$app->Helpers->GetErrorModel($modelMailItemDetail));
                    }
                }

                // กรมธรรม์
                if(!empty($dataPost['TInsureMailItemDetail']['insure_stock_id'])) {
                    $modelInsureStock = TInsureStock::find()->where(['insure_id' => $model->insure_id])->one();
                    if(!empty($modelInsureStock)) {
                        $modelInsureStock->status = "นำส่งลูกค้า";
                        $modelInsureStock->status_date = date('Y-m-d H:i:m', strtotime($dataPost['TInsureMailItem']['from_date']));
                        $modelInsureStock->status_company_id =  Yii::$app->user->identity->company_id;
                        $modelInsureStock->status_department_id =  Yii::$app->user->identity->department;
                        $modelInsureStock->status_officer_id =  $dataPost['TInsureMailItem']['from_officer'];
                        $modelInsureStock->doc_send =  $modelMailItemDetail->insure_mail_item_detail_id;
                        if(!$modelInsureStock->save()) {
                            $transaction->rollBack();
                            throw new HttpException(500, yii::$app->Helpers->GetErrorModel($modelInsureStock));
                        }
                    }

                    $model->k_send = $modelMailItemDetail->insure_mail_item_detail_id;
                    $model->k_send_date = date('Y-m-d H:i:m', strtotime($dataPost['TInsureMailItem']['from_date']));
                    $model->k_send_code = empty($dataPost['TInsureMailItemDetail']['code']) ? null : $dataPost['TInsureMailItemDetail']['code'];
                    $model->k_send_officer_id = $dataPost['TInsureMailItem']['from_officer'];
                    $model->k_number = empty($model->k_number) ? "" : $model->k_number;
                    $model->k_kom_status = empty($model->k_kom_status) ? "" : $model->k_kom_status;
                    if(!$model->save()) {
                        $transaction->rollBack();
                        throw new HttpException(500, yii::$app->Helpers->GetErrorModel($model));
                    }
                }

                // พรบ.
                if(!empty($dataPost['TInsureMailItemDetail']['stock_insure_id'])) {
                    $modelTStockInsure = TStockInsure::find()->where(['stock_insure_id' => $model->p_stock_id])->one();
                    if(!empty($modelTStockInsure)) {
                        $modelTStockInsure->doc_send = $modelMailItemDetail->insure_mail_item_detail_id;
                        if(!$modelTStockInsure->save())
                        {
                            $transaction->rollBack();
                            throw new HttpException(500, yii::$app->Helpers->GetErrorModel($modelTStockInsure));
                        }
                    }

                    $modelTInsureStockPrb = TInsureStockPrb::find()->where(['insure_id' => $model->insure_id])->one();
                    if(!empty($modelTInsureStockPrb)) {
                        $modelTInsureStockPrb->status = "นำส่งลูกค้า";
                        $modelTInsureStockPrb->status_date = date('Y-m-d H:i:m', strtotime($dataPost['TInsureMailItem']['from_date']));
                        $modelTInsureStockPrb->status_company_id = Yii::$app->user->identity->company_id;
                        $modelTInsureStockPrb->status_department_id = Yii::$app->user->identity->department;
                        $modelTInsureStockPrb->status_officer_id = $dataPost['TInsureMailItem']['from_officer'];
                        if(!$modelTInsureStockPrb->save())
                        {
                            $transaction->rollBack();
                            throw new HttpException(500, yii::$app->Helpers->GetErrorModel($modelTInsureStockPrb));
                        }
                    }

                    $model->p_send = $modelMailItemDetail->insure_mail_item_detail_id;
                    $model->p_send_date = date('Y-m-d H:i:m', strtotime($dataPost['TInsureMailItem']['from_date']));
                    $model->p_send_code = empty($dataPost['TInsureMailItemDetail']['code']) ? null : $dataPost['TInsureMailItemDetail']['code'];
                    $model->p_send_officer_id = $dataPost['TInsureMailItem']['from_officer'];
                    $model->k_number = empty($model->k_number) ? "" : $model->k_number;
                    $model->k_kom_status = empty($model->k_kom_status) ? "" : $model->k_kom_status;
                    if(!$model->save()) {
                        $transaction->rollBack();
                        throw new HttpException(500, yii::$app->Helpers->GetErrorModel($model));
                    }
                }

                // Bill
                if(!empty($dataPost['TInsureMailItemDetail']['bill'])) {
                    $model->bill_send = $modelMailItemDetail->insure_mail_item_detail_id;
                    $model->bill_send_date = date('Y-m-d H:i:m', strtotime($dataPost['TInsureMailItem']['from_date']));
                    $model->bill_send_code = empty($dataPost['TInsureMailItemDetail']['code']) ? null : $dataPost['TInsureMailItemDetail']['code'];
                    $model->bill_send_officer_id = $dataPost['TInsureMailItem']['from_officer'];
                    $model->k_number = empty($model->k_number) ? "" : $model->k_number;
                    $model->k_kom_status = empty($model->k_kom_status) ? "" : $model->k_kom_status;
                    if(!$model->save()) {
                        $transaction->rollBack();
                        throw new HttpException(500, yii::$app->Helpers->GetErrorModel($model));
                    }
                }

                // Pasee
                 if(!empty($dataPost['TInsureMailItemDetail']['pasee'])) {
                    $model->pasee_send = $modelMailItemDetail->insure_mail_item_detail_id;
                    $model->pasee_send_date = date('Y-m-d H:i:m', strtotime($dataPost['TInsureMailItem']['from_date']));
                    $model->pasee_send_code = empty($dataPost['TInsureMailItemDetail']['code']) ? null : $dataPost['TInsureMailItemDetail']['code'];
                    $model->pasee_send_officer_id = $dataPost['TInsureMailItem']['from_officer'];
                    $model->k_number = empty($model->k_number) ? "" : $model->k_number;
                    $model->k_kom_status = empty($model->k_kom_status) ? "" : $model->k_kom_status;
                    if(!$model->save()) {
                        $transaction->rollBack();
                        throw new HttpException(500, yii::$app->Helpers->GetErrorModel($model));
                    }
                }

                // Saluk
                if(!empty($dataPost['TInsureMailItemDetail']['saluk'])) {
                    $model->saluk_send = $modelMailItemDetail->insure_mail_item_detail_id;
                    $model->saluk_send_date = date('Y-m-d H:i:m', strtotime($dataPost['TInsureMailItem']['from_date']));
                    $model->saluk_send_code = empty($dataPost['TInsureMailItemDetail']['code']) ? null : $dataPost['TInsureMailItemDetail']['code'];
                    $model->saluk_send_officer_id = $dataPost['TInsureMailItem']['from_officer'];
                    $model->k_number = empty($model->k_number) ? "" : $model->k_number;
                    $model->k_kom_status = empty($model->k_kom_status) ? "" : $model->k_kom_status;
                    if(!$model->save()) {
                        $transaction->rollBack();
                        throw new HttpException(500, yii::$app->Helpers->GetErrorModel($model));
                    }
                }

                $transaction->commit();
                return $this->redirect(['send-ems']);// index-mail
            } catch (ErrorException $e) {
                $transaction->rollBack(); 
                throw new NotFoundHttpException($e->getMessage());
            }

        }
        // echo '<pre>'; print_r($modelMailItemDetail); exit;

        return $this->render('view-mail', [
            'model'               => $model,
            'modelInsureForm'     => $modelInsureForm,
            'modelMember'     => $modelMember,
            'modelMailItem'       => $modelMailItem,
            'modelMailItemDetail' => $modelMailItemDetail,
            'dataProviderInsureItemDetail' => $dataProviderInsureItemDetail,
            // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
        ]);
    }
}
