<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Tumenusubin */

$this->title = 'เพิ่ม Tumenusubin';
$this->params['breadcrumbs'][] = ['label' => 'Tumenusubins', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'เพิ่ม';
?>
<div class="tumenusubin-create">

    <?php echo $this->render('_form', [
        'model' => $model,
        'arrMenu' => $arrMenu,
        'arrMenuSub' => $arrMenuSub,
    ]) ?>

</div>
