<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model common\models\Tumenusubin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tumenusubin-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL, //TYPE_HORIZONTAL
        //'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        //SIZE_TINY = 'xs',SIZE_SMALL = 'sm',SIZE_MEDIUM = 'md',SIZE_LARGE = 'lg'
    ]); ?>

    <div class="card card-custom example example-compact">
    <div class="card-header <?php  echo ($model->isNewRecord)?'bg-success':'bg-warning'?>">
        <span class="card-title">
            <i class="fas <?php  echo ($model->isNewRecord)?'fa-plus-square':'fa-edit'?> text-light"></i>
        </span>
    </div>
    <div class="card-body">
    <div class="container-fluid">


        <div class="row">
            <div class="col-sm-8 col-md-8">
                <?php echo $form->field($model, 'tumenu_id')->widget(Select2::classname(), [
                        'data' => $arrMenu,
                        'language' => 'th',
                        'options' => ['placeholder' => 'ระบุเมนู'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>
            </div>
        </div>
    
        <div class="row">
            <div class="col-sm-8 col-md-8">
                <?php echo $form->field($model, 'tumenusub_id')->widget(DepDrop::classname(), [
                    'data'=> $arrMenuSub,
                    'type' => DepDrop::TYPE_SELECT2,
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['tumenusubin-tumenu_id'],
                        'url'=>Url::to(['/admin/managemenusubin/jsonmenusub']),
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8 col-md-8">
                <?php echo $form->field($model, 'subin_code')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8 col-md-8">
                <?php echo $form->field($model, 'subin_label')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8 col-md-8">
                <?php echo $form->field($model, 'subin_url')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8 col-md-8">
                <?php echo $form->field($model, 'subin_icon')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <?php 
                if ($model->isNewRecord) {
                    echo $form->field($model, 'isactive')->hiddenInput(['value' => true])->label(false);
                } else {
                    echo $form->field($model, 'isactive')->widget(SwitchInput::classname(), ['pluginOptions' => [
                                'size' => 'middle',
                                'onColor' => 'success',
                                'offColor' => 'danger',
                                'onText'=>'ใช้งาน',
                                'offText'=>'ไม่ใช้งาน'
                        ]
                    ]);
                }?>
            </div>
        </div>

    </div>
    </div>
    <div class="card-footer text-right">
        <?php echo Html::submitButton($model->isNewRecord ? 'บันทึก' : 'บันทึกแก้ไข', ['class' =>'btn btn-success']); ?>
    </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
