<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TumenusubinSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'เมนูย่อย 2';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tumenusubin-index">

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=> '',
            'after'=>false,
        ],
        'toolbar'=> [
            [
                'content'=> Html::a('<i class="glyphicon glyphicon-plus"></i> เพิ่มเมนูย่อย 2', ['create'], ['class' => 'btn btn-success'])
            ],
            // '{export}',
            '{toggleData}',
        ],
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'enablePushState' => false,
        ],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'attribute' => 'tumenusub_id',
                'format'=>'raw',
                'value' => function ($model) {
                            return $model->tumenusub->submenu_label;
                        }
            ],
            'subin_code',
            'subin_label',
            'subin_url',
            [
                'attribute' => 'subin_icon',
            ],
            [
                'label' => 'ตัวอย่างเมนู',
                'format'=>'raw',
                'value' => function ($model) { 
                            if ($model->subin_icon != '') {
                                return '<i class="fa fa-'.$model->subin_icon.'"></i> '.$model->subin_label;
                            } else {
                                return $model->subin_label;
                            }
                        }
            ],
            [
                'attribute' => 'isactive',
                'format'=>'raw',
                'value' => function ($model) { 
                    if ($model->isactive) {
                        return '<span class="label label-success">'.$model->isactive_text.'</span>';
                    } else {
                        return '<span class="label label-danger">'.$model->isactive_text.'</span>';
                    }
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'จัดการ',
                'template'=>'{view} {update}',
                'buttons' => [
  
                    'view' => function ($url, $model) {
                        return Html::a('<span class="btn btn-info btn-sm fa fa-eye"></span>', $url, [
                                    'title' => 'แสดง',
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="btn btn-warning btn-sm fa fa-edit"></span>', $url, [
                                    'title' => 'แก้ไข',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    
</div>
