<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TumenusubinSerach */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tumenusubin-search  gutter-b">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="card card-custom">

        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label"><i class="fas fa-search"></i> ค้นหา </h3>
            </div>
        </div>

        <div class="card-body">
        
        <div class="container-fluid"> 

            <?php echo $form->field($model, 'id') ?>

            <?php echo $form->field($model, 'tumenusub_id') ?>

            <?php echo $form->field($model, 'subin_code') ?>

            <?php echo $form->field($model, 'subin_label') ?>

            <?php echo $form->field($model, 'subin_url') ?>

            <?php // echo $form->field($model, 'subin_icon') ?>

            <?php // echo $form->field($model, 'isactive') ?>

            <?php // echo $form->field($model, 'created_at') ?>

            <?php // echo $form->field($model, 'created_by') ?>

            <?php // echo $form->field($model, 'updated_at') ?>

            <?php // echo $form->field($model, 'updated_by') ?>


        </div>

        <div class="card-footer">
            <div class="col-md-12 text-right">
                <?php echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
                <?php echo Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
