<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Tumenusubin */

$this->title = 'แก้ไข Tumenusubin: ' . $model->subin_label;
$this->params['breadcrumbs'][] = ['label' => 'Tumenusubins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->subin_label, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="tumenusubin-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'arrMenu' => $arrMenu,
        'arrMenuSub' => $arrMenuSub,
    ]) ?>

</div>
