<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Tumenusubin */

$this->title = 'รายละเอียดเมนูย่อย 2';
$this->params['breadcrumbs'][] = ['label' => 'เมนูย่อย 2', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'แสดง';
?>
<div class="tumenusubin-view">

    <?php
        echo DetailView::widget([
        'model'      => $model,
        'mode'       => 'view',
        'bordered'   => true,
        'striped'    => false,
        'condensed'  => false,
        'responsive' => true,
        'hover'      => true,
        'hAlign'     =>'right',
        'vAlign'     =>'middle',
        'buttons1' => '',
        'panel'=>[
            'before' => false,
            'type'=>DetailView::TYPE_PRIMARY,
            'heading'=>'<i class="fa fa-list"></i>',
        ],
        'attributes' => [
            'tumenusub_id',
            [
                'attribute'=>'tumenusub_id',
                'value'=>$model->tumenusub->submenu_label,
            ],
            'subin_code',
            'subin_label',
            'subin_url',
            [
                'attribute'=>'subin_icon',
                'format'=>'raw',
                'value'=>($model->subin_icon != '')?'<i class="fa fa-'.$model->subin_icon.'"></i> '.$model->subin_icon:$model->subin_icon,
            ],
            [
                'attribute'=>'isactive',
                'format'=>'raw',
                'value'=>($model->isactive == true)?'<span class="label label-success">'.$model->isactive_text.'</span>':'<span class="label label-danger">'.$model->isactive_text.'</span>',
            ],
        ],
    ]) ?>

</div>
