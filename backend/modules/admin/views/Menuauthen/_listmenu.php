<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

$form = ActiveForm::begin([
	'id' => 'authen-menu-form',
	'method' => 'post',
	'action' => ['/admin/menuauthen/save'],
	'type' => ActiveForm::TYPE_VERTICAL,
]);
$arrOldMainMenu = array();
$getMainMenu    = Yii::$app->Authcontrol->getManageAuthMainMenu($dataPost['comptype_id'], $dataPost['dep_id'], $dataPost['role_id']);
// echo '<pre>';print_r($getMainMenu);exit;
if (!empty($getMainMenu)) {
	foreach ($getMainMenu as $val) {
		$arrOldMainMenu[] = $val['menu_id'];
	}
}

$arrOldSubMenu = array();
$getSubMenu    = Yii::$app->Authcontrol->getManageAuthSubMenu($dataPost['comptype_id'], $dataPost['dep_id'], $dataPost['role_id']);
// echo '<pre>';print_r($getSubMenu);exit;
if (!empty($getSubMenu)) {
	foreach ($getSubMenu as $val) {
		$arrOldSubMenu[] = $val['menu_id'];
	}
}

$arrOldSubinMenu = array();
$getSubinMenu    = Yii::$app->Authcontrol->getManageAuthSubinMenu($dataPost['comptype_id'], $dataPost['dep_id'], $dataPost['role_id']);
// echo '<pre>';print_r($getSubMenu);exit;
if (!empty($getSubinMenu)) {
	foreach ($getSubinMenu as $val) {
		$arrOldSubinMenu[] = $val['menu_id'];
	}
}
?>
<?php echo Html::hiddenInput('comptype_id', $dataPost['comptype_id']); ?>
<?php echo Html::hiddenInput('dep_id', $dataPost['dep_id']); ?>
<?php //echo Html::hiddenInput('pos_id', $dataPost['pos_id']); ?>
<?php echo Html::hiddenInput('role_id', $dataPost['role_id']); ?>
<div class="panel panel-default">
	<div class="panel-body">

		<table class="table table-hover">
			<thead>
				<tr>
					<th class="col-xs-8 col-sm-8 col-md-8">ชื่อเมนู</th>
					<th class="col-xs-4 col-sm-4 col-md-4 text-center">
						<input type="checkbox" id="select_all" style="width:20px; height:20px;">
						เปิดสิทธิ์ให้ใช้งาน</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$modelMenu    = Yii::$app->Authcontrol->getMainMenu()->all();
				foreach ($modelMenu as $valMenu) {
					if (in_array($valMenu->id, $arrOldMainMenu)) {
						$checked = 'checked';
					} else {
						$checked = '';
					}
				?>
					<tr>
						<td class="col-xs-8 col-sm-8 col-md-8">
							<i class="fa fa-<?php echo $valMenu->menu_icon; ?>"></i>
							<?php echo $valMenu->menu_label; ?>
						</td>
						<td class="col-xs-4 col-sm-4 col-md-4 text-center">
							<input type="checkbox" name="maimmenu[]" style="width:25px; height:25px;" value="<?php echo $valMenu->id; ?>" <?php echo $checked; ?>>
						</td>
					</tr>
					<?php
					$modelSubMenu = Yii::$app->Authcontrol->getSubMenu($valMenu->id)->all();
					foreach ($modelSubMenu as $valSubMenu) {
						if (in_array($valSubMenu->id, $arrOldSubMenu)) {
							$checked = 'checked';
						} else {
							$checked = '';
						}
					?>
						<tr>
							<td class="col-xs-8 col-sm-8 col-md-8">
								<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1">
									<i class="fa fa-<?php echo $valSubMenu->submenu_icon; ?>"></i>
									<?php echo $valSubMenu->submenu_label; ?>
								</div>
							</td>
							<td class="col-xs-4 col-sm-4 col-md-4 text-center">
								<input type="checkbox" name="submenu[]" style="width:23px; height:23px;" value="<?php echo $valSubMenu->id; ?>" <?php echo $checked; ?>>
							</td>
						</tr>
						<?php
						$modelSubInMenu = Yii::$app->Authcontrol->getSubInMenu($valSubMenu->id)->all();
						foreach ($modelSubInMenu as $valSubInMenu) {
							if (in_array($valSubInMenu->id, $arrOldSubinMenu)) {
								$checked = 'checked';
							} else {
								$checked = '';
							}
						?>
							<tr>
								<td class="col-xs-8 col-sm-8 col-md-8">
									<div class="col-xs-offset-2 col-sm-offset-2 col-md-offset-2">
										<i class="fa fa-<?php echo $valSubInMenu->subin_icon; ?>"></i>
										<?php echo $valSubInMenu->subin_label; ?>
									</div>
								</td>
								<td class="col-xs-4 col-sm-4 col-md-4 text-center">
									<input type="checkbox" name="subinmenu[]" style="width:23px; height:23px;" value="<?php echo $valSubInMenu->id; ?>" <?php echo $checked; ?>>
								</td>
							</tr>
						<?php } ?>
					<?php } ?>
				<?php } ?>
			</tbody>
		</table>

	</div>

	<div class="panel-footer text-right">
		<?php //echo Html::submitButton('บันทึกข้อมูล', ['class' => 'btn btn-success']) 
		?>
		<?php echo Html::a('<i class="fa fa-save"></i> บันทึกข้อมูล', null, ['class' => 'btn btn-success', 'onclick' => 'saveMenuAuthen();']) ?>
	</div>
</div>

<?php ActiveForm::end() ?>

<script>
	$('#select_all').change(function() {
		var checkboxes = $(this).closest('form').find(':checkbox');
		if ($(this).is(':checked')) {
			checkboxes.prop('checked', true);
		} else {
			checkboxes.prop('checked', false);
		}
	});
</script>