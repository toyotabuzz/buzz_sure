<?php

use yii\helpers\Html;
use kartik\widgets\Select2;

$this->title = 'กำหนดสิทธิ Menu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-authen">
	<div class="panel panel-default">

		<div class="panel-header"></div>

		<div class="panel-body">

			<div class="container-fluid">

				<div class="collapse in" id="collapseConditionSearch">

					<div class="row">
						<div class="col-sm-3 col-md-3">
							<?php
							echo '<label class="control-label">บริษัท:</label>';
							echo Select2::widget([
								'id'            => 'sel_comptype_id',
								'name'          => 'sel_comptype_id',
								'data'          => $arrCompType,
								'options'       => ['placeholder' => 'เลือกบริษัท ...', 'onChange' => 'getMenu()'],
								'pluginOptions' => [
									'allowClear' => true
								],
							]);
							?>
						</div>

						<div class="col-sm-3 col-md-3">
							<?php
							echo '<label class="control-label">หน่วยงาน:</label>';
							echo Select2::widget([
								'id'            => 'sel_department_id',
								'name'          => 'sel_department_id',
								'data'          => $arrDepartment,
								'options'       => ['placeholder' => 'เลือกหน่วยงาน ...', 'onChange' => 'getRole(); getMenu(); '],
								'pluginOptions' => [
									'allowClear' => true
								],
							]);
							?>
						</div>

						<!-- <div class="col-sm-3 col-md-3">
							<?php
							// echo '<label class="control-label">ระดับ:</label>';
							// echo Select2::widget([
							// 	'id'            => 'sel_position_id',
							// 	'name'          => 'sel_position_id',
							// 	'data'          => $arrPosition,
							// 	'options'       => ['placeholder' => 'เลือกระดับ ...', 'onChange' => 'getMenu()'],
							// 	'pluginOptions' => [
							// 		'allowClear' => true
							// 	],
							// ]);
							?>
						</div> -->

						<div class="col-sm-3 col-md-3">
							<?php
							echo '<label class="control-label">หน้าที่:</label>';
							echo Select2::widget([
								'id'            => 'sel_role_id',
								'name'          => 'sel_role_id',
								'data'          => $arrPosition,
								'options'       => ['placeholder' => 'เลือกระดับ ...', 'onChange' => 'getMenu()'],
								'disabled'      => true,
								'pluginOptions' => [
									'allowClear' => true
								],
							]);
							?>
						</div>
					</div>
				</div>

			</div>

		</div>

	</div>

	<div id="divListmenu"></div>
</div>

<script type="text/javascript">
	function getMenu() {
		let comptype_id = $('#sel_comptype_id').val();
		let dep_id = $('#sel_department_id').val();
		let pos_id = $('#sel_position_id').val();
		let role_id = $("#sel_role_id").val();

		if (comptype_id != '' && dep_id != '' && pos_id != '' && role_id != '') {
			// console.log("role_id: " + role_id);
			$.ajax({
				type: 'post',
				url: baseUrl + '/admin/menuauthen/getmenu',
				data: {
					comptype_id: comptype_id,
					dep_id: dep_id,
					pos_id: pos_id,
					role_id: role_id
				},
				dataType: 'html',
				beforeSend: function() {
					LoadingShow();
					$('#divListmenu').fadeOut('slow');
					setTimeout(function() {
						$('#divListmenu').html('');
					}, 150);
				},
				success: function(res) {
					$('#divListmenu').html(res);
					setTimeout(function() {
						$('#divListmenu').fadeIn('slow');
					}, 200);
				},
				complete: function(res) {
					LoadingHide();
				}
			}).fail(function(error) {
				Swal.fire({
					icon: 'error',
					title: 'เกิดข้อผิดพลาด กรุณาแจ้งผู้ดูแลระบบ (' + error.status + ')',
					html: '<p>getMenu :</p><p>' + error.responseText + '</p>',
					confirmButtonColor: '#d33',
					confirmButtonText: 'ปิด',
					allowOutsideClick: false,
					focusConfirm: false,
				});
			});
		} else {
			// console.log('123');
			$('#divListmenu').fadeOut('slow');
			setTimeout(function() {
				$('#divListmenu').html('');
			}, 150);
		}
	}

	function saveMenuAuthen() {
		Swal.fire({
			icon: 'question',
			title: 'ต้องการบันทึกข้อมูลใช่หรือไม่',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'ใช่',
			cancelButtonText: 'ไม่',
			allowOutsideClick: false,
			focusConfirm: false,
		}).then((result) => {
			if (result.value) { // กรณีกดปุ่ม "ตกลง" หรือ confirm
				LoadingShow();
				$('form[id="authen-menu-form"]').submit();
			}
		});
	}

	function getRole() {
		LoadingShow();
		let dep_id = $("#sel_department_id").val();
		if (dep_id != "") {
			$.ajax({
				type: 'post',
				url: baseUrl + '/admin/menuauthen/getrole',
				data: {
					dep_id: dep_id,
				},
				dataType: 'JSON',
				success: function(res) {
					if (res.success) {
						let dataHTML = '<option selected value="">เลือกหน้าที่<option>';
						$.each(res.data, function(index, value) {
							dataHTML += '<option value="' + value.id + '">' + value.role_name + '<option>';
						});
						setTimeout(function() {
							$("#sel_role_id").html(dataHTML);
							$("#sel_role_id").prop('disabled', false);
						}, 300);

					}
				},
				complete: function(res) {
					LoadingHide();
				}
			});
		}
	}
</script>