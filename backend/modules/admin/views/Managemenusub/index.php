<?php
use yii\helpers\Html;
use yii\widgets\Pjax;

use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ManagemenusubSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูล Sub Menu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tumenusub-index">

<?php echo $this->render('_search', [
    'model' => $searchModel,
    'arrMenu' => $arrMenu,]); ?>

<?php Pjax::begin(['id' => 'grid-menu-pjax','timeout'=>5000]) ?>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=> '',
        ],
        'toolbar'=> [
            [
                'content'=> Html::a('<i class="glyphicon glyphicon-plus"></i> เพิ่ม Menu', ['create'], ['class' => 'btn btn-success'])
            ],
            // '{export}',
            '{toggleData}',
        ],
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'enablePushState' => false,
        ],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'attribute' => 'tumenu_id',
                'format'=>'raw',
                'value' => function ($model) {
                            return $model->tumenu_name;
                        }
            ],
            'submenu_code',
            'submenu_label',
            'submenu_url',
            [
                'attribute' => 'submenu_icon',
            ],
            [
                'label' => 'ตัวอย่าง Menu',
                'format'=>'raw',
                'value' => function ($model) { 
                            if ($model->submenu_icon != '') {
                                return '<i class="fa fa-'.$model->submenu_icon.'"></i> '.$model->submenu_label;
                            } else {
                                return $model->submenu_label;
                            }
                        }
            ],
            [
                'attribute' => 'isactive',
                'format'=>'raw',
                'value' => function ($model) { 
                    if ($model->isactive) {
                        return '<span class="label label-success">'.$model->isactive_text.'</span>';
                    } else {
                        return '<span class="label label-danger">'.$model->isactive_text.'</span>';
                    }
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'จัดการ',
                'template'=>'{view} {update}',
                'buttons' => [
  
                    'view' => function ($url, $model) {
                        return Html::a('<span class="btn btn-info btn-sm fa fa-eye"></span>', $url, [
                                    'title' => 'แสดง',
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="btn btn-warning btn-sm fa fa-edit"></span>', $url, [
                                    'title' => 'แก้ไข',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>

<?php Pjax::end() ?>

</div>