<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\ManagemenusubSerach */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tumenusub-search  gutter-b">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_SMALL],
        'options' => ['data-pjax' => true ],
    ]); ?>

    <div class="card card-custom">

        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label"><i class="fas fa-search"></i> ค้นหา </h3>
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-8">   
                    <?php echo $form->field($model, 'tumenu_id')->widget(Select2::classname(), [
                        'data' => $arrMenu,
                        'language' => 'th',
                        'options' => ['placeholder' => 'ระบุเมนู'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>                
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <?php echo $form->field($model, 'submenu_label') ?>                    
                </div>
            </div>
        </div>

        <div class="card-footer text-right">
            <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?php echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>