<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Tumenusub */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tumenusub-form">

    <?php $form = ActiveForm::begin([
		'id' => 'form-submenu', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_SMALL]
	]); ?>

	<div class="card card-custom example example-compact">
	<div class="card-header <?php echo ($model->isNewRecord)?'bg-success':'bg-warning'; ?>">
        <span class="card-title">
            <i class="fas <?php echo ($model->isNewRecord)?'fa-plus-circle':'fa-edit'; ?> text-light"></i>
        </span>
    </div>
	
	<div class="card-body">
		
		<div class="row">
			<div class="col-md-8">
				<?php echo $form->field($model, 'tumenu_id')->widget(Select2::classname(), [
			            'data' => $arrMenu,
			            'language' => 'th',
			            'options' => ['placeholder' => 'ระบุเมนู'],
			            'pluginOptions' => [
			                'allowClear' => true
			            ],
			        ]);?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<?php echo $form->field($model, 'submenu_code')->textInput(['maxlength' => true]) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<?php echo $form->field($model, 'submenu_label')->textInput(['maxlength' => true]) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<?php echo $form->field($model, 'submenu_url')->textInput(['maxlength' => true]) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<?php echo $form->field($model, 'submenu_icon')->textInput(['maxlength' => true]) ?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-8">
				<?php 
	            if ($model->isNewRecord) {
	                echo $form->field($model, 'isactive')->hiddenInput(['value' => true])->label(false);
	            } else {
	                echo $form->field($model, 'isactive')->widget(SwitchInput::classname(), ['pluginOptions' => [
	                            'size' => 'middle',
	                            'onColor' => 'success',
	                            'offColor' => 'danger',
	                            'onText'=>'ใช้งาน',
	                            'offText'=>'ไม่ใช้งาน'
	                    ]
	                ]);
	            }?>
			</div>
		</div>
	</div>
	
	<div class="card-footer text-right">
		<?php echo Html::submitButton($model->isNewRecord ? 'เพิ่มข้อมูล' : 'บันทึกแก้ไข', ['class' => 'btn btn-success','data' => ['confirm' => 'ยืนยันการบันทึกข้อมูล ใช่หรือไม่?']]) ?>
	</div>

	</div>

    <?php ActiveForm::end(); ?>

</div>
