<?php
use yii\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Tumenusub */

$this->title = 'รายละเอียด Sub Menu';
$this->params['breadcrumbs'][] = ['label' => 'Sub Menu', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'รายละเอียด';
?>
<div class="tumenusub-view">

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'tumenu_id',
                'value'=>$model->tumenu_name,
            ],
            'submenu_code',
            'submenu_label',
            'submenu_url',
            'submenu_icon',
        ],
        'buttons1' => '',
        'panel'=>[
            'before' => false,
            'heading'=>'<i class="fa fa-list"></i>',
            'type'=> DetailView::TYPE_PRIMARY,
            'footer' => Html::a('แก้ไข', ['update', 'id' => $model->id], ['class' => 'btn btn-warning', 'data' => ['confirm' => 'คุณต้องการแก้ไข ใช่หรือไม่?','method' => 'get',]]),
            'footerOptions' => [
                'class'=>'panel-footer text-right',
            ]
        ]
    ]) ?>

</div>