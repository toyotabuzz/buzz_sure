<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Tumenusub */

$this->title = 'แก้ไข Sub Menu: ' . $model->submenu_label;
$this->params['breadcrumbs'][] = ['label' => 'Sub Menu', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->submenu_label, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>

    <?php echo $this->render('_form', [
        'model' => $model,
        'arrMenu' => $arrMenu,
    ]) ?>
