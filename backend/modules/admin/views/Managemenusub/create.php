<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Tumenusub */

$this->title = 'เพิ่ม Sub Menu';
$this->params['breadcrumbs'][] = ['label' => 'Sub Menu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <?php echo $this->render('_form', [
        'model' => $model,
        'arrMenu' => $arrMenu,
    ]) ?>

