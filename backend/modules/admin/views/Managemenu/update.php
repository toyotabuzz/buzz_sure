<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Tumenu */

$this->title = 'แก้ไข Menu: ' . $model->menu_label;
$this->params['breadcrumbs'][] = ['label' => 'Menu', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->menu_label, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>

<?php echo $this->render('_form', [
    'model' => $model,
]) ?>
