<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ManagemenuSerach */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="tumenu-search  gutter-b">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        // 'type' => ActiveForm::TYPE_HORIZONTAL,
        // 'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        // 'options' => ['data-pjax' => true ],
    ]); ?>

    <div class="card card-custom">

        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label"><i class="fas fa-search"></i> ค้นหา </h3>
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-sm-3 col-md-3">
                    <?php echo $form->field($model, 'menu_label') ?>                
                </div>
            </div>
        </div>

        <div class="card-footer text-right">
            <?php echo Html::resetButton('<i class="fa fa-undo"></i> Reset', ['class' => 'btn btn-default']) ?>
            <?php echo Html::submitButton('<i class="fa fa-search"></i> Search', ['class' => 'btn btn-primary', 'onclick'=>'LoadingShow();']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
