<?php
use yii\helpers\Html;
use yii\widgets\Pjax;

use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ManagemenuSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูล Menu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tumenu-index">

<?php echo $this->render('_search', ['model' => $searchModel]); ?>

<?php //Pjax::begin(['id' => 'grid-menu-pjax','timeout'=>5000]) ?>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=> '',
            'after'=>false,
        ],
        'toolbar'=> [
            [
                'content'=> Html::a('<i class="glyphicon glyphicon-plus"></i> เพิ่ม Menu', ['create'], ['class' => 'btn btn-success', 'onclick'=>'LoadingShow();'])
            ],
            // '{export}',
            '{toggleData}',
        ],
        // 'pjax'=>true,
        // 'pjaxSettings'=>[
        //     'neverTimeout'=>true,
        //     'enablePushState' => false,
        // ],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'attribute' => 'menu_code',
                // 'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
                'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
            ],
            'menu_label',
            'menu_url',
            [
                'attribute' => 'menu_icon',
            ],
            [
                'label' => 'ตัวอย่าง Menu',
                'format'=>'raw',
                'value' => function ($model) { 
                    if ($model->menu_icon != '') {
                        return '<i class="fa fa-'.$model->menu_icon.'"></i> '.$model->menu_label;
                    } else {
                        return $model->menu_label;
                    }
                }
            ],
            [
                'attribute' => 'isactive',
                // 'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
                'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
                'format'=>'raw',
                'value' => function ($model) { 
                    if ($model->isactive) {
                        return '<span class="label label-success">'.$model->isactive_text.'</span>';
                    } else {
                        return '<span class="label label-danger">'.$model->isactive_text.'</span>';
                    }
                }
            ],
            ['class' => 'kartik\grid\ActionColumn',
                'header' => 'จัดการ',
                'template'=>'{view} {update}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fa fa-eye"></i>', $url, [
                            'title' => 'แสดง',
                            'class' => 'btn btn-info btn-sm',
                            'onclick'=>'LoadingShow();'
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-edit"></i>', $url, [
                            'title' => 'แก้ไข',
                            'class' => 'btn btn-warning btn-sm',
                            'onclick'=>'LoadingShow();'
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>

<?php //Pjax::end() ?>

</div>
