<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\Tumenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tumenu-form">

	<?php $form = ActiveForm::begin([
		'id' => 'form-menu', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
	]); ?>

	<div class="box <?php echo ($model->isNewRecord)?'box-success':'box-warning'; ?>">

	<div class="box-header with-border">
        <span class="pull-right">
            <i class="fa fa-3 <?php echo ($model->isNewRecord)?'fa-plus-circle':'fa-edit'; ?>"></i>
        </span>
    </div>
	
	<div class="box-body">
		<div class="row">
			<div class="col-md-8">
				<?php echo $form->field($model, 'menu_code')->textInput(['maxlength' => true]) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<?php echo $form->field($model, 'menu_label')->textInput(['maxlength' => true]) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<?php echo $form->field($model, 'menu_url')->textInput(['maxlength' => true]) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<?php echo $form->field($model, 'menu_icon')->textInput(['maxlength' => true]) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<?php 
	            if ($model->isNewRecord) {
	                echo $form->field($model, 'isactive')->hiddenInput(['value' => true])->label(false);
	            } else {
	                echo $form->field($model, 'isactive')->widget(SwitchInput::classname(), ['pluginOptions' => [
	                            'size' => 'mini',
	                            'onColor' => 'success',
	                            'offColor' => 'danger',
	                            'onText'=>'ใช้งาน',
	                            'offText'=>'ไม่ใช้งาน'
	                    ]
	                ]);
	            }?>
			</div>
		</div>
		
	</div>
	
	<div class="box-footer text-right">
		<?php echo Html::submitButton('<i class="fa fa-save"></i> '.($model->isNewRecord ? 'เพิ่มข้อมูล' : 'บันทึกแก้ไข'), ['class' =>'btn btn-success','data' => ['confirm' => 'ยืนยันการบันทึกข้อมูล ใช่หรือไม่?']]) ?>
	</div>

	</div>

    <?php ActiveForm::end(); ?>

</div>
