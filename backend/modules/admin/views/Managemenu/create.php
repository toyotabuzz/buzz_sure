<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Tumenu */

$this->title = 'เพิ่ม Menu';
$this->params['breadcrumbs'][] = ['label' => 'Menu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

 <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>
