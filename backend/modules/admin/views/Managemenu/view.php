<?php
use yii\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Tumenu */

$this->title ='ข้อมูล Menu: '. $model->menu_label;
$this->params['breadcrumbs'][] = ['label' => 'Menu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tumenu-view">

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'menu_code',
            'menu_label',
            'menu_url:url',
            'menu_icon',
        ],
        'buttons1' => '',
        'panel'=>[
            'before' => false,
            'heading'=>Html::encode($this->title),
            'type'=> DetailView::TYPE_PRIMARY,
            'footer' => Html::a('<i class="fa fa-save"></i> แก้ไข', ['update', 'id' => $model->id], ['class' => 'btn btn-warning', 'data' => ['confirm' => 'คุณต้องการแก้ไข ใช่หรือไม่?','method' => 'get',]]),
            'footerOptions' => [
                'class'=>'panel-footer text-right',
            ]
        ]
    ]) ?>

</div>
