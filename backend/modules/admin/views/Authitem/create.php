<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AuthItem */

$this->title = 'เพิ่มสิทธิ';
$this->params['breadcrumbs'][] = ['label' => 'จัดการสิทธิ', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'เพิ่ม';
?>
<div class="auth-item-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
