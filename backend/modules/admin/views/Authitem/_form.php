<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-item-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL, //TYPE_HORIZONTAL
        //'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        //SIZE_TINY = 'xs',SIZE_SMALL = 'sm',SIZE_MEDIUM = 'md',SIZE_LARGE = 'lg'
    ]); ?>

    <div class="box <?php  echo ($model->isNewRecord)?'box-success':'box-warning'?>">
    <div class="box-header with-border">
        <i class="fa <?php  echo ($model->isNewRecord)?'fa-plus-square':'fa-edit'?>"></i>
    </div>
    <div class="box-body">
    <div class="container-fluid">

    <div class="row">
        <div class="col-sm-8 col-md-8">
            <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4 col-md-4">
            <?php echo $form->field($model, 'type')->dropDownList(
                $model->arrType, 
                ['prompt'=>'ระบุประเภท'] 
            ); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <?php echo $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <?php echo $form->field($model, 'rule_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <?php echo $form->field($model, 'data')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    </div>
    <div class="box-footer text-right">
        <?php echo Html::submitButton($model->isNewRecord ? 'บันทึก' : 'บันทึกแก้ไข', ['class' =>'btn btn-success']); ?>
    </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
