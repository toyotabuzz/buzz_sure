<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AuthItemSerach */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-item-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="box box-default">
    <div class="box-header with-border"><i class="fa fa-search"></i></div>

    <div class="box-body">
    
    <div class="container-fluid"> 

    <?php echo $form->field($model, 'name') ?>

    <?php echo $form->field($model, 'type') ?>

    <?php echo $form->field($model, 'description') ?>

    <?php echo $form->field($model, 'rule_name') ?>

    <?php echo $form->field($model, 'data') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>


    </div>

    <div class="box-footer">
        <div class="col-md-12 text-right">
            <?php echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            <?php echo Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
