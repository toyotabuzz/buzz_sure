<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AuthItem */

$this->title = 'รายละเอียดสิทธิ';
$this->params['breadcrumbs'][] = ['label' => 'จัดการสิทธิ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-view">

    <?php
        echo DetailView::widget([
        'model'      => $model,
        'mode'       => 'view',
        'bordered'   => true,
        'striped'    => false,
        'condensed'  => false,
        'responsive' => true,
        'hover'      => true,
        'hAlign'     =>'right',
        'vAlign'     =>'middle',
        'panel'=>[
            'type'=>DetailView::TYPE_PRIMARY,
            'heading'=>'<i class="fa fa-list"></i>',
        ],
        'attributes' => [
            [
                'columns' => [
                    [
                        'attribute'=>'name',
                        'valueColOptions'=>['style'=>'width:30%'],
                    ],
                    [
                        'attribute'=>'type', 
                        'value'=>$model->arrType[$model->type],
                        'valueColOptions'=>['style'=>'width:30%'], 
                    ],
                ],
            ],
            'description:ntext',
            'rule_name',
            'data:ntext',
        ],
    ]) ?>

</div>
