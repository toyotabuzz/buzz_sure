<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AuthItemSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'จัดการสิทธิ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-index">

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=> '<i class="fa fa-list"></i>',
            'before'=> false,
            'after'=> false,
        ],
        'toolbar'=> [
            // '{export}',
            //'{toggleData}',
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'name',
            [
                'attribute'=>'type',
                'value'=>function($model){
                    return $model->arrType[$model->type];
                },
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=>[1=>'ROLE', 2=>'PERMISSION'],
                'filterWidgetOptions'=>[
                    'pluginOptions'=>['allowClear'=>true],
                ],
                'filterInputOptions'=>['placeholder'=>'ประเภท'],
                'format'=>'raw',
            ],
            'description:ntext',
            // 'rule_name',
            // 'data:ntext',
            // 'created_at',
            // 'updated_at',

            ['class' => 'kartik\grid\ActionColumn',
                'template'=>'{view} {update}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="btn btn-info btn-xs fa fa-eye"></span>', $url, [
                                    'title' => 'แสดง',
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="btn btn-warning btn-xs fa fa-edit"></span>', $url, [
                                    'title' => 'แก้ไข',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
