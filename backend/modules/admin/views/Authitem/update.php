<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AuthItem */

$this->title = 'แก้ไขสิทธิ: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'จัดการสิทธิ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="auth-item-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
