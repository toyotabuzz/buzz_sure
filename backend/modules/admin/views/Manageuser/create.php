<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Tuuser */

$this->title = 'เพิ่มข้อมูล User';
$this->params['breadcrumbs'][] = ['label' => 'จัดการ User', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tuuser-create">

    <?php echo $this->render('_form', [
        'model' => $model,
        'modelComp' => $modelComp,
        'modelDep' => $modelDep,
        'modelPos' => $modelPos,
        'userrole' => $userrole,
    ]) ?>

</div>
