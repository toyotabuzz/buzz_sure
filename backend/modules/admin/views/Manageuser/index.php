<?php

use yii\helpers\Html;

use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UsermanageSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'จัดการข้อมูล User';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tuuser-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            // 'heading'=> Html::encode($this->title),
        ],
        'toolbar'=> [
            [
                'content'=> Html::a('<i class="glyphicon glyphicon-plus"></i> เพิ่ม User', ['create'], ['class' => 'btn btn-success'])
            ]
            // ,'{export}'
        ],
        'headerRowOptions'=>['class'=>'info'],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            // 'id',
            'username',
            // 'password',
            'emp_no',
            'firstname',
            'lastname',
            'firstname_en',
            'lastname_en',
            'nickname',
            // 'email',
            // 'level_id',
            // 'position_id',
            [
                'attribute'=>'position_id',
                'value'=>function($model){
                    return $model->position_name;
                },
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=>Yii::$app->Utilities->getItemPosition(), 
                'filterWidgetOptions'=>[
                    'pluginOptions'=>['allowClear'=>true],
                ],
                'filterInputOptions'=>['placeholder'=>'ตำแหน่ง'],
                'format'=>'raw',
            ],
            // 'department_id',
            [
                'attribute'=>'department_id',
                'value'=>function($model){
                    return $model->department_name;
                },
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=>Yii::$app->Utilities->getItemDepartment(), 
                'filterWidgetOptions'=>[
                    'pluginOptions'=>['allowClear'=>true],
                ],
                'filterInputOptions'=>['placeholder'=>'หน่วยงาน'],
                'format'=>'raw',
            ],
            // 'branch_id',
            [
                'attribute'=>'branch_id',
                'value'=>function($model){
                    return $model->company_name;
                },
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=>Yii::$app->Utilities->getItemCompanyAll(), 
                'filterWidgetOptions'=>[
                    'pluginOptions'=>['allowClear'=>true],
                ],
                'filterInputOptions'=>['placeholder'=>'สาขา'],
                'format'=>'raw',
            ],
            // 'status',
            // 'create_date',
            // 'create_by',
            // 'update_date',
            // 'update_by',
            [
                'attribute'=>'status',
                'value'=>function($model){
                    if ($model->status == 'Y') {
                        return '<span class="label label-success">ใช้งาน</span>';
                    } else {
                        return '<span class="label label-danger">ไม่ใช้งาน</span>';
                    }                   
                },
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=>['Y'=>'ใช้งาน','N'=>'ไม่ใช้งาน'], 
                'filterWidgetOptions'=>[
                    'pluginOptions'=>['allowClear'=>true],
                ],
                'filterInputOptions'=>['placeholder'=>'สถานะ'],
                'format'=>'raw',
            ],

            ['class' => 'kartik\grid\ActionColumn',
                'mergeHeader'=>true,
                'buttonOptions'=>['class'=>'btn btn-default'],
                'template'=>'{view} {update} {reset-password}',
                'headerOptions' => ['style' => 'width:150px'],
                'buttons' => [  
                    'view' => function ($url, $model) {
                        return Html::a('<span class="fa fa-eye"></span>', $url, [
                                    'title' => 'แสดง',
                                    'class'=>  'btn btn-info btn-xs',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<span class="fa fa-edit"></span>', $url, [
                                    'class'=>  'btn btn-warning btn-xs',
                                    'title' => 'แก้ไข',
                        ]);
                    },  
                    'reset-password' => function ($url, $model) {
                        return Html::a('<span class="fa fa-undo"></span>', $url, [
                                    'title' => 'รีเซ็ตรหัสผ่าน',
                                    'class'=>  'btn btn-primary btn-xs',
                                    'data-method'=>'post',
                                    'data-confirm'=>'คุณต้องการรีเซ็ตรหัสผ่าน ใช่หรือไม่?',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
