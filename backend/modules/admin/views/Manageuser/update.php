<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Tuuser */

$this->title = 'แก้ไขข้อมูล : ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'จัดการ User', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->firstname.' '.$model->lastname, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="tuuser-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'modelComp' => $modelComp,
        'modelDep' => $modelDep,
        'modelPos' => $modelPos,
        'userrole' => $userrole,
    ]) ?>

</div>
