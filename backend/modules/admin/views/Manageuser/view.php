<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Tuuser */

$this->title = 'ข้อมูล : ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Manage User', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .ab {
        background-color: #dff0d8;
        padding: 1px 20px;
        margin: 0px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
    }

    .aa {
        background-color: #dff0d8;
        padding: 1px 20px;
        margin: 0px;
    }

    .aa:last-child {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }

    .data ul {
        list-style: none;
        padding: 0px;
        margin-bottom: 0px;
    }
</style>
<div class="tuuser-view">

    <div class="row">
        <div class="col-sm-9 col-md-10">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php echo DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            // 'id',
                            'username',
                            // 'password',
                            'emp_no',
                            'firstname',
                            'lastname',
                            'firstname_en',
                            'lastname_en',
                            'nickname',
                            // 'level_id',
                            // 'position_id',
                            'position_name',
                            'department_name',
                            // 'department_id',
                            // 'branch_id',
                            'company_name',
                            'tel_no',
                            'mobile_no',
                            'email:email',
                            'extension',
                            // 'status',
                            // 'create_date',
                            // 'create_by',
                            // 'update_date',
                            // 'update_by',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-md-2 text-center">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php
                    echo Html::a('เปลี่ยนรหัสผ่าน', ['change-password', 'id' => $model->id], [
                        'class' => 'btn btn-warning',
                        'data' => [
                            'confirm' => 'คุณต้องการเปลี่ยนรหัสผ่าน ใช่หรือไม่?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-md-2 text-center">
            <div class="panel panel-default">
                <div class="panel-body">
                    <tr class="warning text-center">
                        <?php if (empty($userrole)) { ?>
                            <div class="data">
                                <ul>
                                    <li class="ab">
                                        <h4>หน้าที่</h4>
                                    </li>
                                    <li class="aa" style="background-color: #fcf8e3;">
                                        <h4>ไม่ได้กำหนดหน้าที่</h4>
                                    </li>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="data">
                                <ul>
                                    <li class="ab">
                                        <h4>หน้าที่</h4>
                                    </li>
                                    <?php foreach ($userrole as $keyrole => $valrole) { ?>
                                        <li class="aa" style="background-color: #fcf8e3;">
                                            <h4><?php echo $valrole->role->role_name; ?></h4>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </tr>
                </div>
            </div>
        </div>
    </div>
    <?php //echo Html::a('แก้ไขข้อมูล', ['update', 'id'=>$model->id], ['class' => 'btn btn-primary']);
    ?>
    <?php
    // echo Html::a('ลบข้อมูล', ['delete', 'id' => $model->id], ['class' => 'btn btn-danger',
    //     'data' => [
    //         'confirm' => 'คุณต้องการลบข้อมูลรายการนี้ ใช่หรือไม่?',
    //         'method' => 'post',
    //     ],
    // ]) 
    ?>