<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

$this->title = 'เปลี่ยนรหัสผ่าน';
$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['view', 'id'=>$id]];
$this->params['breadcrumbs'][] = $this->title;
?>


<?php $form = ActiveForm::begin(); ?>

<div class="box box-warning">

<div class="box-header with-border">
    <i class="fa fa-edit"></i>
</div>

<div class="box-body">

    <div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
            <?php echo $form->field($model, 'password', [
                    'addon' => [
                        'prepend' => ['content'=>'<i class="glyphicon glyphicon-lock"></i>'],
                    ],
                ])->passwordInput()->label('รหัสผ่านเดิม');
            ?>
        </div>
        <div class="col-md-12">
            <?php echo $form->field($model, 'new_password', [
                    'addon' => [
                        'prepend' => ['content'=>'<i class="glyphicon glyphicon-lock"></i>'],
                    ],
                ])->passwordInput();
            ?>
        </div>
        <div class="col-md-12">
            <?php echo $form->field($model, 'confirm_password', [
                    'addon' => [
                        'prepend' => ['content'=>'<i class="glyphicon glyphicon-lock"></i>'],
                    ],
                ])->passwordInput();
            ?>
        </div>
    </div>
    
    </div>
    
</div>

<div class="box-footer text-right">
    <?php echo Html::submitButton('บันทึกแก้ไข', ['class' => 'btn btn-success']) ?>
</div>

</div>


<?php ActiveForm::end(); ?>