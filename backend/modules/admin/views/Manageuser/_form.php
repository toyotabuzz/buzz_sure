<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\Tuuser */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    #myModal.modal.right .modal-dialog {
        position: relative;
        float: right;
        margin: auto;
        width: 320px;
        height: 100%;
        -webkit-transform: translate3d(0%, 0, 0);
        -ms-transform: translate3d(0%, 0, 0);
        -o-transform: translate3d(0%, 0, 0);
        transform: translate3d(0%, 0, 0);
    }

    #myModal.modal.right .modal-content {
        height: 100%;
        overflow-y: auto;
        /* position: absolute; */
        bottom: 0px;
        width: 100%;
    }

    /* #myModal.modal.right .modal-body {
        padding: 15px 15px 80px;
    } */

    /*Right*/
    #myModal.modal.right.fade .modal-dialog {
        right: -320px;
        -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
        -o-transition: opacity 0.3s linear, right 0.3s ease-out;
        transition: opacity 0.3s linear, right 0.3s ease-out;
    }

    #myModal.modal.right.fade.in .modal-dialog {
        right: 0;
    }

    /* ----- MODAL STYLE ----- */
    #myModal .modal-content {
        border-radius: 0;
        border: none;
    }

    #myModal .modal-header {
        border-bottom-color: #33b5e5;
        background-color: #33b5e5;
        color: #f6f6f6;
    }

    /* ----- v CAN BE DELETED v ----- */
    body {
        background-color: #78909C;
    }

    /* .bg {
        background: none !important;
    } */

    .switch {
    position: relative;
    display: inline-block;
    width: 50px;
    height: 25px;
    top: 2px
    /* width: 27px;
    height: 53px; */
    }

    .switch input { 
    opacity: 0;
    width: 0;
    height: 0;
    }

    .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ff3547 !important;
    -webkit-transition: .4s;
    transition: .4s;
    }

    .slider:before {
    position: absolute;
    content: "";
    height: 21px;
    width: 21px;
    left: 2px;
    bottom: 2px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
    }

    input:checked + .slider {
    background-color: #00c851 !important;
    }

    input:focus + .slider {
    box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
    -webkit-transform: translateX(26px);
    -ms-transform: translateX(26px);
    transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
    border-radius: 34px;
    }

    .slider.round:before {
    border-radius: 50%;
    }
</style>

<?php $form = ActiveForm::begin([
    'id' => 'form-user',
]);
?>

<div class="col-md-9">
    <div class="box <?php echo ($model->isNewRecord) ? 'box-success' : 'box-warning'; ?>">

        <div class="box-header with-border">
            <h3 class="box-title">รายละเอียด</h3>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <?php echo $form->field($model, 'username', [
                        'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-user"></i>']]
                    ])->textInput(['readonly' => (($model->isNewRecord) ? false : ((Yii::$app->user->identity->department_id == Yii::$app->params['DEP_ADMIN']) ? false : true))]) ?>
                </div>
                <div class="col-md-4"><?php echo $form->field($model, 'emp_no')->textInput(['readonly' => (($model->isNewRecord) ? false : ((Yii::$app->user->identity->department_id == Yii::$app->params['DEP_ADMIN']) ? false : true))])->label('รหัสพนักงาน') ?></div>
            <?php /* ?><div class="col-md-6">
            <?php
            if ($model->isNewRecord) {
                echo $form->field($model, 'password', [
                    'addon' => [
                        'prepend' => ['content' => '<i class="glyphicon glyphicon-lock"></i>'],
                    ],
                ])->passwordInput(['readonly' => true]);
            } else {
                echo $form->field($model, 'password', [
                    'addon' => [
                        'prepend' => ['content' => '<i class="glyphicon glyphicon-lock"></i>'],
                        'append' => [
                            'content' => Html::button('Reset', ['class' => 'btn btn-warning', 'onClick' => 'alert("ยังไม่เปิดใช้งาน")']),
                            'asButton' => true,
                        ],
                    ],
                ])->passwordInput(['readonly' => true]);
            }
            ?>
        </div><?php */ ?>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?php echo $form->field($model, 'branch_id')->widget(Select2::classname(), [
                        'data' => $modelComp,
                        'language' => 'th',
                        'options' => ['placeholder' => 'กรุณาเลือก', 'disabled' => (Yii::$app->user->identity->department_id != 27 && Yii::$app->user->identity->department_id != 25)],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <div class="col-md-4">
                    <?php echo $form->field($model, 'department_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($modelDep, 'id', 'dep_name'),
                        'language' => 'th',
                    'options' => ['placeholder' => 'กรุณาเลือก', /*'onChange' => 'oldRole(' . $model->id . ');'*/],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <div class="col-md-4">
                    <?php echo $form->field($model, 'position_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($modelPos, 'id', 'position_name'),
                        'language' => 'th',
                        'options' => ['placeholder' => 'กรุณาเลือก'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"><?php echo $form->field($model, 'tel_no')->textInput(['maxlength' => true])->label('เบอร์ต่อโต๊ะทำงาน') ?></div>
                <div class="col-md-4">
                    <?php echo $form->field($model, 'call_extension_id')->widget(Select2::classname(), [
                        'data' => Yii::$app->Utilities->getListExtension($model->id),
                        'language' => 'th',
                        'options' => ['placeholder' => 'กรุณาเลือก'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
            </div>
        </div>

        <div class="box-footer">
            <div class="row">
                <div class="col-md-4"><?php echo $form->field($model, 'firstname')->textInput(['maxlength' => true])->label('ชื่อจริง') ?></div>
                <div class="col-md-4"><?php echo $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-4"><?php echo $form->field($model, 'nickname')->textInput(['maxlength' => true]) ?></div>
            </div>

            <div class="row">
                <div class="col-md-4"><?php echo $form->field($model, 'firstname_en')->textInput(['maxlength' => true])->label('ชื่อจริง (Eng.)') ?></div>
                <div class="col-md-4"><?php echo $form->field($model, 'lastname_en')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-4"><?php echo $form->field($model, 'nickname_en')->textInput(['maxlength' => true]) ?></div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <?php echo $form->field($model, 'birth_date')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'ปี ค.ศ. -เดือน-วัน'],
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-md-4"><?php echo $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-4"><?php echo $form->field($model, 'mobile_no')->textInput(['maxlength' => true])->label('เอบร์โทรศัพท์ (มือถือ)') ?></div>
            </div>
        </div>
        
        <?php  if ($model->isNewRecord) { echo $form->field($model, 'status')->hiddenInput(['value' => true])->label(false); } else {?>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-4">
                <?php  echo $form->field($model, 'status')->widget(SwitchInput::classname(), ['pluginOptions' => [
                            'size' => 'middle',
                            'onColor' => 'success',
                            'offColor' => 'danger',
                            'onText' => 'ใช้งาน',
                            'offText' => 'ไม่ใช้งาน'
                        ]]);
                ?>
                </div>
            </div>
        </div>
        <?php } ?>

    </div>
</div>

<div class="col-md-3">
    <div class="box <?php echo ($model->isNewRecord) ? 'box-success' : 'box-warning'; ?>">
        <div class="box-header with-border">
            <h3 class="box-title">หน้าที่</h3>
            <div class="area-btn-print">
                <div style="    right: -5px; top: -23px;">
                    <?php if ($model->isNewRecord) {
                        $button = 'btn btn-success btn-sm';
                    } else {
                        $button = 'btn btn-warning btn-sm';
                    } ?>
                    <?php echo Html::Button('<i class="fa fa-plus-circle"></i> จัดการ', ['class' => $button, 'onclick' => 'ManageRole(' . $model->id . ');']) ?>
                </div>
            </div>
        </div>
        <div class="box-body padding-0">
                <?php if ($model->isNewRecord) { ?>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <table class="table table-bordered table-hover margin-bottom-0">
                                <thead>
                                    <tr class="info">
                                    </tr>
                                </thead>
                                <tbody id="partoption_table_2">
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <table class="table table-bordered table-hover margin-bottom-0">
                                <thead>
                                    <tr class="info">
                                    </tr>
                                </thead>
                                <tbody id="partoption_table_2">
                                    <?php $arrDep = []; foreach ($userrole as $keyrole => $valrole) { $arrDep[] = $valrole->role->dep_id; ?>
                                        <tr class="warning text-center">
                                            <td><button type="button" class="remove-role btn btn-danger btn-xs"  onclick ="removeRole(<?php echo $valrole->role_id; ?>)"><span class="fa fa-trash-o"></span></button><input checked type='checkbox' id='select-role-id-<?php echo $valrole->role_id; ?>' class='RoleID' name='RoleID[]' value='<?php echo $valrole->role_id; ?>' style='margin-left:auto; margin-right:auto; width: 20px;height: 20px; display:none'></td>
                                            <input type='hidden' id='DepID-' <?php echo $valrole->role->dep_id; ?> class='DepID' name='DepID[]' value='<?php echo $valrole->role->dep_id; ?>'>
                                            <td class="vcenter"><?php echo $valrole->role->role_name; ?></td>
                                        </tr>
                                    <?php } //echo '<pre>'; print_r($arrDep); exit; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php } ?>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="box-footer text-right">
        <?php echo Html::a('<i class ="fa fa-save" ></i> ' . ($model->isNewRecord ? 'บันทึกข้อมูล' : 'บันทึกแก้ไข'), null, ['class' => 'btn btn-success', 'onclick' => 'confirmSubmitForm("form-user",this);']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<!-- จัดการหน้าที่ -->
<div class="container demo">
    <!-- Modal -->
    <div class="modal right fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="white-text">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-info-circle" aria-hidden="true"></i> จัดการหน้าที่</h4>
                </div>

                <!-- <div class="modal-body" id="myModalin"> -->
                    <div class="modal-footer flex-center">
                        <p class="modal-subject text-center">
                            
                        </p>
                        <div class="modal-content-1">
                            <?php 
                                // $arrDatas = Yii::$app->Utilities->getListReferance('REFINANCE', 'RECEIPT_CANCEL', false, 'Y');
                                // foreach($arrDatas as $keydata => $valdata) {
                            ?>
                                <div class="form-check mb-4 text-left">
                                <?php $model->manage_department_id = empty($arrDep) ? null : $arrDep; ?>
                                <?php echo $form->field($model, 'manage_department_id')->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map($modelDep, 'id', 'dep_name'),
                                    'language' => 'th',
                                    'options' => ['placeholder' => 'กรุณาเลือก', 'onChange' => 'ManageAddRole(' . $model->id . ');'],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'multiple' => true
                                    ],
                                ])->label('หน่วยงาน'); ?>
                                </div>
                            <?php //} ?>
                        </div>
                        <div class="modal-content-2">
                            <!-- <?php 
                                // $arrDatas = Yii::$app->Utilities->getListReferance('REFINANCE', 'RECEIPT_CANCEL_CREDITNOTE', false, 'Y');
                                // foreach($arrDatas as $keydata => $valdata) {
                            ?>
                                <div class="form-check mb-4 text-left">
                                    <input class="form-check-input" name="group" type="radio" id="radio-<?php //echo$keydata;?>" value=<?php //echo $valdata;?>>
                                    <label class="form-check-label"><?php //echo $valdata; ?></label>
                                </div>
                            <?php //} ?> -->
                            <div class="box-body padding-0">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <table class="table table-bordered table-hover margin-bottom-0">
                                            <thead>
                                                <tr class="info">
                                                </tr>
                                            </thead>
                                            <tbody id="partoption_table">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-center">
                        <div class="text-center">
                            <a class="btn btn-danger" data-dismiss="modal" title="ยกเลิก"><i class="fa fa-remove"></i> ปิด</a>
                            <!-- <a class="btn btn-success" title="ตกลง" id="confirm-cancel-receipt" onclick="confirm()"><i class="fa fa-check"></i> ตกลง</a> -->
                        </div>
                    </div>
                    <?php //echo Html::hiddenInput('receipt_id', null, ['id'=>'receipt-id']); ?>
                <!-- </div> -->

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->
</div>

<script type="text/javascript">
    $(function() {
        $('#myModal').on('hidden.bs.modal', function(e) {
            $('html [id="myModalStyle"]').remove();
        });
    });

    function removeRole(roleId = null) {
        // console.log(role_id);
        $('input[id="select-role-id-'+roleId+'"]').parents('tr').remove();
    }

    function ManageRole(id = null) {
        // $('#receipt-id').val(id);
        ManageAddRole(id);
        $('html').find('head').append('<style id="myModalStyle">.modal, .modal-backdrop{background-color: transparent;}</style>');
        // $('#myModal').modal()
        // .find('.modal-subject')
        // .html('<strong>'+html+'</strong>');
        $('#myModal').modal();
    }

    function ManageAddRole(id = null) {
        var arrSelected =  [];
        $.each($("input[id^='select-role-id-']"), function(key, value) {
            // console.log(key);
            // console.log($(value).attr('value'));
            arrSelected.push(parseInt($(value).attr('value')));
        });
          console.log(arrSelected);
        var department_id = $("#tuuser-manage_department_id").val();

        if (department_id == '') {
            $('#partoption_table').html('');
            return false;
        } else {
            $.ajax({
                url: baseUrl + '/admin/manageuser/manage-role',
                type: 'post',
                dataType: "json",
                data: {
                    user_id: id,
                    department_id: department_id,
                },
                beforeSend: function() {
                    LoadingShow();
                },
                success: function(res) {
                    // console.log(res);
                    if (res.status) {
                        if (res['data'] != '') {
                            var partoption_data = "";
                            $.each(res['data'], function(key, value) {
                                // console.log(key, value);
                                partoption_data += "<div class='info vleft'><label class='bg-gray' style='font-size: 16px;padding: 2px 10px;margin-bottom: 10px; margin-top: 10px;'>"+ value.dep_name +"</label></div>";
                                $.each(value.data_list, function(itemkey, vData) {
                                    // console.log(vData.id+' '+arrSelected+' '+$.inArray(vData.id, arrSelected));
                                    partoption_data += "<tr class='warning text-center'>";
                                    partoption_data += "<td ><label class='switch'><input type='checkbox' id='DataRoleID-" + vData.id + "'' class='RoleID' name='RoleID[]' value='" + vData.id + "' style='margin-left:auto; margin-right:auto; width: 20px;height: 20px;' " + ((vData.isSelect != '0' || ($.inArray(vData.id, arrSelected) !== -1)) ? 'checked' : '') + " data-roleid='"+vData.id+"' data-userid='"+value.user_id+"' data-rolename='"+vData.role_name+"' data-depid='"+vData.dep_id+"' onclick ='switchRole(this);'><span class='slider round'></span> </label></td>";
                                    partoption_data += "<input type='hidden' id='DepID-" +  vData.id + "'' class='DepID' name='DepID[]' value='" +  vData.dep_id + "'>";
                                    partoption_data += "<td class='vcenter'>" +  vData.role_name + "</td>";
                                    partoption_data += "</tr>";
                                });
                            });
                            $('#partoption_table').html(partoption_data);
                        }
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Fail !',
                            html: `Message <pre><code>${res.text}</code></pre>`,
                            confirmButtonColor: '#d33',
                            confirmButtonText: 'ปิด',
                            allowOutsideClick: false,
                            focusConfirm: false,
                        }).then((result) => {
                            return false;
                            // Swal.fire({
                            //     title: 'Loading',
                            //     text: 'Please Wait',
                            //     timer: location.reload(),
                            //     allowOutsideClick: false,
                            //     focusConfirm: false,
                            //     onOpen: () => {
                            //         swal.showLoading();
                            //     }
                            // })
                        });
                    }
                },
                complete: function() {
                    LoadingHide();
                },
            }).fail(function(error) {
                Swal.fire({
                    icon: 'error',
                    title: 'ขออภัย! ระบบทำงานผิดพลาด กรุณาแจ้งผู้ดูแลระบบ',
                    confirmButtonColor: '#d33',
                    confirmButtonText: 'ปิด',
                    allowOutsideClick: false,
                    focusConfirm: false,
                });
            });
        }
    }

    function switchRole(elem) {
        var roleId = $(elem).data('roleid');
        var userId = $(elem).data('userid');
        var depId = $(elem).data('depid');
        var roleName = $(elem).data('rolename');
        // console.log(roleId+' '+userId+' '+roleName);
        if ($(elem).is(':checked')) {
            var partoption_data = "";

            partoption_data += "<tr class='warning text-center'>";
            partoption_data += "<td><button type='button' class='remove-role btn btn-danger btn-xs' onclick ='removeRole("+ roleId +")'><span class='fa fa-trash-o'></span></button><input type='checkbox' id='select-role-id-" +roleId + "'' class='RoleID' name='RoleID[]' value='" +roleId + "' style='margin-left:auto; margin-right:auto; width: 20px;height: 20px; display:none' checked></td>";
            partoption_data += "<input type='hidden' class='DepID' name='DepID[]' value='" + depId + "'>";
            partoption_data += "<td class='vcenter'>" + roleName + "</td>";
            // partoption_data += "<input type='hidden' id='RoleID-"+roleId + "'>";
            partoption_data += "</tr>";

            $('#partoption_table_2').append(partoption_data);
        } else {
            // $('input[id="RoleID-'+roleId+'"]').parents('tr').remove();
            $('input[id="select-role-id-'+roleId+'"]').parents('tr').remove();
        }
    }
</script>