<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\tupermission */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tupermission-form">

    <?php $form = ActiveForm::begin([
        'id' => empty($from) ? 0 : $from,
        'type' => ActiveForm::TYPE_VERTICAL, //TYPE_HORIZONTAL
        //'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        //SIZE_TINY = 'xs',SIZE_SMALL = 'sm',SIZE_MEDIUM = 'md',SIZE_LARGE = 'lg'
    ]); ?>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <?php echo $form->field($model, 'dep_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($modelDep, 'id', 'dep_name'),
                'language' => 'th',
                'options' => ['placeholder' => 'เลือก...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <?php echo $form->field($model, 'role_name')->textInput() ?>
        </div>
    </div>
    <?php echo $form->field($model, 'id',['options' => ['style' => 'display: none;']])->hiddenInput()->label(false) ?>

    <?php ActiveForm::end(); ?>