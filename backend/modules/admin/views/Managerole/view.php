<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\tupermission */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Turole', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tupermission-view">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php     /***
    **GROUP COLLUMN
    [
        'columns' => [
            [
                'attribute'=>'field_name',
                'value'=>$model->field_name,
                'valueColOptions'=>['style'=>'width:30%'],
            ],
            [
                'attribute'=>'field_name', 
                'format'=>'raw',
                'valueColOptions'=>['style'=>'width:30%'], 
            ],
        ],
    ]
    */
        echo DetailView::widget([
        'model'      => $model,
        'mode'       => 'view',
        'bordered'   => true,
        'striped'    => false,
        'condensed'  => false,
        'responsive' => true,
        'hover'      => true,
        'hAlign'     =>'right',
        'vAlign'     =>'middle',
        'panel'=>[
            'type'=>DetailView::TYPE_PRIMARY,
            'heading'=>'<div class="row"><div class="col-md-6 text-left"><i class="fa fa-list"></i></div><div class="col-md-6 text-right">'.Html::a('<i class="fa fa-edit"></i> แก้ไขข้อมูล', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-sm']).'</div></div>',
        ],
        'buttons1' => '',
        'attributes' => [
            'id',
            //'agency',
            //'duty_position',
            'isactive',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
        ],
    ]) ?>

</div>
