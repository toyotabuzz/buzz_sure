<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap4\Modal;
//backend\assets\AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model common\models\TuroleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="turole-search gutter-b">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label"><i class="fa fa-search"></i>
            </div>
        </div>
        <div class="card-body">
                <div class="row">
                    <div class="col-sm-3 col-md-3">
                        <?php
                        $modelDep  = \common\models\Tudep::find()->orderBy('dep_name')->asArray()->all();
                        echo $form->field($model, 'dep_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map($modelDep, 'id', 'dep_name'),
                            'language' => 'th',
                            'options' => ['placeholder' => 'เลือก...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                    </div>
                    <!-- <div class="col-sm-3 col-md-3">
                        <?php echo $form->field($model, 'role_name') ?>
                    </div> -->
                </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-sm-6 col-md-6 text-left">
                    <?php echo Html::Button('<i class="fa fa-plus"></i> เพิ่มข้อมูล', ['class' => 'btn btn-success', 'onclick' => 'Create();']) ?>
                </div>
                <div class="col-sm-6 col-md-6 text-right">
                    <?php echo Html::submitButton('<i class="fa fa-search"></i> ค้นหา', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>


<!-- Modal Create -->
<?php
Modal::begin([
    'id' => 'create-model',
    'title' => '<h4 class="modal-title">เพิ่มรายการ</h4>',
    'options' => ['tabindex' => false],
    'size' => Modal::SIZE_SMALL,
    'closeButton'   => ['label' => '<i class="fa fa-remove"></i> ปิด', 'class' => 'btn btn-danger btn-sm', 'style' => 'float: right;'],
    'clientOptions' => [
        'backdrop' => 'static',
        'keyboard' => false,
    ],
    'footer' => Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-success', 'onClick' => 'Createdata();']),
]);
Modal::end();
?>

<script type="text/javascript">
    function Create() {
        LoadingShow();
        $.ajax({
            url: baseUrl + '/admin/managerole/create',
            type: 'post',
            success: function(data) {
                $('#update-model .modal-body').html('');
                $('#create-model .modal-body').html(data);
                $("#create-model").on('shown.bs.modal', function() {
                    LoadingHide();
                }).modal('show');
            }
        });
    }

    function Createdata() {
        var agency = $("#turole-dep_id").val();
        var duty_position = $("#turole-role_name").val();

        if (agency == '' || duty_position == '') {
            Swal.fire({
                title: 'เตือน',
                html: 'กรุณา ระบุหน่วยงานและหน้าที่',
                icon: 'warning',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'ยืนยัน',
                showCancelButton: false,
                cancelButtonColor: '#d33',
                cancelButtonText: 'ยกเลิก',
                allowOutsideClick: false,
                focusConfirm: false,
            }).then((result) => {
                return false;
            });
        } else {
            $.ajax({
                url: baseUrl + '/admin/managerole/create',
                type: 'post',
                dataType: "json",
                data: {
                    agency: agency,
                    duty_position: duty_position,
                },
                beforeSend: function() {
                    LoadingShow();
                },
                success: function(res) {
                    if (res.status) {
                        Swal.fire({
                            icon: 'success',
                            title: 'All done !',
                            html: `Success <pre><code>${res.text}</code></pre>`,
                            confirmButtonColor: '#00a65a',
                            confirmButtonText: 'ตกลง',
                            allowOutsideClick: false,
                            focusConfirm: false,
                        }).then((result) => {
                            Swal.fire({
                                title: 'Loading',
                                text: 'Please Wait',
                                timer: location.reload(),
                                allowOutsideClick: false,
                                focusConfirm: false,
                                onOpen: () => {
                                    swal.showLoading();
                                }
                            })
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Fail !',
                            html: `Message <pre><code>${res.text}</code></pre>`,
                            confirmButtonColor: '#d33',
                            confirmButtonText: 'ปิด',
                            allowOutsideClick: false,
                            focusConfirm: false,
                        }).then((result) => {
                            Swal.fire({
                                title: 'Loading',
                                text: 'Please Wait',
                                timer: location.reload(),
                                allowOutsideClick: false,
                                focusConfirm: false,
                                onOpen: () => {
                                    swal.showLoading();
                                }
                            })
                        });
                    }
                },
                complete: function() {
                    LoadingHide();
                },
            }).fail(function(error) {
                Swal.fire({
                    icon: 'error',
                    title: 'ขออภัย! ระบบทำงานผิดพลาด กรุณาแจ้งผู้ดูแลระบบ',
                    confirmButtonColor: '#d33',
                    confirmButtonText: 'ปิด',
                    allowOutsideClick: false,
                    focusConfirm: false,
                });
            });
        }
    }
</script>