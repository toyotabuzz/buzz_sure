<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\turole */

$this->title = 'เพิ่ม Turole';
$this->params['breadcrumbs'][] = ['label' => 'Turole', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'เพิ่ม';
?>
<div class="turole-create">

    <?php echo $this->render('_form', [
        'from' => $from,
        'model' => $model,
        'modelDep' => $modelDep,
    ]) ?>

</div>
