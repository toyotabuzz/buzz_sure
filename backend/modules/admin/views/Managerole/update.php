<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\turole */

$this->title = 'แก้ไข Turole: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Turole', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="tupermission-update">

    <?php echo $this->render('_form', [
        'from' => $from,
        'model' => $model,
        'modelDep' => $modelDep,
    ]) ?>

</div>
