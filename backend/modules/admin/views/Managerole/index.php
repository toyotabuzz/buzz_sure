<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\bootstrap4\Modal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TuroleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'จัดการหน้าที่';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="turole-index">

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],

        [
            'attribute' => 'dep_id',
            'value' => function ($model) {
                return $model->dep->dep_name;
            },
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
        ],
        [
            'attribute' => 'role_name',
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
        ],
        [
            'attribute' => 'isactive',
            'format' => 'raw',
            'value' => function ($model) {
                if ($model->isactive == 'Y') {
                    return '<span class="label label-lg label-light-success label-inline">เปิด</span>';
                } else {
                    return '<span class="label label-lg label-light-danger label-inline">ปิด</span>';
                }
            },
            'headerOptions' => ['class' => 'kv-align-middle kv-align-center'],
            'contentOptions' => ['class' => 'kv-align-middle kv-align-center'],
        ],
        // [
        //     'attribute'=>'created_at',
        //     'vAlign'=>'middle',
        //     'hAlign'=>'left',
        // ],
        // 'created_by',
        // 'updated_at',
        // 'updated_by',

        [
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{update} {confirmdelete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<i class="fa fa-edit text-warning"></i>', $url, [ 
                        'title' => 'แก้ไข',
                        'class' => 'btn btn-icon btn-light btn-hover-warning  btn-sm',
                    ]);
                },

                'confirmdelete' => function ($url, $model) {
                    if ($model->isactive == 'Y') {
                        return Html::a('<i class="fa fa-ban text-danger"></i>', $url, [
                            'title' => 'ปิดใช้งาน',
                            'class' => 'btn btn-icon btn-light btn-hover-danger btn-sm',
                            'data' => [
                                'confirm' => 'คุณต้องการปิดใช้งานรายการนี้ ใช่หรือไม่',
                                'data' => 'post',
                            ],
                        ]);
                    } else {
                        return Html::a('<i class="fa fa-check-circle-o text-success"></i>', $url, [
                            'title' => 'เปิดใช้งาน',
                            'class' => 'btn btn-icon btn-light btn-hover-success btn-sm',  
                            'data' => [
                                'confirm' => 'คุณต้องการเปิดใช้งานรายการนี้ ใช่หรือไม่',
                                'data' => 'post',
                            ],
                        ]);
                    }
                },
            ],
        ],
    ]; ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        //filterModel' => $searchModel,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<i class="fa fa-list"></i>',
            'before' => false,
            'after' => false,
        ],
        'toolbar' => [
            'content' => ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'fontAwesome' => true,
                'target' => ExportMenu::TARGET_BLANK,
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_PDF  => false,
                ],
                'filename' => 'export_data_' . date('Ymd'),
            ]),
            //'{toggleData}',
        ],
        'bordered' => true,
        'striped' => true,
        'responsive' => false,
        'responsiveWrap' => false,
        'floatHeader' => false,
        // 'floatOverflowContainer'=>true, //only set floatHeader = true for fix header
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false,        
        'headerRowOptions' => ['class' => 'info'],
        'filterRowOptions' => ['class' => 'info'],
        'columns' => $gridColumns,
    ]); ?>
</div>
<?php
Modal::begin([
    'id' => 'update-model',
    'title' => '<h4 class="modal-title">แก้ไขข้อมูล</h4>',
    'options' => ['tabindex' => false],
    'size' => Modal::SIZE_SMALL,
    'closeButton'   => ['label' => '<i class="fa fa-remove"></i> ปิด', 'class' => 'btn btn-danger btn-sm', 'style' => 'float: right;'],
    'clientOptions' => [
        'backdrop' => 'static',
        'keyboard' => false,
    ],
    'footer' => Html::button('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-success', 'onClick' => 'Editsavedata();']),
]);
Modal::end();
?>
<script type="text/javascript">
    $(function() {
       
        $('.update-data').on('click', function(e) {
            e.preventDefault();
            $.LoadingOverlay("show");
            $('#create-model .modal-body').html('');
            $('#update-model').modal('show')
                .find('.modal-body')
                .html('');
            $("#update-model").modal("show")
                .find(".modal-body")
                .load($(this).attr('href'));

            $("#update-model").on('shown.bs.modal', function() {
                LoadingHide();
            });
        });
    });

    function Editsavedata() {
        var id = $("#turole-id").val();
        var agency = $("#turole-dep_id").val();
        var duty_position = $("#turole-role_name").val();

        if (agency == '' || duty_position == '') {
            Swal.fire({
                title: 'เตือน',
                html: 'กรุณา ระบุหน่วยงานและหน้าที่',
                icon: 'warning',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'ยืนยัน',
                showCancelButton: false,
                cancelButtonColor: '#d33',
                cancelButtonText: 'ยกเลิก',
                allowOutsideClick: false,
                focusConfirm: false,
            }).then((result) => {
                return false;
            });
        } else {
            $.ajax({
                url: baseUrl + '/admin/managerole/update?id='+id,
                type: 'post',
                dataType: "json",
                data: {
                    agency: agency,
                    duty_position: duty_position,
                },
                beforeSend: function() {
                    LoadingShow();
                },
                success: function(res) {
                    if (res.status) {
                        Swal.fire({
                            icon: 'success',
                            title: 'All done !',
                            html: `Success <pre><code>${res.text}</code></pre>`,
                            confirmButtonColor: '#00a65a',
                            confirmButtonText: 'ตกลง',
                            allowOutsideClick: false,
                            focusConfirm: false,
                        }).then((result) => {
                            Swal.fire({
                                title: 'Loading',
                                text: 'Please Wait',
                                timer: location.reload(),
                                allowOutsideClick: false,
                                focusConfirm: false,
                                onOpen: () => {
                                    swal.showLoading();
                                }
                            })
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Fail !',
                            html: `Message <pre><code>${res.text}</code></pre>`,
                            confirmButtonColor: '#d33',
                            confirmButtonText: 'ปิด',
                            allowOutsideClick: false,
                            focusConfirm: false,
                        }).then((result) => {
                            Swal.fire({
                                title: 'Loading',
                                text: 'Please Wait',
                                timer: location.reload(),
                                allowOutsideClick: false,
                                focusConfirm: false,
                                onOpen: () => {
                                    swal.showLoading();
                                }
                            })
                        });
                    }
                },
                complete: function() {
                    LoadingHide();
                },
            }).fail(function(error) {
                Swal.fire({
                    icon: 'error',
                    title: 'ขออภัย! ระบบทำงานผิดพลาด กรุณาแจ้งผู้ดูแลระบบ',
                    confirmButtonColor: '#d33',
                    confirmButtonText: 'ปิด',
                    allowOutsideClick: false,
                    focusConfirm: false,
                });
            });
        }
    }
</script>