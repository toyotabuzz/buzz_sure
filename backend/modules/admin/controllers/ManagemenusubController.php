<?php

namespace backend\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use common\models\Tumenu;
use common\models\Tumenusub;
use common\models\ManagemenusubSerach;
/**
 * ManagemenusubController implements the CRUD actions for Tumenusub model.
 */
class ManagemenusubController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Tumenusub models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ManagemenusubSerach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $arrMenu = ArrayHelper::map(Tumenu::find()->orderBy(['menu_code'=>SORT_ASC])->all(), 'id', 'menu_label');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'arrMenu' => $arrMenu,
        ]);
    }

    /**
     * Displays a single Tumenusub model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tumenusub model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model   = new Tumenusub();
        $arrMenu = ArrayHelper::map(Tumenu::find()->orderBy(['menu_code'=>SORT_ASC])->all(), 'id', 'menu_label');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'arrMenu' => $arrMenu,
            ]);
        }
    }

    /**
     * Updates an existing Tumenusub model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model   = $this->findModel($id);
        $arrMenu = ArrayHelper::map(Tumenu::find()->all(), 'id', 'menu_label');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'arrMenu' => $arrMenu,
            ]);
        }
    }

    /**
     * Deletes an existing Tumenusub model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tumenusub model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tumenusub the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tumenusub::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
