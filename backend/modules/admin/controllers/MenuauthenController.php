<?php

namespace backend\modules\admin\controllers;

use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\filters\AccessControl;

use common\models\TuauthMainMenu;
use common\models\TuauthSubMenu;
use common\models\TuauthSubinMenu;
use common\models\TuroleSearch;

class MenuauthenController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'getmenu', 'getrole', 'save'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
	}
	
	public function actionIndex()
	{

		$arrCompType   = Yii::$app->Utilities->getItemCompanyType();
		$arrDepartment = Yii::$app->Utilities->getItemDepartment();
		$arrPosition   = Yii::$app->Utilities->getItemPosition();

		return $this->render('index', [
			'arrCompType'   => $arrCompType,
			'arrDepartment' => $arrDepartment,
			'arrPosition'   => $arrPosition,
		]);
	}

	public function actionGetmenu()
	{
		$this->layout = "@backend/views/layouts/blank";

		$dataPost = Yii::$app->request->post();

		return $this->renderAjax('_listmenu', [
			'dataPost' => $dataPost,
		]);
	}

	public function actionGetrole()
	{
		$dataPost          = Yii::$app->request->post();
		$modelTuroleSearch = new TuroleSearch();
		$dataReturn        = $modelTuroleSearch->getRoleByDep($dataPost);

		return json_encode(['success' => true, 'data' => $dataReturn]);
	}

	public function actionSave()
	{
		$dataPost = Yii::$app->request->post();
		// echo "<pre>";
		// print_r($dataPost);
		// echo "</pre>";
		// exit;
		if (!empty($dataPost)) {
			TuauthSubinMenu::deleteAll([
				'comp_type_id'  => $dataPost['comptype_id'],
				'department_id' => $dataPost['dep_id'],
				// 'position_id'   => $dataPost['pos_id'],
				'role_id'       => $dataPost['role_id']
			]);
			TuauthSubMenu::deleteAll([
				'comp_type_id'  => $dataPost['comptype_id'],
				'department_id' => $dataPost['dep_id'],
				// 'position_id'   => $dataPost['pos_id'],
				'role_id'       => $dataPost['role_id']
			]);
			TuauthMainMenu::deleteAll([
				'comp_type_id'  => $dataPost['comptype_id'],
				'department_id' => $dataPost['dep_id'],
				// 'position_id'   => $dataPost['pos_id'],
				'role_id'       => $dataPost['role_id']
			]);

			if (!empty($dataPost['subinmenu'])) {
				foreach ($dataPost['subinmenu'] as $valSubinMenu) {
					$modelAuthSubinMenu                = new TuauthSubinMenu();
					$modelAuthSubinMenu->comp_type_id  = $dataPost['comptype_id'];
					$modelAuthSubinMenu->department_id = $dataPost['dep_id'];
					// $modelAuthSubinMenu->position_id   = $dataPost['pos_id'];
					$modelAuthSubinMenu->role_id       = $dataPost['role_id'];
					$modelAuthSubinMenu->menu_id       = $valSubinMenu;
					if (!$modelAuthSubinMenu->save()) {
						echo '<pre>';
						print_r($modelAuthSubinMenu->getErrors());
						echo '</pre>';
						exit();
					}
				}
			}
			if (!empty($dataPost['submenu'])) {
				foreach ($dataPost['submenu'] as $valSubMenu) {
					$modelAuthSubMenu                = new TuauthSubMenu();
					$modelAuthSubMenu->comp_type_id  = $dataPost['comptype_id'];
					$modelAuthSubMenu->department_id = $dataPost['dep_id'];
					// $modelAuthSubMenu->position_id   = $dataPost['pos_id'];
					$modelAuthSubMenu->role_id       = $dataPost['role_id'];
					$modelAuthSubMenu->menu_id       = $valSubMenu;
					if (!$modelAuthSubMenu->save()) {
						echo '<pre>';
						print_r($modelAuthSubMenu->getErrors());
						echo '</pre>';
						exit();
					}
				}
			}
			if (!empty($dataPost['maimmenu'])) {
				foreach ($dataPost['maimmenu'] as $valMainMenu) {
					$modelAuthMainMenu                = new TuauthMainMenu();
					$modelAuthMainMenu->comp_type_id  = $dataPost['comptype_id'];
					$modelAuthMainMenu->department_id = $dataPost['dep_id'];
					// $modelAuthMainMenu->position_id   = $dataPost['pos_id'];
					$modelAuthMainMenu->role_id       = $dataPost['role_id'];
					$modelAuthMainMenu->menu_id       = $valMainMenu;
					if (!$modelAuthMainMenu->save()) {
						echo '<pre>';
						print_r($modelAuthMainMenu->getErrors());
						echo '</pre>';
						exit();
					}
				}
			}

			Yii::$app->session->setFlash('success', 'บันทึกข้อมูลเรียบร้อย');
			return $this->redirect('index');
		}
	}
}
