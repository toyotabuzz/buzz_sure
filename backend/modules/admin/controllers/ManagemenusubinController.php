<?php

namespace backend\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

use common\models\Tumenu;
use common\models\Tumenusub;
use common\models\Tumenusubin;
use common\models\TumenusubinSerach;

/**
 * ManagemenusubinController implements the CRUD actions for Tumenusubin model.
 */
class ManagemenusubinController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view', 'jsonmenusub'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all Tumenusubin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TumenusubinSerach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tumenusubin model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tumenusubin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tumenusubin();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $arrMenu = ArrayHelper::map(Tumenu::find()->all(), 'id', 'menu_label');
            $arrMenuSub = ArrayHelper::map(Tumenusub::find()->all(), 'id', 'submenu_label');

            return $this->render('create', [
                'model' => $model,
                'arrMenu' => $arrMenu,
                'arrMenuSub' => [],
            ]);
        }
    }

    /**
     * Updates an existing Tumenusubin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        $arrMenu = ArrayHelper::map(Tumenu::find()->all(), 'id', 'menu_label');
        $arrMenuSub = ArrayHelper::map(Tumenusub::find()->all(), 'id', 'submenu_label');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'arrMenu' => $arrMenu,
                'arrMenuSub' => $arrMenuSub,
            ]);
        }
    }

    /**
     * Deletes an existing Tumenusubin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tumenusubin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tumenusubin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tumenusubin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];

        foreach ($datas as $key => $value) {
            if (trim($value->{$fieldId}) != '') {
                array_push($obj, ['id'=>$value->{$fieldId},'name'=>$value->{$fieldName}]);
            }
        }

        return $obj;
    }

    public function actionJsonmenusub()
    {
        $out = [];

         if (isset($_POST['depdrop_parents'])) {
             $parents = $_POST['depdrop_parents'];

             if ($parents != null) {
                 $intMenuID = $parents[0];
                 $out         = $this->MapData(Tumenusub::find()->where(['tumenu_id'=>$intMenuID])->orderBy(['submenu_code'=>SORT_ASC])->all(),'id','submenu_label');

                 return Json::encode(['output'=>$out, 'selected'=>'']);
             }
         }

         return Json::encode(['output'=>'', 'selected'=>'']);
    }
}
