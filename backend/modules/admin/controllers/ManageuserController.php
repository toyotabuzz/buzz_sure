<?php

namespace backend\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use common\models\Tuuser;
use common\models\TuuserRole;
use common\models\UsermanageSerach;
use common\models\Tucomp;
use common\models\Tudep;
use common\models\Tuposition;
use common\models\Turole;
use kartik\widgets\Alert;
use yii\rbac\Role;

/**
 * UsermanageController implements the CRUD actions for Tuuser model.
 */
class ManageuserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view', 'change-password','reset-password', 'manage-role', 'switch-role'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Tuuser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsermanageSerach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (in_array(Yii::$app->user->identity->branch_id, Yii::$app->params['COMP_OUT'])) {
             $dataProvider->query->andWhere(['branch_id'=>Yii::$app->params['COMP_OUT']]);
        }
        // $arrtest = [];
        // $arrtest[] = [""]=>'admin',[""]=>'manageuser',[""]=>'index';
        // echo "<pre>";print_r(var_dump($arrtest));echo "</pre>";
        // echo "<pre>";print_r(var_dump("///"));echo "</pre>";exit;
        // $test = strtr("///", array([""]=>'admin',[""]=>'manageuser',[""]=>'index'));
        // echo "<pre>";print_r($test);echo "</pre>";exit;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tuuser model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model     = $this->findModel($id);
        $userrole  = TuuserRole::find()->where(['user_id' => $id, 'isactive' => 'Y'])->all();
        return $this->render('view', [
            'model' => $model,
            'userrole' => $userrole,
        ]);
    }

    /**
     * Creates a new Tuuser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model     = new Tuuser();
        $userrole  =  new TuuserRole();
        if (in_array(Yii::$app->user->identity->branch_id, Yii::$app->params['COMP_OUT'])) {
            $modelComp = Yii::$app->Utilities->getItemCompanyID(true, 'short_title', 2, 'กรุณาระบุ', 5);
        } else if (Yii::$app->user->identity->department_id == Yii::$app->params['DEP_ADMIN']) {
            // $modelComp = Yii::$app->Utilities->getItemCompanyID(true, 'short_title', null, 'กรุณาระบุ', [1,2]);  
            $comp_location    = array();
            $type_comp        = array();
            $comp_location    = Yii::$app->params['COMP_LOCATION'];
            $comp_location[]  = 2; // 2 = LOCATION_COMP_OUT
            $type_comp        = Yii::$app->params['COMP_TYPE_IN_MANAGE'];
            $type_comp[]      = 5; // 5 = COMP_TYPE_OUT
            $modelComp = Yii::$app->Utilities->getItemCompanyID(false, 'short_title',$comp_location, 'กรุณาระบุ', $type_comp);
        } else {
            $modelComp = Yii::$app->Utilities->getItemCompanyID();
        }
        $modelDep  = Tudep::find()->orderBy('dep_name')->asArray()->all();
        $modelPos  = Tuposition::find()->orderBy('id')->asArray()->all();

        $dataPost = Yii::$app->request->post();
        if ($model->load($dataPost)) {
            // echo '<pre>'; print_r($dataPost); exit;
            if ($model->save()) {
             //   throw new HttpException(500, Yii::$app->Helpers->GetErrorModel($model));
                if(!empty($dataPost['RoleID'])) {
                    foreach ($dataPost['RoleID'] as $kdata => $vdata) {
                        $userroleSave  =  new TuuserRole();
                        $userroleSave->user_id = $model->id;
                        $userroleSave->role_id = $vdata;
                        if (!$userroleSave->save()) {
                            throw new HttpException(500, Yii::$app->Helpers->GetErrorModel($userroleSave));
                        }
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'modelComp' => $modelComp,
            'modelDep' => $modelDep,
            'modelPos' => $modelPos,
            'userrole' => $userrole,
        ]);
    }

    /**
     * Updates an existing Tuuser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model     = $this->findModel($id);
        $userrole  = TuuserRole::find()->where(['user_id' => $id, 'isactive' => 'Y'])->all();
        if (in_array(Yii::$app->user->identity->branch_id, Yii::$app->params['COMP_OUT'])) {
            $modelComp = Yii::$app->Utilities->getItemCompanyID(true, 'short_title', 2, 'กรุณาระบุ', 5);
        } else if (Yii::$app->user->identity->department_id == Yii::$app->params['DEP_ADMIN']) {
            $comp_location    = array();
            $type_comp        = array();
            $comp_location    = Yii::$app->params['COMP_LOCATION'];
            $comp_location[]  = 2; // 2 = LOCATION_COMP_OUT
            $type_comp        = Yii::$app->params['COMP_TYPE_IN_MANAGE'];
            $type_comp[]      = 5; // 5 = COMP_TYPE_OUT
            $modelComp = Yii::$app->Utilities->getItemCompanyID(false, 'short_title',$comp_location, 'กรุณาระบุ', $type_comp);
            //$modelComp = Yii::$app->Utilities->getItemCompanyID(false, 'short_title', Yii::$app->params['COMP_LOCATION'], 'กรุณาระบุ', Yii::$app->params['COMP_TYPE_IN_MANAGE']);
        } else {
            $modelComp = Yii::$app->Utilities->getItemCompanyID();
        }
        $modelDep  = Tudep::find()->orderBy('dep_name')->asArray()->all();
        $modelPos  = Tuposition::find()->orderBy('id')->asArray()->all();

        $dataPost = Yii::$app->request->post();
        if ($model->load($dataPost)) {
            // echo '<pre>'; print_r($dataPost); exit;
            if (!$model->save()) {
                throw new HttpException(500, Yii::$app->Helpers->GetErrorModel($model));
            }

            // เพิ่มสิทธิ์การใช้งาน
            $useRole = [];
            if(!empty($dataPost['RoleID'])) {
                foreach ($dataPost['RoleID'] as $vRoleID) {
                    $modelRole = TuuserRole::find()->where(['role_id' => $vRoleID, 'user_id' => $model->id])->one();
                    if (empty($modelRole)) {
                        $modelRole = new TuuserRole();
                        $modelRole->user_id = $model->id;
                        $modelRole->role_id = $vRoleID;
                    } else {
                        $modelRole->isactive = 'Y';
                    }
                    if (!$modelRole->save()) {
                        throw new HttpException(500, Yii::$app->Helpers->GetErrorModel($modelRole));
                    }
                    $useRole[] = $modelRole->role_id;
                }
            }
            // echo '<pre>'; print_r($useRole); exit;
            TuuserRole::updateAll(['isactive'=>'N', 'updated_at'=>date('Y-m-d H:i:s'), 'updated_by'=>Yii::$app->user->identity->id], ['AND', ['user_id' => $model->id], ['NOT IN', 'role_id', $useRole]]);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelComp' => $modelComp,
            'modelDep' => $modelDep,
            'modelPos' => $modelPos,
            'userrole' => $userrole,
        ]);
    }

    public function actionManageRole()
    {
        $dataPost = Yii::$app->request->post();
        // echo '<pre>'; print_r($dataPost); exit;
        $res = [];
        $Turole = Turole::find()->where(['dep_id' => $dataPost['department_id'], 'isactive' => 'Y'])->all();
        $Tuuser = Tuuser::find()->where(['id'=>$dataPost['user_id'], 'status'=>'Y'])->one();
        if(!empty($Turole)) {
            $res['status'] = true;
            foreach ($Turole as $krole => $vrole ) {
                $res['data'][$vrole->dep_id]['dep_name'] = empty($vrole->dep) ? 'ไม่พบหน่วยงาน' : $vrole->dep->dep_name;
                $res['data'][$vrole->dep_id]['user_id'] = empty($Tuuser) ? null : $Tuuser->id;
                $res['data'][$vrole->dep_id]['data_list'][$krole]['id'] = $vrole->id;
                $res['data'][$vrole->dep_id]['data_list'][$krole]['dep_id'] = $vrole->dep_id;
                $res['data'][$vrole->dep_id]['data_list'][$krole]['role_name'] = $vrole->role_name;
                $res['data'][$vrole->dep_id]['data_list'][$krole]['isactive'] = $vrole->isactive;
                $res['data'][$vrole->dep_id]['data_list'][$krole]['isSelect'] = TuuserRole::find()->where(['user_id'=>$dataPost['user_id'], 'role_id'=>$vrole->id, 'isactive'=>'Y'])->count();
            }
        } else {
            $res['status'] = false;
            $res['text'] = 'ยังไม่มีการเพิ่ม หน้าที่ในหน่วยงานนี้ กรุณาเพิ่มหน้าที่ในหน้ามาสเตอร์';
        }
        // echo "<pre>"; print_r($res); exit;
        return json_encode($res);
    }

    public function actionSwitchRole()
    {
        $dataPost = Yii::$app->request->post();
        // echo '<pre>'; print_r($dataPost); exit;
        $res = [];
        $Turole = Turole::find()->where(['id' => $dataPost['turole_id'], 'isactive' => 'Y'])->all();
        if(!empty($Turole)) {
            $res['status'] = true;
            foreach ($Turole as $krole => $vrole ) {
                $res['data'][$krole]['id'] = $vrole->id;
                $res['data'][$krole]['dep_id'] = $vrole->dep_id;
                $res['data'][$krole]['role_name'] = $vrole->role_name;
                $res['data'][$krole]['isactive'] = $vrole->isactive;
                $res['data'][$krole]['isSelect'] = 1;
                // $res['data'][$krole]['isSelect'] = TuuserRole::find()->where(['user_id'=>$dataPost['user_id'], 'role_id'=>$vrole->id, 'isactive'=>'Y'])->count();
            }
        } else {
            $res['status'] = false;
            $res['text'] = 'เกิดข้อผิดพลาด ไม่พบ หน้าที่ดังที่ถูกใช้งาน';
        }
        // echo "<pre>"; print_r($res); exit;
        return json_encode($res);
    }

    /**
     * Deletes an existing Tuuser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tuuser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tuuser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tuuser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionChangePassword($id)
    {
        $model = new Tuuser();
        $dataPost = Yii::$app->request->post();

        if (!empty($dataPost) && $model->load($dataPost)) {
            $modelOld = $this->findModel($id);
            if (trim($model->password)=='') {
                $model->addError('password','กรุณาระบุรหัสผ่านเดิม');
            } else if (trim($model->new_password)=='') {
                $model->addError('new_password','กรุณาระบุรหัสผ่านใหม่');
            } else if (trim($model->confirm_password)=='') {
                $model->addError('confirm_password','กรุณาระบุยืนยันรหัสผ่าน');
            } else if (trim($model->new_password) != trim($model->confirm_password)) {
                $model->addError('confirm_password','การยืนยันรหัสผ่านไม่ถูกต้อง');
            } else if (md5(trim($model->password)) != $modelOld->password ) {
                $model->addError('password','รหัสผ่านเดิมระบุไม่ถูกต้อง');
            } else {
                $modelOld->password = md5(trim($model->new_password));
                if ($modelOld->save()) {
                    return $this->redirect(['view', 'id' => $modelOld->id]);                    
                }
            }
        }
        
        return $this->render('_form-change-password', ['model'=>$model, 'id'=>$id]);
    }

    public function actionResetPassword($id)
    {
        $model     = $this->findModel($id);
        $birthDate = empty($model->birth_date) ? 'password' : date('dmY',strtotime($model->birth_date));
        if(!empty($birthDate)){
            $model->password = md5($birthDate);
        }
        if($model->save()) {
            Yii::$app->session->setFlash('success', 'รีเซ็ตรหัสผ่านของ '.$model->textfullname.' เรียบร้อย');  
            return $this->redirect(['index']);                
        }
    }
}
