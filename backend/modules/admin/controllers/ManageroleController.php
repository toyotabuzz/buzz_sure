<?php

namespace backend\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use common\models\turole;
use common\models\TuroleSearch;
use common\models\Tudep;

/**
 * ManageroleController implements the CRUD actions for turole model.
 */
class ManageroleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view', 'confirmdelete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all turole models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TuroleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single turole model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new turole model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $from = 1;
        $model = new turole();
        $modelDep   = Tudep::find()->orderBy('dep_name')->asArray()->all();

        $res = [];
        $dataPost = Yii::$app->request->post();
        if (!empty($dataPost)) {
            $model->dep_id = $dataPost['agency'];
            $model->role_name = $dataPost['duty_position'];
            if (!$model->save()) {
                $res['status'] = false;
                $res['text'] = 'บันทึกข้อมูลล้มเหลว กรุณาติดต่อผู้ดูแลระบบ';
            } else {
                $res['status'] = true;
                $res['text'] = 'บันทึกข้อมูลเรียบร้อยแล้ว';
            }
            return json_encode($res);
        }

        return $this->renderAjax('create', [
            'from' => $from,
            'model' => $model,
            'modelDep' => $modelDep,
        ]);
    }

    /**
     * Updates an existing turole model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $from = 2;
        $model = $this->findModel($id);
        $modelDep   = Tudep::find()->orderBy('dep_name')->asArray()->all();

        $res = [];
        $dataPost = Yii::$app->request->post();
        if (!empty($dataPost)) {
            $model->dep_id = $dataPost['agency'];
            $model->role_name = $dataPost['duty_position'];
            if (!$model->save()) {
                $res['status'] = false;
                $res['text'] = 'บันทึกข้อมูลล้มเหลว กรุณาติดต่อผู้ดูแลระบบ';
            } else {
                $res['status'] = true;
                $res['text'] = 'บันทึกข้อมูลเรียบร้อยแล้ว';
            }
            return json_encode($res);
        }

        return $this->renderAjax('update', [
            'from' => $from,
            'model' => $model,
            'modelDep' => $modelDep,
        ]);
    }

    public function actionConfirmdelete($id)
    {
        $request = Yii::$app->request->post() ? Yii::$app->request->post() : Yii::$app->request->queryParams;

        $model = turole::findOne($request['id']);

        if($model->isactive == 'Y')
        {
            $model->isactive = 'N';
        }
        else
        {
            $model->isactive = 'Y';
        }
        
        if(!$model->save())
        {
            echo "<pre>";
            print_r($model->getErrors());
            exit;
        }
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing turole model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the turole model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return turole the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = turole::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
