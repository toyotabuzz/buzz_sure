<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure_item".
 *
 * @property int $insure_item_id
 * @property int $insure_id
 * @property string $payment_type
 * @property string $payment_date
 * @property float $payment_price
 * @property string $payment_status
 * @property string $payment_number
 * @property string $insure_date
 * @property string $remark
 * @property int $payment_order
 * @property int $payment_sub_order
 * @property string $payment_no
 * @property string $acc_approve
 * @property string $acc_date
 * @property int $acc_by
 * @property int $acc_company_id
 * @property string $acc_remark
 * @property string $old_number_nohead
 * @property string $old_number_head
 * @property string $payment_number_head
 * @property string $payment_number_nohead
 * @property int $insure_car_id
 * @property int $insure_car
 * @property int $insure_sale
 * @property int $insure_customer
 * @property string $cus_name
 * @property string $cus_address
 * @property string $car_code
 * @property string $car_name
 * @property string $sale_name
 * @property string $cashier_number
 */
class TInsureItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insure_id', 'payment_order', 'payment_sub_order', 'acc_by', 'acc_company_id', 'insure_car_id', 'insure_car', 'insure_sale', 'insure_customer'], 'integer'],
            [['payment_date', 'insure_date', 'payment_sub_order', 'acc_approve', 'acc_date', 'acc_by', 'acc_company_id', 'acc_remark', 'old_number_nohead', 'old_number_head', 'payment_number_head', 'payment_number_nohead', 'insure_car_id', 'insure_car', 'insure_sale', 'insure_customer', 'cus_name', 'cus_address', 'car_code', 'car_name', 'sale_name', 'cashier_number'], 'required'],
            [['payment_date', 'insure_date', 'acc_date'], 'safe'],
            [['payment_price'], 'number'],
            [['payment_type', 'payment_status', 'payment_no'], 'string', 'max' => 50],
            [['payment_number', 'old_number_nohead', 'old_number_head', 'car_code', 'cashier_number'], 'string', 'max' => 20],
            [['remark', 'acc_remark', 'cus_address'], 'string', 'max' => 255],
            [['acc_approve'], 'string', 'max' => 4],
            [['payment_number_head', 'payment_number_nohead'], 'string', 'max' => 30],
            [['cus_name', 'car_name', 'sale_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insure_item_id' => 'Insure Item ID',
            'insure_id' => 'Insure ID',
            'payment_type' => 'Payment Type',
            'payment_date' => 'Payment Date',
            'payment_price' => 'Payment Price',
            'payment_status' => 'Payment Status',
            'payment_number' => 'Payment Number',
            'insure_date' => 'Insure Date',
            'remark' => 'Remark',
            'payment_order' => 'Payment Order',
            'payment_sub_order' => 'Payment Sub Order',
            'payment_no' => 'Payment No',
            'acc_approve' => 'Acc Approve',
            'acc_date' => 'Acc Date',
            'acc_by' => 'Acc By',
            'acc_company_id' => 'Acc Company ID',
            'acc_remark' => 'Acc Remark',
            'old_number_nohead' => 'Old Number Nohead',
            'old_number_head' => 'Old Number Head',
            'payment_number_head' => 'Payment Number Head',
            'payment_number_nohead' => 'Payment Number Nohead',
            'insure_car_id' => 'Insure Car ID',
            'insure_car' => 'Insure Car',
            'insure_sale' => 'Insure Sale',
            'insure_customer' => 'Insure Customer',
            'cus_name' => 'Cus Name',
            'cus_address' => 'Cus Address',
            'car_code' => 'Car Code',
            'car_name' => 'Car Name',
            'sale_name' => 'Sale Name',
            'cashier_number' => 'Cashier Number',
        ];
    }
}
