<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TInsureStock;

/**
 * TInsureStockSearch represents the model behind the search form of `common\models\TInsureStock`.
 */
class TInsureStockSearch extends TInsureStock
{
    public $strDate;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insure_stock_id', 'insure_id', 'insure_company_id', 'status_company_id', 'status_department_id', 'status_officer_id', 'cancel_form_id', 'stock_company_id', 'stock_department_id', 'stock_officer_id', 'send_code', 'doc_send', 'run_stock'], 'integer'],
            [['stock_number', 'remark', 'status', 'status_date', 'changes', 'change_date', 'change_form_id', 'cancel', 'cancel_date', 'stock_date', 'stock_code','strDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TInsureStock::find()
        ->joinWith('tInsure');

        // add conditions that should always apply here

        // $dataProvider = new ActiveDataProvider([
        //     'query' => $query,
        // ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $query;
        }

        if(!empty($this->strDate)){
            $query->where('t_insure.k_start_date LIKE "'.$this->strDate.'-%"');
        }

        // grid filtering conditions
        $query->andFilterWhere([
            't_insure_stock.insure_stock_id' => $this->insure_stock_id,
            't_insure_stock.insure_id' => $this->insure_id,
            't_insure_stock.insure_company_id' => $this->insure_company_id,
            't_insure_stock.status_date' => $this->status_date,
            't_insure_stock.status_company_id' => $this->status_company_id,
            't_insure_stock.status_department_id' => $this->status_department_id,
            't_insure_stock.status_officer_id' => $this->status_officer_id,
            't_insure_stock.change_date' => $this->change_date,
            't_insure_stock.cancel_date' => $this->cancel_date,
            't_insure_stock.cancel_form_id' => $this->cancel_form_id,
            't_insure_stock.stock_date' => $this->stock_date,
            't_insure_stock.stock_company_id' => $this->stock_company_id,
            't_insure_stock.stock_department_id' => $this->stock_department_id,
            't_insure_stock.stock_officer_id' => $this->stock_officer_id,
            't_insure_stock.send_code' => $this->send_code,
            't_insure_stock.doc_send' => $this->doc_send,
            't_insure_stock.run_stock' => $this->run_stock,
        ]);

        $query->andFilterWhere(['like', 't_insure_stock.stock_number', $this->stock_number])
            ->andFilterWhere(['like', 't_insure_stock.remark', $this->remark])
            ->andFilterWhere(['like', 't_insure_stock.status', $this->status])
            ->andFilterWhere(['like', 't_insure_stock.changes', $this->changes])
            ->andFilterWhere(['like', 't_insure_stock.change_form_id', $this->change_form_id])
            ->andFilterWhere(['like', 't_insure_stock.cancel', $this->cancel])
            ->andFilterWhere(['like', 't_insure_stock.stock_code', $this->stock_code]);

        $query->orderBy(['t_insure_stock.stock_code' => SORT_DESC]);
        //echo $query->createCommand()->getRawSql();exit;
        return $query;
    }
}
