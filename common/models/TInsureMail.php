<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure_mail".
 *
 * @property int $label_id
 * @property string $title
 * @property string $text01
 * @property string $text02
 * @property string $text03
 * @property string $text04
 * @property string $text05
 * @property string $text06
 * @property string $text07
 * @property string $text08
 * @property string $text09
 * @property string $text10
 * @property int $h1
 * @property int $h2
 * @property int $w1
 * @property int $w2
 * @property int $company_id
 * @property string $mtype
 */
class TInsureMail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure_mail';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text01', 'text02', 'text03', 'text04', 'text05', 'text06', 'text07', 'text08', 'text09', 'text10'], 'required'],
            [['text01', 'text02', 'text03', 'text04', 'text05', 'text06', 'text07', 'text08', 'text09', 'text10'], 'string'],
            [['h1', 'h2', 'w1', 'w2', 'company_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['mtype'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'label_id' => 'Label ID',
            'title' => 'Title',
            'text01' => 'Text01',
            'text02' => 'Text02',
            'text03' => 'Text03',
            'text04' => 'Text04',
            'text05' => 'Text05',
            'text06' => 'Text06',
            'text07' => 'Text07',
            'text08' => 'Text08',
            'text09' => 'Text09',
            'text10' => 'Text10',
            'h1' => 'H1',
            'h2' => 'H2',
            'w1' => 'W1',
            'w2' => 'W2',
            'company_id' => 'Company ID',
            'mtype' => 'Mtype',
        ];
    }
}
