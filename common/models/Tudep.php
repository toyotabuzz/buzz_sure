<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tudep".
 *
 * @property integer $id
 * @property string $dep_name
 * @property string $dep_code
 */
class Tudep extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tudep';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dep_code'], 'required'],
            [['dep_name'], 'string', 'max' => 255],
            [['dep_code'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dep_name' => 'Dep Name',
            'dep_code' => 'Dep Code',
        ];
    }
}
