<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_stock_insure".
 *
 * @property int $stock_insure_id
 * @property int $broker_id
 * @property int $insure_company_id
 * @property string|null $title
 * @property string $title01
 * @property float $price
 * @property int $insure_id
 * @property string $status
 * @property string $insure_type
 * @property int $company_id
 * @property string $status_sale
 * @property float $cprice
 * @property string $stock_remark
 * @property string $cancel_date
 * @property string $account_remark
 * @property int $insure_officer_id
 * @property int $insure_cancel_id
 * @property int $doc_send
 * @property string $doc_date
 * @property string $stock_code
 */
class TStockInsure extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_stock_insure';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['broker_id', 'insure_company_id', 'insure_id', 'company_id', 'insure_officer_id', 'insure_cancel_id', 'doc_send'], 'integer'],
            [['title01', 'cprice', 'stock_remark', 'cancel_date', 'account_remark', 'insure_officer_id', 'insure_cancel_id', 'doc_send', 'doc_date', 'stock_code'], 'required'],
            [['price', 'cprice'], 'number'],
            [['stock_remark'], 'string'],
            [['cancel_date', 'doc_date'], 'safe'],
            [['title', 'title01', 'status'], 'string', 'max' => 100],
            [['insure_type', 'status_sale'], 'string', 'max' => 20],
            [['account_remark'], 'string', 'max' => 255],
            [['stock_code'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'stock_insure_id' => 'Stock Insure ID',
            'broker_id' => 'Broker ID',
            'insure_company_id' => 'Insure Company ID',
            'title' => 'Title',
            'title01' => 'Title01',
            'price' => 'Price',
            'insure_id' => 'Insure ID',
            'status' => 'Status',
            'insure_type' => 'Insure Type',
            'company_id' => 'Company ID',
            'status_sale' => 'Status Sale',
            'cprice' => 'Cprice',
            'stock_remark' => 'Stock Remark',
            'cancel_date' => 'Cancel Date',
            'account_remark' => 'Account Remark',
            'insure_officer_id' => 'Insure Officer ID',
            'insure_cancel_id' => 'Insure Cancel ID',
            'doc_send' => 'Doc Send',
            'doc_date' => 'Doc Date',
            'stock_code' => 'Stock Code',
        ];
    }
}
