<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_payment_subject".
 *
 * @property int $payment_subject_id
 * @property string $title
 * @property int $type_id
 * @property int $nohead
 * @property string $rank
 * @property int $discount
 */
class TPaymentSubject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_payment_subject';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_id', 'nohead', 'discount'], 'integer'],
            [['rank', 'discount'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['rank'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'payment_subject_id' => 'Payment Subject ID',
            'title' => 'Title',
            'type_id' => 'Type ID',
            'nohead' => 'Nohead',
            'rank' => 'Rank',
            'discount' => 'Discount',
        ];
    }
}
