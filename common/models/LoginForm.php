<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user) {
                $this->addError($attribute, 'Incorrect username.');
            } else if (!$user->validatePassword($this->password)) {//md5()
                $this->addError($attribute, 'Incorrect password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $userLogin = Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 10 * 1 : 0);
            //INSERT LOG FOR LOG IN

            // $modelLog              = new TmemberLog();
            // $modelLog->member_id   = Yii::$app->user->identity->id;
            // $modelLog->date_log    = date('Y-m-d H:i:s');
            // $modelLog->module_code = (Yii::$app->controller->module->id != 'app-backend')? Yii::$app->controller->module->id : Yii::$app->controller->id;
            // $modelLog->action      = Yii::$app->controller->action->id;
            // $modelLog->url         = Yii::$app->request->getUrl();
            // $modelLog->ip          = Yii::$app->getRequest()->getUserIP();
            // if(!$modelLog->save()){
            //     echo Yii::$app->Helpers->GetErrorModel($modelLog);
            //     exit;
            // }

            return $userLogin;
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = Tmember::findByUsername($this->username);
        }

        return $this->_user;
    }

    public function logout()
    {
        //INSERT LOG FOR LOG OUT
        // $modelLog             = new TlogLogin();
        // $modelLog->user_id    = Yii::$app->user->identity->id;
        // $modelLog->log_type   = 'OUT';
        // $modelLog->ip_address = Yii::$app->getRequest()->getUserIP();
        //return $modelLog->save();
    }
}
