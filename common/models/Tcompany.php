<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "t_company".
 *
 * @property integer $company_id
 * @property string $title
 * @property string $short_title
 * @property string $short_title_en
 * @property string $address1
 * @property string $address2
 * @property string $province
 * @property string $zip
 * @property string $phone
 * @property string $fax
 * @property string $taxId
 * @property string $contact_name
 * @property string $remark
 * @property string $email
 * @property string $website
 * @property string $logo
 * @property string $tax_id
 * @property string $order_number_default
 * @property string $order_number_nohead_default
 * @property string $order_number_recent
 * @property string $order_number_nohead_recent
 * @property string $order_number_01
 * @property string $order_number_02
 * @property string $viriya_code
 * @property string $cus_code
 * @property string $inven
 * @property string $location
 */
class Tcompany extends \yii\db\ActiveRecord
{
    public $status_second_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_company';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->identity->id,
            ],
        ];
    }  
     */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'],'required'],
            [['order_number_default','order_number_nohead_default','order_number_recent','order_number_nohead_recent','order_number_01','order_number_02'],'integer'],
            [['title','address1','address2','contact_name'], 'string', 'max' => 255],
            [['short_title','phone','fax','taxId','email','website','logo'], 'string', 'max' => 100],
            [['province'], 'string', 'max' => 50],
            [['short_title_en','tax_id'], 'string', 'max' => 30],
            [['zip'], 'string', 'max' => 10],
            [['viriya_code'], 'string', 'max' => 10],
            [['cus_code'], 'string', 'max' => 8],
            [['inven','location'], 'string', 'max' => 8],
            [['remark'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id'                  => 'ID',
            'title'                       => 'Title',
            'short_title'                 => 'Short Title',
            'address1'                    => 'Address1',
            'address2'                    => 'Address2',
            'province'                    => 'Province',
            'zip'                         => 'Zip',
            'phone'                       => 'Phone',
            'fax'                         => 'Fax',
            'taxId'                       => 'TaxId',
            'contact_name'                => 'Contact_name',
            'remark'                      => 'Remark',
            'email'                       => 'Email',
            'website'                     => 'Website',
            'logo'                        => 'Logo',
            'tax_id'                      => 'Tax ID',
            'order_number_default'        => 'Order Number Default',
            'order_number_nohead_default' => 'Compaorder Number Nohead Defaultny',
            'order_number_recent'         => 'Order Number Recent',
            'order_number_nohead_recent'  => 'Order Number Nohead Recent',
            'order_number_01'             => 'Order Number 01',
            'order_number_02'             => 'Order Number 02',
            'viriya_code'                 => 'Viriya Code',
            'cus_code'                    => 'Cus Code',
            'inven'                       => 'Inven',
            'location'                    => 'Location',
        ];
    }

    

    /**
    **
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->attribute       = $this->attribute;            
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        $this->attribute       = $this->attribute;        
    }
    *
    */
}
