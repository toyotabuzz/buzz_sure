<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure_customer01".
 *
 * @property int $insure_customer_id
 * @property int|null $customer_id
 * @property string $id_card
 * @property string $type_id
 * @property int $acard
 * @property int $bcard
 * @property int $ccard
 * @property int $icard
 * @property int $grade_id
 * @property int $event_id
 * @property int $group_id
 * @property int $follow_up
 * @property string|null $information_date
 * @property int $sale_id
 * @property string|null $birthday
 * @property string $title
 * @property string $firstname
 * @property string $lastname
 * @property int $customer_title_id
 * @property string $address
 * @property string $address1
 * @property string $homeno
 * @property string $company_name
 * @property string $building
 * @property string $moono
 * @property string $mooban
 * @property string $soi
 * @property string $road
 * @property string $tumbon
 * @property string $amphur
 * @property string $province
 * @property string $tumbon_code
 * @property string $amphur_code
 * @property string $province_code
 * @property string $zip_code
 * @property string $zip
 * @property string $home_tel
 * @property string $mobile
 * @property string $office_tel
 * @property string $fax
 * @property string $email
 * @property string $contact_name
 * @property string $address01
 * @property string $address011
 * @property string $homeno01
 * @property string $company_name01
 * @property string $building01
 * @property string $moono01
 * @property string $mooban01
 * @property string $soi01
 * @property string $road01
 * @property string $tumbon01
 * @property string $amphur01
 * @property string $province01
 * @property string $tumbon_code01
 * @property string $amphur_code01
 * @property string $province_code01
 * @property string $zip_code01
 * @property string $zip01
 * @property string $tel01
 * @property string $fax01
 * @property string $company
 * @property string $address02
 * @property string $address021
 * @property string $homeno02
 * @property string $company_name02
 * @property string $building02
 * @property string $moono02
 * @property string $mooban02
 * @property string $soi02
 * @property string $road02
 * @property string $tumbon02
 * @property string $amphur02
 * @property string $province02
 * @property string $tumbon_code02
 * @property string $amphur_code02
 * @property string $province_code02
 * @property string $zip_code02
 * @property string $zip02
 * @property string $tel02
 * @property string $fax02
 * @property string $name03
 * @property string $address03
 * @property string $address031
 * @property string $homeno03
 * @property string $company_name03
 * @property string $building03
 * @property string $moono03
 * @property string $mooban03
 * @property string $soi03
 * @property string $road03
 * @property string $tumbon03
 * @property string $amphur03
 * @property string $province03
 * @property string $tumbon_code03
 * @property string $amphur_code03
 * @property string $province_code03
 * @property string $zip_code03
 * @property string $zip03
 * @property string $tel03
 * @property string $fax03
 * @property string $name04
 * @property string $address04
 * @property string $address041
 * @property string $homeno04
 * @property string $company_name04
 * @property string $building04
 * @property string $moono04
 * @property string $mooban04
 * @property string $soi04
 * @property string $road04
 * @property string $tumbon04
 * @property string $amphur04
 * @property string $province04
 * @property string $tumbon_code04
 * @property string $amphur_code04
 * @property string $province_code04
 * @property string $zip_code04
 * @property string $zip04
 * @property string $tel04
 * @property string $fax04
 * @property int $incomplete
 * @property int $mailback
 * @property int $add_by
 * @property string|null $add_date
 * @property int $edit_by
 * @property string|null $edit_date
 * @property int $delete_by
 * @property string|null $delete_date
 * @property string $delete_reason
 * @property int $delete_status
 * @property int $verify_address
 * @property int $verify_phone
 * @property int $acard_old
 * @property string $mailback_remark
 * @property string $mailback_date
 * @property int $duplicates
 * @property string $remark
 * @property int $company_id
 * @property int $dealer
 * @property int $insure_member
 * @property int $relate_member
 * @property int $relate_team
 * @property string $niti01
 * @property string $niti02
 */
class TInsureCustomer01 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure_customer01';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'acard', 'bcard', 'ccard', 'icard', 'grade_id', 'event_id', 'group_id', 'follow_up', 'sale_id', 'customer_title_id', 'incomplete', 'mailback', 'add_by', 'edit_by', 'delete_by', 'delete_status', 'verify_address', 'verify_phone', 'acard_old', 'duplicates', 'company_id', 'dealer', 'insure_member', 'relate_member', 'relate_team'], 'integer'],
            [['icard', 'follow_up', 'address1', 'homeno', 'company_name', 'building', 'moono', 'mooban', 'soi', 'road', 'zip_code', 'contact_name', 'address011', 'homeno01', 'company_name01', 'building01', 'moono01', 'mooban01', 'soi01', 'road01', 'tumbon_code01', 'amphur_code01', 'province_code01', 'zip_code01', 'company', 'address021', 'homeno02', 'company_name02', 'building02', 'moono02', 'mooban02', 'soi02', 'road02', 'tumbon_code02', 'amphur_code02', 'province_code02', 'zip_code02', 'name03', 'address031', 'homeno03', 'company_name03', 'building03', 'moono03', 'mooban03', 'soi03', 'road03', 'tumbon_code03', 'amphur_code03', 'province_code03', 'zip_code03', 'name04', 'address04', 'address041', 'homeno04', 'company_name04', 'building04', 'moono04', 'mooban04', 'soi04', 'road04', 'tumbon04', 'amphur04', 'province04', 'tumbon_code04', 'amphur_code04', 'province_code04', 'zip_code04', 'zip04', 'tel04', 'fax04', 'incomplete', 'mailback', 'verify_address', 'verify_phone', 'acard_old', 'mailback_remark', 'mailback_date', 'duplicates', 'remark', 'company_id', 'dealer', 'insure_member', 'relate_member', 'relate_team', 'niti01', 'niti02'], 'required'],
            [['information_date', 'birthday', 'add_date', 'edit_date', 'delete_date', 'mailback_date'], 'safe'],
            [['mailback_remark', 'remark'], 'string'],
            [['id_card', 'firstname', 'lastname', 'home_tel', 'mobile', 'office_tel', 'fax', 'email', 'tumbon01', 'amphur01', 'province01', 'tel01', 'fax01', 'tumbon02', 'amphur02', 'province02', 'tel02', 'fax02', 'tumbon03', 'amphur03', 'province03', 'tel03', 'fax03', 'tumbon04', 'amphur04', 'province04', 'tel04', 'fax04', 'niti01', 'niti02'], 'string', 'max' => 50],
            [['type_id'], 'string', 'max' => 1],
            [['title', 'tumbon_code', 'amphur_code', 'province_code', 'zip_code', 'tumbon_code01', 'amphur_code01', 'province_code01', 'zip_code01', 'zip01', 'tumbon_code02', 'amphur_code02', 'province_code02', 'zip_code02', 'zip02', 'tumbon_code03', 'amphur_code03', 'province_code03', 'zip_code03', 'zip03', 'tumbon_code04', 'amphur_code04', 'province_code04', 'zip_code04', 'zip04'], 'string', 'max' => 10],
            [['address', 'delete_reason'], 'string', 'max' => 255],
            [['address1', 'company_name', 'building', 'mooban', 'soi', 'road', 'tumbon', 'amphur', 'province', 'contact_name', 'address01', 'address011', 'company_name01', 'building01', 'mooban01', 'soi01', 'road01', 'company', 'address02', 'address021', 'company_name02', 'building02', 'mooban02', 'soi02', 'road02', 'name03', 'address03', 'address031', 'company_name03', 'building03', 'mooban03', 'soi03', 'road03', 'address04', 'address041', 'company_name04', 'building04', 'mooban04', 'soi04', 'road04'], 'string', 'max' => 100],
            [['homeno', 'moono', 'homeno01', 'moono01', 'homeno02', 'moono02', 'homeno03', 'moono03', 'homeno04', 'moono04'], 'string', 'max' => 20],
            [['zip'], 'string', 'max' => 5],
            [['name04'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insure_customer_id' => 'Insure Customer ID',
            'customer_id' => 'Customer ID',
            'id_card' => 'Id Card',
            'type_id' => 'Type ID',
            'acard' => 'Acard',
            'bcard' => 'Bcard',
            'ccard' => 'Ccard',
            'icard' => 'Icard',
            'grade_id' => 'Grade ID',
            'event_id' => 'Event ID',
            'group_id' => 'Group ID',
            'follow_up' => 'Follow Up',
            'information_date' => 'Information Date',
            'sale_id' => 'Sale ID',
            'birthday' => 'Birthday',
            'title' => 'Title',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'customer_title_id' => 'Customer Title ID',
            'address' => 'Address',
            'address1' => 'Address1',
            'homeno' => 'Homeno',
            'company_name' => 'Company Name',
            'building' => 'Building',
            'moono' => 'Moono',
            'mooban' => 'Mooban',
            'soi' => 'Soi',
            'road' => 'Road',
            'tumbon' => 'Tumbon',
            'amphur' => 'Amphur',
            'province' => 'Province',
            'tumbon_code' => 'Tumbon Code',
            'amphur_code' => 'Amphur Code',
            'province_code' => 'Province Code',
            'zip_code' => 'Zip Code',
            'zip' => 'Zip',
            'home_tel' => 'Home Tel',
            'mobile' => 'Mobile',
            'office_tel' => 'Office Tel',
            'fax' => 'Fax',
            'email' => 'Email',
            'contact_name' => 'Contact Name',
            'address01' => 'Address01',
            'address011' => 'Address011',
            'homeno01' => 'Homeno01',
            'company_name01' => 'Company Name01',
            'building01' => 'Building01',
            'moono01' => 'Moono01',
            'mooban01' => 'Mooban01',
            'soi01' => 'Soi01',
            'road01' => 'Road01',
            'tumbon01' => 'Tumbon01',
            'amphur01' => 'Amphur01',
            'province01' => 'Province01',
            'tumbon_code01' => 'Tumbon Code01',
            'amphur_code01' => 'Amphur Code01',
            'province_code01' => 'Province Code01',
            'zip_code01' => 'Zip Code01',
            'zip01' => 'Zip01',
            'tel01' => 'Tel01',
            'fax01' => 'Fax01',
            'company' => 'Company',
            'address02' => 'Address02',
            'address021' => 'Address021',
            'homeno02' => 'Homeno02',
            'company_name02' => 'Company Name02',
            'building02' => 'Building02',
            'moono02' => 'Moono02',
            'mooban02' => 'Mooban02',
            'soi02' => 'Soi02',
            'road02' => 'Road02',
            'tumbon02' => 'Tumbon02',
            'amphur02' => 'Amphur02',
            'province02' => 'Province02',
            'tumbon_code02' => 'Tumbon Code02',
            'amphur_code02' => 'Amphur Code02',
            'province_code02' => 'Province Code02',
            'zip_code02' => 'Zip Code02',
            'zip02' => 'Zip02',
            'tel02' => 'Tel02',
            'fax02' => 'Fax02',
            'name03' => 'Name03',
            'address03' => 'Address03',
            'address031' => 'Address031',
            'homeno03' => 'Homeno03',
            'company_name03' => 'Company Name03',
            'building03' => 'Building03',
            'moono03' => 'Moono03',
            'mooban03' => 'Mooban03',
            'soi03' => 'Soi03',
            'road03' => 'Road03',
            'tumbon03' => 'Tumbon03',
            'amphur03' => 'Amphur03',
            'province03' => 'Province03',
            'tumbon_code03' => 'Tumbon Code03',
            'amphur_code03' => 'Amphur Code03',
            'province_code03' => 'Province Code03',
            'zip_code03' => 'Zip Code03',
            'zip03' => 'Zip03',
            'tel03' => 'Tel03',
            'fax03' => 'Fax03',
            'name04' => 'Name04',
            'address04' => 'Address04',
            'address041' => 'Address041',
            'homeno04' => 'Homeno04',
            'company_name04' => 'Company Name04',
            'building04' => 'Building04',
            'moono04' => 'Moono04',
            'mooban04' => 'Mooban04',
            'soi04' => 'Soi04',
            'road04' => 'Road04',
            'tumbon04' => 'Tumbon04',
            'amphur04' => 'Amphur04',
            'province04' => 'Province04',
            'tumbon_code04' => 'Tumbon Code04',
            'amphur_code04' => 'Amphur Code04',
            'province_code04' => 'Province Code04',
            'zip_code04' => 'Zip Code04',
            'zip04' => 'Zip04',
            'tel04' => 'Tel04',
            'fax04' => 'Fax04',
            'incomplete' => 'Incomplete',
            'mailback' => 'Mailback',
            'add_by' => 'Add By',
            'add_date' => 'Add Date',
            'edit_by' => 'Edit By',
            'edit_date' => 'Edit Date',
            'delete_by' => 'Delete By',
            'delete_date' => 'Delete Date',
            'delete_reason' => 'Delete Reason',
            'delete_status' => 'Delete Status',
            'verify_address' => 'Verify Address',
            'verify_phone' => 'Verify Phone',
            'acard_old' => 'Acard Old',
            'mailback_remark' => 'Mailback Remark',
            'mailback_date' => 'Mailback Date',
            'duplicates' => 'Duplicates',
            'remark' => 'Remark',
            'company_id' => 'Company ID',
            'dealer' => 'Dealer',
            'insure_member' => 'Insure Member',
            'relate_member' => 'Relate Member',
            'relate_team' => 'Relate Team',
            'niti01' => 'Niti01',
            'niti02' => 'Niti02',
        ];
    }
}
