<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_car_series".
 *
 * @property int $car_series_id
 * @property float $margin
 * @property string $margin_date
 * @property int $goa
 * @property int $car_model_id
 * @property int $car_type_id
 * @property int $car_type
 * @property string $title
 * @property string $model
 * @property float $retail
 * @property float $price
 * @property string $car_number
 * @property string $engine_number
 * @property int $rank
 * @property string $expire
 * @property string $start_date
 * @property int $insure_add
 * @property string $cc
 * @property string $gear
 * @property int $navigator
 */
class TCarSeries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_car_series';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['margin', 'retail', 'price'], 'number'],
            [['margin_date', 'model', 'price', 'rank', 'expire', 'start_date', 'insure_add', 'cc', 'gear', 'navigator'], 'required'],
            [['margin_date', 'expire', 'start_date'], 'safe'],
            [['goa', 'car_model_id', 'car_type_id', 'car_type', 'rank', 'insure_add', 'navigator'], 'integer'],
            [['title', 'model'], 'string', 'max' => 100],
            [['car_number'], 'string', 'max' => 7],
            [['engine_number'], 'string', 'max' => 4],
            [['cc', 'gear'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'car_series_id' => 'Car Series ID',
            'margin' => 'Margin',
            'margin_date' => 'Margin Date',
            'goa' => 'Goa',
            'car_model_id' => 'Car Model ID',
            'car_type_id' => 'Car Type ID',
            'car_type' => 'Car Type',
            'title' => 'Title',
            'model' => 'Model',
            'retail' => 'Retail',
            'price' => 'Price',
            'car_number' => 'Car Number',
            'engine_number' => 'Engine Number',
            'rank' => 'Rank',
            'expire' => 'Expire',
            'start_date' => 'Start Date',
            'insure_add' => 'Insure Add',
            'cc' => 'Cc',
            'gear' => 'Gear',
            'navigator' => 'Navigator',
        ];
    }
}
