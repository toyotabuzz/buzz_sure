<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure_mail_item_detail".
 *
 * @property int $insure_mail_item_detail_id
 * @property int $insure_mail_item_id
 * @property int $insure_stock_id
 * @property int $stock_insure_id
 * @property string $bill
 * @property string $pasee
 * @property string $saluk
 * @property int $insure_id
 * @property string $remark
 * @property string $code
 * @property int $recieve
 * @property string $recieve_date
 * @property string $recieve_name
 * @property string $recieve_relate
 * @property int $cancel
 * @property string $cancel_date
 * @property string $cancel_remark
 * @property int $car_id
 * @property string $postcode
 * @property float $weight
 * @property float $price
 */
class TInsureMailItemDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure_mail_item_detail';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insure_mail_item_id', 'insure_id', 'code', 'recieve', 'recieve_date', 'cancel', 'car_id', 'weight', 'price'], 'required'],
            [['insure_mail_item_id', 'insure_stock_id', 'stock_insure_id', 'insure_id', 'recieve', 'cancel', 'car_id'], 'integer'],
            [['recieve_date', 'cancel_date'], 'safe'],
            [['weight', 'price'], 'number'],
            [['bill', 'pasee', 'saluk'], 'string', 'max' => 3],
            [['remark', 'cancel_remark'], 'string', 'max' => 255],
            [['code', 'recieve_relate'], 'string', 'max' => 50],
            [['recieve_name'], 'string', 'max' => 100],
            [['postcode'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insure_mail_item_detail_id' => 'Insure Mail Item Detail ID',
            'insure_mail_item_id' => 'Insure Mail Item ID',
            'insure_stock_id' => 'Insure Stock ID',
            'stock_insure_id' => 'Stock Insure ID',
            'bill' => 'Bill',
            'pasee' => 'Pasee',
            'saluk' => 'Saluk',
            'insure_id' => 'Insure ID',
            'remark' => 'Remark',
            'code' => 'Code',
            'recieve' => 'Recieve',
            'recieve_date' => 'Recieve Date',
            'recieve_name' => 'Recieve Name',
            'recieve_relate' => 'Recieve Relate',
            'cancel' => 'Cancel',
            'cancel_date' => 'Cancel Date',
            'cancel_remark' => 'Cancel Remark',
            'car_id' => 'Car ID',
            'postcode' => 'Postcode',
            'weight' => 'Weight',
            'price' => 'Price',
        ];
    }
}
