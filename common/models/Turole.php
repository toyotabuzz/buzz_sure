<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "turole".
 *
 * @property int $id
 * @property int|null $dep_id
 * @property string|null $role_name
 * @property string|null $isactive Y = ใช้งาน, N = ไม่ใช้งาน
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 *
 * @property Tudep $dep
 * @property TuuserRole[] $tuuserRoles
 */
class Turole extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'turole';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->identity->id,
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dep_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['role_name'], 'string', 'max' => 200],
            [['isactive'], 'string', 'max' => 1],
            [['dep_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tudep::className(), 'targetAttribute' => ['dep_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dep_id' => 'หน่วยงาน',
            'role_name' => 'หน้าที่',
            'isactive' => 'สถานะ',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[Dep]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDep()
    {
        return $this->hasOne(Tudep::className(), ['id' => 'dep_id']);
    }

    /**
     * Gets query for [[TuuserRoles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTuuserRoles()
    {
        return $this->hasMany(TuuserRole::className(), ['role_id' => 'id']);
    }

    /**
    **
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->attribute       = $this->attribute;            
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        $this->attribute       = $this->attribute;        
    }

    public function afterFind()
    {
        $this->attribute       = $this->attribute;

        parent::afterFind();
    }
    *
    */

}
