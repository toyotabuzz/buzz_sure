<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_car_type".
 *
 * @property int $car_type_id
 * @property string $title
 */
class TCarType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_car_type';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'car_type_id' => 'Car Type ID',
            'title' => 'Title',
        ];
    }
}
