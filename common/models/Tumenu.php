<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tumenu".
 *
 * @property integer $id
 * @property string $menu_code
 * @property string $menu_label
 * @property string $menu_url
 * @property string $menu_icon
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property Tumenusub[] $tumenusubs
 */
class Tumenu extends \yii\db\ActiveRecord
{
    public $isactive_text;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tumenu';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->identity->username,
            ],
        ];
    }  

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_label'], 'required'],
            [['menu_code'], 'integer'],
            [['menu_label', 'menu_url', 'menu_icon'], 'string', 'max' => 100],
            [['created_at', 'updated_at', 'isactive'], 'safe'],
            [['created_by', 'updated_by'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'menu_code'  => 'รหัส',
            'menu_label' => 'ชื่อ',
            'menu_url'   => 'Url',
            'menu_icon'  => 'Icon',
            'isactive'   => 'สถานะใช้งาน',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function attributeHints()
    {
        return [
            'menu_url'   => 'ex. /controller/action',
            'menu_icon'   => 'ex. fa fa-icon-name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubmenus()
    {
        return $this->hasMany(Tumenusub::className(), ['tumenu_id' => 'id']);
    }

    /**
     * Handles owner 'afterFind' event, ensuring attribute typecasting.
     * @param \yii\base\Event $event event instance.
     */
    public function afterFind()
    {
        $this->isactive_text = ($this->isactive == 'Y')?'ใช้งาน':'ไม่ใช้งาน';
        $this->isactive      = ($this->isactive == 'Y')?true:false;

        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->isactive = ($this->isactive)?'Y':'N';

            return true;
        } else {
            return false;
        }
    }
}
