<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure_company".
 *
 * @property int $insure_company_id
 * @property string|null $title
 * @property string $rank
 * @property string $short_title
 * @property string $contact_name
 * @property string $contact_tel
 * @property string $contact_fax
 * @property string $contact_code
 * @property float $num1
 * @property float $num2
 * @property float $num3
 * @property float $num4
 * @property float $num5
 * @property float $num6
 * @property float $num7
 * @property float $num8
 * @property float $num9
 * @property float $num10
 * @property float $num11
 * @property float $num12
 * @property string $num13
 * @property string $title01
 * @property string $title02
 * @property string $title03
 * @property string $title04
 * @property string $vendor
 * @property string $part_no
 * @property string $part_no1
 */
class TInsureCompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure_company';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rank', 'short_title', 'contact_name', 'contact_tel', 'contact_fax', 'contact_code', 'num1', 'num2', 'num3', 'num4', 'num5', 'num6', 'num7', 'num8', 'num9', 'num10', 'num11', 'num12', 'num13', 'title01', 'title02', 'title03', 'title04', 'vendor', 'part_no', 'part_no1'], 'required'],
            [['num1', 'num2', 'num3', 'num4', 'num5', 'num6', 'num7', 'num8', 'num9', 'num10', 'num11', 'num12'], 'number'],
            [['title', 'contact_name', 'num13'], 'string', 'max' => 100],
            [['rank'], 'string', 'max' => 3],
            [['short_title'], 'string', 'max' => 10],
            [['contact_tel', 'contact_fax', 'contact_code'], 'string', 'max' => 50],
            [['title01', 'title02', 'title03', 'title04'], 'string', 'max' => 255],
            [['vendor'], 'string', 'max' => 8],
            [['part_no', 'part_no1'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insure_company_id' => 'Insure Company ID',
            'title' => 'Title',
            'rank' => 'Rank',
            'short_title' => 'Short Title',
            'contact_name' => 'Contact Name',
            'contact_tel' => 'Contact Tel',
            'contact_fax' => 'Contact Fax',
            'contact_code' => 'Contact Code',
            'num1' => 'Num1',
            'num2' => 'Num2',
            'num3' => 'Num3',
            'num4' => 'Num4',
            'num5' => 'Num5',
            'num6' => 'Num6',
            'num7' => 'Num7',
            'num8' => 'Num8',
            'num9' => 'Num9',
            'num10' => 'Num10',
            'num11' => 'Num11',
            'num12' => 'Num12',
            'num13' => 'Num13',
            'title01' => 'Title01',
            'title02' => 'Title02',
            'title03' => 'Title03',
            'title04' => 'Title04',
            'vendor' => 'Vendor',
            'part_no' => 'Part No',
            'part_no1' => 'Part No1',
        ];
    }
}
