<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_fund_company".
 *
 * @property int $fund_company_id
 * @property string $title
 * @property string $code
 * @property float $p01
 * @property float $p02
 * @property float $p03
 * @property float $p04
 * @property string $rp_name
 * @property string $rp_department
 * @property string $rp_address
 * @property string $rp_tel
 */
class TFundCompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_fund_company';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'p01', 'p02', 'p03', 'p04', 'rp_name', 'rp_department', 'rp_address', 'rp_tel'], 'required'],
            [['p01', 'p02', 'p03', 'p04'], 'number'],
            [['title', 'rp_name', 'rp_department', 'rp_tel'], 'string', 'max' => 100],
            [['code'], 'string', 'max' => 10],
            [['rp_address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fund_company_id' => 'Fund Company ID',
            'title' => 'Title',
            'code' => 'Code',
            'p01' => 'P01',
            'p02' => 'P02',
            'p03' => 'P03',
            'p04' => 'P04',
            'rp_name' => 'Rp Name',
            'rp_department' => 'Rp Department',
            'rp_address' => 'Rp Address',
            'rp_tel' => 'Rp Tel',
        ];
    }
}
