<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure_type".
 *
 * @property int $insure_type_id
 * @property string $title
 * @property string $title01
 */
class TInsureType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure_type';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title01'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['title01'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insure_type_id' => 'Insure Type ID',
            'title' => 'Title',
            'title01' => 'Title01',
        ];
    }
}
