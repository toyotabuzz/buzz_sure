<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TInsure;

/**
 * TInsureSearch represents the model behind the search form of `common\models\TInsure`.
 */
class TInsureSearch extends TInsure
{
    public $car_number;
    public $car_plate;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insure_id', 'sale_id', 'team_id', 'car_id', 'customer_id', 'company_id', 'quotation_id', 'quotation_item_id', 'k_check', 'k_broker_id', 'k_prb_id', 'k_stock_id', 'k_frees', 'p_check', 'p_broker_id', 'p_prb_id', 'p_stock_id', 'p_frees', 'k_dis_type_01', 'k_dis_type_02', 'k_dis_type_03', 'p_car_type', 'pay_number', 'k_send', 'k_send_officer_id', 'p_send', 'p_send_officer_id', 'bill_send', 'bill_send_officer_id', 'pasee_send', 'pasee_send_officer_id', 'prepare', 'prepare_by', 'p_acc_check', 'p_acc_id', 'k_acc_check', 'k_acc_id', 'officer_id', 'k_car_type', 'request_cancel', 'request_edit', 'k_ins_officer', 'k_acc_officer', 'k_sale_officer'], 'integer'],
            [['year_extend', 'date_protect', 'k_number', 'k_start_date', 'k_end_date', 'p_number', 'p_start_date', 'p_end_date', 'k_niti', 'k_dis_con_02', 'k_free', 'k_type', 'k_type_remark', 'k_fix', 'p_year', 'p_call_date', 'p_get_date', 'p_call_number', 'p_remark', 'pay_type', 'status', 'k_send_date', 'k_send_code', 'p_send_date', 'p_send_code', 'bill_send_date', 'bill_send_code', 'pasee_send_date', 'pasee_send_code', 'prepare_date', 'prepare_no', 'direct', 'p_acc_remark', 'k_acc_remark', 'p_rnum', 'p_rnum_date', 'insure_date', 'k_new', 'officer_date', 'k_status', 'doc01', 'doc02', 'doc03', 'doc04', 'doc05', 'doc06', 'doc07', 'doc08', 'doc09', 'doc10', 'k_ins_date', 'k_acc_date', 'k_sale_date', 'k_ins_remark', 'p_status', 'k_kom_status', 'k_cancel_date', 'k_cancel_remark', 'product_type', 'car_number', 'car_plate'], 'safe'],
            [['k_num01', 'k_num02', 'k_num03', 'k_num04', 'k_num05', 'k_num06', 'k_num07', 'k_num08', 'k_num09', 'k_num10', 'k_num11', 'k_num12', 'k_num13', 'k_dis_con_01', 'p_prb_price', 'p_insure_vat', 'p_argon', 'p_total', 'p_acc_com', 'k_acc_com', 'p_vat', 'k_cancel_price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TInsure::find()
        ->leftjoin('t_insure_car','t_insure_car.car_id = t_insure.car_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $date1 = mktime(0,0,0,date("m"),(date("d")-180),date("Y"));
        $date2 = date("Y-m-d",$date1);
        
        $query->andFilterWhere(['like', 't_insure_car.car_number', $this->car_number])
        ->andFilterWhere(['like', 't_insure_car.code', $this->car_plate])
        ->andFilterWhere(['AND', 
            ['OR',
                ['>', 't_insure.k_start_date', $date2], ['>', 't_insure.p_start_date', $date2]
            ],
        ]);

        return $dataProvider;
    }

    public function searchPayment($params)
    {
        $connection    = Yii::$app->get('db_buzz_control');
        $queryInterest = 'SELECT
                b.insure_id,
                sum(payment_price) AS premium,
                (
                    SELECT
                        sum(x.payment_price)
                    FROM
                        t_insure_item x
                    WHERE
                        b.insure_id = x.insure_id
                    AND x.insure_date <> "0000-00-00"
                ) AS premium_received
            FROM
                t_insure a,
                t_insure_item b
            WHERE
                (
                    `status` <> "cancel"
                    OR STATUS IS NULL
                )
            AND a.insure_id = b.insure_id
            AND a.insure_id = 28223
            GROUP BY
                1,
                3;';
        $command             = $connection->createCommand($queryInterest);
        $resultInterest      = $command->queryOne();
        return $resultInterest;
    }   
}
