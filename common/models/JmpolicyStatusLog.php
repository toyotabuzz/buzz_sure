<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "jmpolicy_status_log".
 *
 * @property int|null $id
 * @property int|null $insure_id
 * @property int|null $k_number
 * @property int|null $policy_status_id
 * @property int|null $policy_status_second_id
 * @property string|null $k_ins_remark
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 */
class JmpolicyStatusLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jmpolicy_status_log';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => empty(Yii::$app->user->identity->id) ? null : Yii::$app->user->identity->id,
            ],
        ];
    }

    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'insure_id', 'policy_status_id', 'policy_status_second_id', 'created_by', 'updated_by'], 'integer'],
            [['k_number'], 'string','max' => 50],
            [['k_ins_remark'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                      => 'ID',
            'insure_id'               => 'Insure ID',
            'k_number'                => 'K Number',
            'policy_status_id'        => 'Policy Status ID',
            'policy_status_second_id' => 'Policy Status Second ID',
            'k_ins_remark'            => 'K Ins Remark',
            'created_at'              => 'Created At',
            'created_by'              => 'Created By',
            'updated_at'              => 'Updated At',
            'updated_by'              => 'Updated By',
        ];
    }
}
