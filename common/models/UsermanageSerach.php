<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tuuser;

/**
 * UsermanageSerach represents the model behind the search form about `common\models\Tuuser`.
 */
class UsermanageSerach extends Tuuser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'level_id', 'position_id', 'department_id', 'branch_id'], 'integer'],
            [['username', 'password', 'emp_no', 'firstname', 'lastname', 'firstname_en', 'lastname_en', 'nickname', 'email', 'status', 'create_date', 'create_by', 'update_date', 'update_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tuuser::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'level_id' => $this->level_id,
            'position_id' => $this->position_id,
            'department_id' => $this->department_id,
            'branch_id' => $this->branch_id,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'emp_no', $this->emp_no])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'firstname_en', $this->firstname_en])
            ->andFilterWhere(['like', 'lastname_en', $this->lastname_en])
            ->andFilterWhere(['like', 'nickname', $this->nickname])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
    // ฟังก์ชันเรียกดูข้อมลพนักงาน
    // return array
    public function getArrUserByBranchId($branchId = null, $departmentId = null, $positionId = null)
    {
        $result = [];
        // if (!empty($branchId)) {
            $result = Tuuser::find()
            ->leftJoin('tucomp', 'tucomp.id = tuuser.branch_id')
            ->leftJoin('tudep', 'tudep.id = tuuser.department_id')
            ->leftJoin('tuposition', 'tuposition.id = tuuser.position_id')
            ->select('tuuser.id, tuuser.emp_no, tuuser.firstname, tuuser.lastname, tuuser.nickname, tuuser.branch_id, tucomp.short_title, tuuser.department_id, tudep.dep_name, tuuser.position_id, tuposition.position_name, tuuser.mobile_no')
            ->andFilterWhere(['tuuser.branch_id' => $branchId])
            ->andFilterWhere(['tuuser.department_id' => $departmentId])
            ->andFilterWhere(['tuuser.position_id'=> $positionId])
            ->asArray()->all();
        // }
        return $result;
    }

    // ฟังก์ชันเรียกดูข้อมลพนักงานขาย
    // return array
    public function getArrUserLeaderSale($branchId = null, $managerId = null, $leaderId = null)
    {
        $result = [];
        // if (!empty($branchId)) {
            $result = Tuuser::find()
            ->leftJoin('tucomp', 'tucomp.id = tuuser.branch_id')
            ->leftJoin('tudep', 'tudep.id = tuuser.department_id')
            ->leftJoin('tuposition', 'tuposition.id = tuuser.position_id')
            ->select('tuuser.id, tuuser.emp_no, tuuser.firstname, tuuser.lastname, tuuser.nickname, tuuser.branch_id, tucomp.short_title, tuuser.department_id, tudep.dep_name, tuuser.position_id, tuposition.position_name, tuuser.mobile_no')
            ->innerJoin('tusteam', 'tusteam.head_id = tuuser.id')
            ->andWhere(['tuuser.department_id' => 4])
            ->andFilterWhere(['tuuser.branch_id' => $branchId])
            ->andFilterWhere(['tusteam.manager_user_id' => $managerId])
            ->asArray()->all();
        // }
        return $result;
    }

    // ฟังก์ชันเรียกดูข้อมลพนักงานขาย
    // return array
    public function getArrUserSale($branchId = null, $managerId = null, $leaderId = null)
    {
        $result = [];
        // if (!empty($branchId)) {
            $result = Tuuser::find()
            ->leftJoin('tucomp', 'tucomp.id = tuuser.branch_id')
            ->leftJoin('tudep', 'tudep.id = tuuser.department_id')
            ->leftJoin('tuposition', 'tuposition.id = tuuser.position_id')
            ->select('tuuser.id, tuuser.emp_no, tuuser.firstname, tuuser.lastname, tuuser.nickname, tuuser.branch_id, tucomp.short_title, tuuser.department_id, tudep.dep_name, tuuser.position_id, tuposition.position_name, tuuser.mobile_no')
            ->innerJoin('tusaleteam', 'tusaleteam.sale_id = tuuser.id')
            ->leftJoin('tusteam', 'tusteam.id = tusaleteam.team_id')
            ->andFilterWhere(['tuuser.department_id' => 4])
            ->andFilterWhere(['tuuser.branch_id' => $branchId])
            ->andFilterWhere(['tusteam.manager_user_id' => $managerId])
            ->andFilterWhere(['tusteam.head_id' => $leaderId])
            ->asArray()->all();
        // }
        return $result;
    }
}
