<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TInsureItemDetail;

/**
 * TInsureItemDetailSearch represents the model behind the search form of `common\models\TInsureItemDetail`.
 */
class TInsureItemDetailSearch extends TInsureItemDetail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_price_id', 'order_id', 'order_extra_id', 'order_payment_id', 'payment_subject_id', 'qty', 'status', 'vat', 'payin'], 'integer'],
            [['price', 'price_acc'], 'number'],
            [['remark'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TInsureItemDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_price_id'     => $this->order_price_id,
            'order_id'           => $this->order_id,
            'order_extra_id'     => $this->order_extra_id,
            'order_payment_id'   => $this->order_payment_id,
            'payment_subject_id' => $this->payment_subject_id,
            'qty'                => $this->qty,
            'price'              => $this->price,
            'status'             => $this->status,
            'vat'                => $this->vat,
            'payin'              => $this->payin,
            'price_acc'          => $this->price_acc,
        ]);

        $query->andFilterWhere(['like', 'remark', $this->remark]);

        return $dataProvider;
    }
}
