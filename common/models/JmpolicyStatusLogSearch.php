<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\JmpolicyStatusLog;

/**
 * JmpolicyStatusLogSearch represents the model behind the search form of `common\models\JmpolicyStatusLog`.
 */
class JmpolicyStatusLogSearch extends JmpolicyStatusLog
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'insure_id', 'policy_status_id', 'policy_status_second_id', 'created_by', 'updated_by'], 'integer'],
            [['k_number', 'k_ins_remark', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JmpolicyStatusLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'                      => $this->id,
            'insure_id'               => $this->insure_id,
            'policy_status_id'        => $this->policy_status_id,
            'policy_status_second_id' => $this->policy_status_second_id,
            'created_at'              => $this->created_at,
            'created_by'              => $this->created_by,
            'updated_at'              => $this->updated_at,
            'updated_by'              => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'k_number', $this->k_number])
            ->andFilterWhere(['like', 'k_ins_remark', $this->k_ins_remark]);

        return $dataProvider;
    }
}
