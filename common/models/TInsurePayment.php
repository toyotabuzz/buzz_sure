<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure_payment".
 *
 * @property int $insure_payment_id
 * @property int $order_id
 * @property int $order_extra_id
 * @property int $payment_type_id
 * @property float $price
 * @property int $bank_id
 * @property string $branch
 * @property string $check_no
 * @property string $check_date
 * @property int $status
 * @property string $remark
 * @property int $no_head
 * @property int $bookbank_id
 * @property string $acc_check
 * @property string $acc_check_date
 * @property string $acc_check_remark
 * @property int $acc_check_by
 * @property int $payment_no
 * @property string $sale_date
 */
class TInsurePayment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure_payment';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'order_extra_id', 'payment_type_id', 'bank_id', 'status', 'no_head', 'bookbank_id', 'acc_check_by', 'payment_no'], 'integer'],
            [['price'], 'number'],
            [['check_date', 'acc_check_date', 'sale_date'], 'required'],
            [['check_date', 'acc_check_date', 'sale_date'], 'safe'],
            [['branch'], 'string', 'max' => 50],
            [['check_no'], 'string', 'max' => 20],
            [['remark', 'acc_check_remark'], 'string', 'max' => 255],
            [['acc_check'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insure_payment_id' => 'Insure Payment ID',
            'order_id' => 'Order ID',
            'order_extra_id' => 'Order Extra ID',
            'payment_type_id' => 'Payment Type ID',
            'price' => 'Price',
            'bank_id' => 'Bank ID',
            'branch' => 'Branch',
            'check_no' => 'Check No',
            'check_date' => 'Check Date',
            'status' => 'Status',
            'remark' => 'Remark',
            'no_head' => 'No Head',
            'bookbank_id' => 'Bookbank ID',
            'acc_check' => 'Acc Check',
            'acc_check_date' => 'Acc Check Date',
            'acc_check_remark' => 'Acc Check Remark',
            'acc_check_by' => 'Acc Check By',
            'payment_no' => 'Payment No',
            'sale_date' => 'Sale Date',
        ];
    }
}
