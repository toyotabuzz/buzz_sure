<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure_stock_prb".
 *
 * @property int $insure_stock_id
 * @property int $insure_id
 * @property string $stock_number
 * @property int $insure_company_id
 * @property string $remark
 * @property string $status
 * @property string $status_date
 * @property int $status_company_id
 * @property int $status_department_id
 * @property int $status_officer_id
 * @property string $changes
 * @property string $change_date
 * @property string $change_form_id
 * @property string $cancel
 * @property string $cancel_date
 * @property int $cancel_form_id
 * @property string $stock_date
 * @property int $stock_company_id
 * @property int $stock_department_id
 * @property int $stock_officer_id
 * @property int $send_code
 * @property int $doc_send
 * @property int $run_stock
 * @property string $stock_code
 * @property int $p_stock_id
 */
class TInsureStockPrb extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure_stock_prb';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insure_id', 'insure_company_id', 'status_company_id', 'status_department_id', 'status_officer_id', 'cancel_form_id', 'stock_company_id', 'stock_department_id', 'stock_officer_id', 'send_code', 'doc_send', 'run_stock', 'p_stock_id'], 'integer'],
            [['remark', 'status_date', 'change_date', 'change_form_id', 'cancel_date', 'stock_date'], 'required'],
            [['remark', 'change_form_id'], 'string'],
            [['status_date', 'change_date', 'cancel_date', 'stock_date'], 'safe'],
            [['stock_number'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 30],
            [['changes', 'cancel'], 'string', 'max' => 3],
            [['stock_code'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insure_stock_id' => 'Insure Stock ID',
            'insure_id' => 'Insure ID',
            'stock_number' => 'Stock Number',
            'insure_company_id' => 'Insure Company ID',
            'remark' => 'Remark',
            'status' => 'Status',
            'status_date' => 'Status Date',
            'status_company_id' => 'Status Company ID',
            'status_department_id' => 'Status Department ID',
            'status_officer_id' => 'Status Officer ID',
            'changes' => 'Changes',
            'change_date' => 'Change Date',
            'change_form_id' => 'Change Form ID',
            'cancel' => 'Cancel',
            'cancel_date' => 'Cancel Date',
            'cancel_form_id' => 'Cancel Form ID',
            'stock_date' => 'Stock Date',
            'stock_company_id' => 'Stock Company ID',
            'stock_department_id' => 'Stock Department ID',
            'stock_officer_id' => 'Stock Officer ID',
            'send_code' => 'Send Code',
            'doc_send' => 'Doc Send',
            'run_stock' => 'Run Stock',
            'stock_code' => 'Stock Code',
            'p_stock_id' => 'P Stock ID',
        ];
    }
}
