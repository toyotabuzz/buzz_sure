<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_car_model".
 *
 * @property int $car_model_id
 * @property int $car_type_id
 * @property string $sale_model
 * @property string $sale_model_01
 * @property string $title
 * @property int $rank
 * @property string $expire
 * @property string $acc03
 * @property string $insure_name
 */
class TCarModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_car_model';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_type_id', 'rank'], 'integer'],
            [['sale_model', 'sale_model_01', 'rank', 'expire', 'acc03', 'insure_name'], 'required'],
            [['expire'], 'safe'],
            [['sale_model', 'title'], 'string', 'max' => 100],
            [['sale_model_01'], 'string', 'max' => 10],
            [['acc03'], 'string', 'max' => 3],
            [['insure_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'car_model_id' => 'Car Model ID',
            'car_type_id' => 'Car Type ID',
            'sale_model' => 'Sale Model',
            'sale_model_01' => 'Sale Model 01',
            'title' => 'Title',
            'rank' => 'Rank',
            'expire' => 'Expire',
            'acc03' => 'Acc03',
            'insure_name' => 'Insure Name',
        ];
    }
}
