<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "turun".
 *
 * @property string $run_type
 * @property string $run_code
 * @property int $run_no
 * @property string|null $created_at
 * @property string|null $created_by
 * @property string|null $updated_at
 * @property string|null $updated_by
 */
class Turun extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'turun';
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => empty(Yii::$app->user->identity->id) ? null : Yii::$app->user->identity->id,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['run_type', 'run_code', 'run_no'], 'required'],
            [['run_no'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['run_type', 'run_code'], 'string', 'max' => 50],
            [['created_by', 'updated_by'], 'string', 'max' => 20],
            [['run_type', 'run_code', 'run_no'], 'unique', 'targetAttribute' => ['run_type', 'run_code', 'run_no']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'run_type' => 'Run Type',
            'run_code' => 'Run Code',
            'run_no' => 'Run No',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
