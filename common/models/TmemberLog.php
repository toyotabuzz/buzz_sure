<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "t_member_log".
 *
 * @property integer $member_log_id
 * @property integer $member_id
 * @property string $date_log
 * @property string $module_code
 * @property string $action
 * @property string $url
 * @property string $ip
 *
 * @property Tmember $user
 */
class TmemberLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_member_log';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }
    /**
    **
    public function behaviors()
    {
        
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->identity->username,
            ],
        ];
    }  
    *
    */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id'], 'required'],
            [['member_id'], 'integer'],
            [['date_log', 'updated_at'], 'safe'],
            [['ip'], 'string', 'max' => 20],
            [['action'], 'string', 'max' => 100],
            [['module_code', 'url'], 'string', 'max' => 225],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tmember::className(), 'targetAttribute' => ['member_id' => 'member_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'member_log_id' => 'ID',
            'member_id'     => 'Member ID',
            'date_log'      => 'Date Log',
            'module_code'   => 'Module Code',
            'action'        => 'Action',
            'url'           => 'Url',
            'ip'            => 'Ip Address',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Tmember::className(), ['member_id' => 'member_id']);
    }
    
    /**
    **
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->attribute       = $this->attribute;            
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        $this->attribute       = $this->attribute;        
    }

    public function afterFind()
    {
        $this->attribute       = $this->attribute;

        parent::afterFind();
    }
    *
    */
}
