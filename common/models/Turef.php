<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "turef".
 *
 * @property int $id
 * @property string $ref_type
 * @property string $ref_code
 * @property int|null $ref_seq
 * @property string $ref_desc
 * @property string $ref_value
 * @property string|null $ref_remark
 * @property string|null $isactive 'Y'=ใช้งาน, 'N'=ไม่ใช้งาน
 * @property string|null $create_date
 * @property string|null $create_by
 * @property string|null $update_date
 * @property string|null $update_by
 */

class Turef extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'turef';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'update_date',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'create_by',
                'updatedByAttribute' => 'update_by',
                'value' => empty(Yii::$app->user->identity->id) ? null : Yii::$app->user->identity->id,
            ],
        ];
    }

    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ref_type', 'ref_code', 'ref_desc', 'ref_value'], 'required'],
            [['ref_seq'], 'integer'],
            [['ref_remark'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['ref_type', 'ref_code'], 'string', 'max' => 30],
            [['ref_desc'], 'string', 'max' => 150],
            [['ref_value', 'create_by', 'update_by'], 'string', 'max' => 20],
            [['isactive'], 'string', 'max' => 1],
            [['ref_type', 'ref_code', 'ref_value'], 'unique', 'targetAttribute' => ['ref_type', 'ref_code', 'ref_value']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ref_type' => 'Ref Type',
            'ref_code' => 'Ref Code',
            'ref_seq' => 'Ref Seq',
            'ref_desc' => 'Ref Desc',
            'ref_value' => 'Ref Value',
            'ref_remark' => 'Ref Remark',
            'isactive' => 'Isactive',
            'create_date' => 'Create Date',
            'create_by' => 'Create By',
            'update_date' => 'Update Date',
            'update_by' => 'Update By',
        ];
    }
}
