<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tumenusub;

/**
 * ManagemenusubSerach represents the model behind the search form about `common\models\Tumenusub`.
 */
class ManagemenusubSerach extends Tumenusub
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tumenu_id'], 'integer'],
            [['submenu_code', 'submenu_label', 'submenu_url', 'submenu_icon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tumenusub::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tumenu_id' => $this->tumenu_id,
        ]);

        $query->andFilterWhere(['like', 'submenu_code', $this->submenu_code])
            ->andFilterWhere(['like', 'submenu_label', $this->submenu_label])
            ->andFilterWhere(['like', 'submenu_url', $this->submenu_url])
            ->andFilterWhere(['like', 'submenu_icon', $this->submenu_icon]);

        return $dataProvider;
    }
}
