<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure_form".
 *
 * @property int $insure_form_id
 * @property string $form_date
 * @property int $insure_id
 * @property string $frm_type
 * @property string $code
 * @property string $title
 * @property string $remark
 * @property string $t01
 * @property string $t02
 * @property string $t03
 * @property string $t04
 * @property string $t05
 * @property string $t06
 * @property string $t07
 * @property string $t08
 * @property string $t09
 * @property string $t10
 * @property string $t11
 * @property string $t12
 * @property string $t13
 * @property string $t14
 * @property string $t15
 * @property string $t16
 * @property string $t17
 * @property string $t18
 * @property string $t19
 * @property string $t20
 * @property string $t21
 * @property string $t22
 * @property string $t23
 * @property string $t24
 * @property string $t25
 * @property string $t26
 * @property string $t27
 * @property string $t28
 * @property string $t29
 * @property string $t30
 * @property string $t31
 * @property string $t32
 * @property string $t33
 * @property string $t34
 * @property string $t35
 * @property string $t36
 * @property string $t37
 * @property string $t38
 * @property string $t39
 * @property string $t40
 * @property string $t41
 * @property string $t42
 * @property string $t43
 * @property string $t44
 * @property string $t45
 * @property string $t46
 * @property string $t47
 * @property string $t48
 * @property string $t49
 * @property string $t50
 * @property string $t51
 * @property string $t52
 * @property string $t53
 * @property string $t54
 * @property string $t55
 * @property string $t56
 * @property string $t57
 * @property string $t58
 * @property string $t59
 * @property string $t60
 * @property string $t61
 * @property string $t62
 * @property string $t63
 * @property string $t64
 * @property string $t65
 * @property string $t66
 * @property string $t67
 * @property string $t68
 * @property string $t69
 * @property string $t70
 * @property string $t71
 * @property string $t72
 * @property string $t73
 * @property string $t74
 * @property string $t75
 * @property string $t76
 * @property string $t77
 * @property string $t78
 * @property string $t79
 * @property string $t80
 * @property string $t81
 * @property string $t82
 * @property string $t83
 * @property string $t84
 * @property string $t85
 * @property string $t86
 * @property string $t87
 * @property string $t88
 * @property string $t89
 * @property string $t90
 * @property string $t91
 * @property string $t92
 * @property string $t93
 * @property string $t94
 * @property string $t95
 * @property string $t96
 * @property string $t97
 * @property string $t98
 * @property string $t99
 * @property string $t00
 * @property string $sup_approve
 * @property string $sup_date
 * @property int $sup_id
 * @property string $sup_remark
 * @property string $admin_approve
 * @property string $admin_date
 * @property int $admin_id
 * @property string $admin_remark
 * @property string $admin_code
 * @property string $admin_code_date
 */
class TInsureForm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure_form';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['form_date', 'remark', 't11', 't12', 't13', 'sup_approve', 'sup_date', 'sup_id', 'sup_remark', 'admin_approve', 'admin_date', 'admin_id', 'admin_remark', 'admin_code', 'admin_code_date'], 'required'],
            [['form_date', 'sup_date', 'admin_date', 'admin_code_date'], 'safe'],
            [['insure_id', 'sup_id', 'admin_id'], 'integer'],
            [['remark', 't11', 't12', 't13', 'sup_remark', 'admin_remark'], 'string'],
            [['frm_type', 'code', 'sup_approve', 'admin_approve'], 'string', 'max' => 20],
            [['title', 't01', 't02', 't03', 't04', 't05', 't06', 't07', 't08', 't09', 't10', 't14', 't15', 't16', 't17', 't18', 't19', 't20', 't21', 't22', 't23', 't24', 't25', 't26', 't27', 't28', 't29', 't30', 't31', 't32', 't33', 't34', 't35', 't36', 't37', 't38', 't39', 't40', 't41', 't42', 't43', 't44', 't45', 't46', 't47', 't48', 't49', 't50', 't51', 't52', 't53', 't54', 't55', 't56', 't57', 't58', 't59', 't60', 't61', 't62', 't63', 't64', 't65', 't66', 't67', 't68', 't69', 't70', 't71', 't72', 't73', 't74', 't75', 't76', 't77', 't78', 't79', 't80', 't81', 't82', 't83', 't84', 't85', 't86', 't87', 't88', 't89', 't90', 't91', 't92', 't93', 't94', 't95', 't96', 't97', 't98', 't99', 't00'], 'string', 'max' => 255],
            [['admin_code'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insure_form_id' => 'Insure Form ID',
            'form_date' => 'Form Date',
            'insure_id' => 'Insure ID',
            'frm_type' => 'Frm Type',
            'code' => 'Code',
            'title' => 'Title',
            'remark' => 'Remark',
            't01' => 'T01',
            't02' => 'T02',
            't03' => 'T03',
            't04' => 'T04',
            't05' => 'T05',
            't06' => 'T06',
            't07' => 'T07',
            't08' => 'T08',
            't09' => 'T09',
            't10' => 'T10',
            't11' => 'T11',
            't12' => 'T12',
            't13' => 'T13',
            't14' => 'T14',
            't15' => 'T15',
            't16' => 'T16',
            't17' => 'T17',
            't18' => 'T18',
            't19' => 'T19',
            't20' => 'T20',
            't21' => 'T21',
            't22' => 'T22',
            't23' => 'T23',
            't24' => 'T24',
            't25' => 'T25',
            't26' => 'T26',
            't27' => 'T27',
            't28' => 'T28',
            't29' => 'T29',
            't30' => 'T30',
            't31' => 'T31',
            't32' => 'T32',
            't33' => 'T33',
            't34' => 'T34',
            't35' => 'T35',
            't36' => 'T36',
            't37' => 'T37',
            't38' => 'T38',
            't39' => 'T39',
            't40' => 'T40',
            't41' => 'T41',
            't42' => 'T42',
            't43' => 'T43',
            't44' => 'T44',
            't45' => 'T45',
            't46' => 'T46',
            't47' => 'T47',
            't48' => 'T48',
            't49' => 'T49',
            't50' => 'T50',
            't51' => 'T51',
            't52' => 'T52',
            't53' => 'T53',
            't54' => 'T54',
            't55' => 'T55',
            't56' => 'T56',
            't57' => 'T57',
            't58' => 'T58',
            't59' => 'T59',
            't60' => 'T60',
            't61' => 'T61',
            't62' => 'T62',
            't63' => 'T63',
            't64' => 'T64',
            't65' => 'T65',
            't66' => 'T66',
            't67' => 'T67',
            't68' => 'T68',
            't69' => 'T69',
            't70' => 'T70',
            't71' => 'T71',
            't72' => 'T72',
            't73' => 'T73',
            't74' => 'T74',
            't75' => 'T75',
            't76' => 'T76',
            't77' => 'T77',
            't78' => 'T78',
            't79' => 'T79',
            't80' => 'T80',
            't81' => 'T81',
            't82' => 'T82',
            't83' => 'T83',
            't84' => 'T84',
            't85' => 'T85',
            't86' => 'T86',
            't87' => 'T87',
            't88' => 'T88',
            't89' => 'T89',
            't90' => 'T90',
            't91' => 'T91',
            't92' => 'T92',
            't93' => 'T93',
            't94' => 'T94',
            't95' => 'T95',
            't96' => 'T96',
            't97' => 'T97',
            't98' => 'T98',
            't99' => 'T99',
            't00' => 'T00',
            'sup_approve' => 'Sup Approve',
            'sup_date' => 'Sup Date',
            'sup_id' => 'Sup ID',
            'sup_remark' => 'Sup Remark',
            'admin_approve' => 'Admin Approve',
            'admin_date' => 'Admin Date',
            'admin_id' => 'Admin ID',
            'admin_remark' => 'Admin Remark',
            'admin_code' => 'Admin Code',
            'admin_code_date' => 'Admin Code Date',
        ];
    }

    public function getTInsure()
    {
        return $this->hasOne(TInsure::className(), ['insure_id' => 'insure_id']);
    }
}
