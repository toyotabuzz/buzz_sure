<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_order".
 *
 * @property int $order_id
 * @property string|null $order_date
 * @property int $order_status
 * @property string $order_number
 * @property string $recieve_number
 * @property string $booking_number
 * @property int $booking_customer_id
 * @property string $booking_number_nohead
 * @property string|null $booking_date
 * @property string $booking_cashier_date
 * @property string $booking_cashier_time
 * @property int $booking_cashier_id
 * @property int $booking_car_color
 * @property int $booking_car_series
 * @property int $booking_car_type
 * @property string $booking_car_gear
 * @property float $booking_car_price
 * @property string $booking_car_remark
 * @property int $booking_sale_id
 * @property int $booking_team_id
 * @property string $booking_remark
 * @property string $booking_cancel_date
 * @property string $booking_cancel_number
 * @property string $booking_cancel_reason
 * @property float $booking_cancel_price
 * @property string $booking_cancel_approve
 * @property int $booking_mail
 * @property string $booking_mail_date
 * @property int $booking_thankyou
 * @property string $sending_number
 * @property string $sending_number_nohead
 * @property int $sending_customer_id
 * @property string $sending_date
 * @property int $sending_sale_id
 * @property int $sending_team_id
 * @property string $sending_remark
 * @property string $sending_car_remark
 * @property string $sending_cancel_date
 * @property string $sending_cancel_number
 * @property string $sending_cancel_reason
 * @property float $sending_cancel_price
 * @property string $sending_cancel_approve
 * @property int $sending_mail
 * @property string $sending_mail_date
 * @property string $sending_cashier_date
 * @property string $sending_cashier_time
 * @property int $sending_cashier_id
 * @property int $sending_thankyou
 * @property int $switch_sale
 * @property int $switch_car
 * @property int $switch_customer
 * @property float $order_price
 * @property float $order_reserve_price
 * @property float $order_reserve_price01
 * @property float $order_reserve_price02
 * @property float $order_old_car
 * @property string $order_old_car_number
 * @property string $order_location
 * @property int $stock_car_id
 * @property int $stock_red_id
 * @property float $red_code_price
 * @property int $no_red_code
 * @property int $black_code_status
 * @property string $black_code_number
 * @property string $black_code_date
 * @property string $recieve_date
 * @property int $registry_status
 * @property float $registry_price
 * @property float $registry_price1
 * @property int $registry_tax_year
 * @property int $registry_tax_year_number
 * @property string $registry_send_date
 * @property string $registry_miss_document
 * @property int $registry_take_code
 * @property string $registry_take_code_date
 * @property int $registry_take_book
 * @property string $registry_take_book_date
 * @property int $registry_condition_id
 * @property string $registry_remark
 * @property int $registry_edit_by
 * @property string $registry_edit_date
 * @property int $registry_person
 * @property string $registry_date_01
 * @property string $registry_date_02
 * @property string $registry_date_05
 * @property string $registry_date_07
 * @property string $registry_text_03
 * @property string $registry_text_04
 * @property string $registry_text_05
 * @property string $registry_text_06
 * @property string $registry_text_07
 * @property string $registry_remark_01
 * @property string $registry_remark_02
 * @property string $registry_remark_03
 * @property string $registry_remark_04
 * @property string $registry_remark_05
 * @property string $registry_remark_06
 * @property string $registry_remark_07
 * @property string $registry_remark_08
 * @property string $registry_recieve_type_01
 * @property string $registry_recieve_type_02
 * @property string $registry_recieve_name_01
 * @property string $registry_recieve_name_02
 * @property string $registry_sign
 * @property string $registry_sign_date
 * @property int $buy_type
 * @property int $buy_company
 * @property float $buy_fee
 * @property int $buy_time
 * @property float $buy_payment
 * @property float $buy_price
 * @property float $buy_down
 * @property float $buy_total
 * @property string $buy_remark
 * @property float $buy_begin
 * @property float $buy_product
 * @property float $buy_engine
 * @property string $buy_engine_remark
 * @property int $prb_company
 * @property string $prb_expire
 * @property float $prb_price
 * @property int $insure_company
 * @property string $insure_expire
 * @property float $insure_price
 * @property string $insure_type
 * @property int $insure_year
 * @property float $insure_budget
 * @property int $insure_from
 * @property string $insure_from_detail
 * @property string $insure_remark
 * @property float $insure_customer_pay
 * @property int $insure_import
 * @property string $other_product
 * @property int $other_product_qty
 * @property float $other_product_price
 * @property int $other_product_tmt
 * @property string $other_premium
 * @property int $other_premium_qty
 * @property float $other_premium_price
 * @property int $other_premium_tmt
 * @property float $discount_price
 * @property float $discount_subdown
 * @property float $discount_premium
 * @property float $discount_insurance
 * @property int $discount_goa
 * @property float $discount_product
 * @property float $discount
 * @property float $discount_subdown_vat
 * @property float $discount_subdown_vat_percent
 * @property float $discount_margin
 * @property string $discount_margin_date
 * @property string $discount_other
 * @property float $discount_other_price
 * @property float $discount_all
 * @property float $discount_over
 * @property float $discount_customer_pay
 * @property string $discount_customer_pay_reason
 * @property float $discount_tmt
 * @property int $tmb_number
 * @property float $tmt_free
 * @property string $tmt_free_remark
 * @property float $tmt_coupon
 * @property string $tmt_coupon_remark
 * @property float $tmt_other
 * @property string $tmt_other_remark
 * @property string $finance_date
 * @property string $finance_number
 * @property float $com_car
 * @property float $com_finance
 * @property float $com_insure
 * @property float $com_old_car
 * @property float $com_equipment
 * @property float $com_extra
 * @property string $com_extra_remark
 * @property string $finance_remain
 * @property float $finance_com
 * @property float $finance_com_insure
 * @property float $finance_face_car
 * @property float $finance_face_com
 * @property string $scan_01
 * @property string $scan_02
 * @property string $scan_03
 * @property string $scan_04
 * @property string $scan_05
 * @property string $scan_06
 * @property string $scan_07
 * @property string $scan_08
 * @property string $scan_09
 * @property string $scan_10
 * @property string $scan_11
 * @property int $car_margin_id
 * @property int $credit
 * @property string $credit_date
 * @property string $credit_sending_date
 * @property int $dealer
 * @property string $tax_date
 * @property string $tax_duedate
 * @property float $tax_price
 * @property string $estimate_finance_date
 * @property int $add_by
 * @property string $add_date
 * @property int $edit_by
 * @property string $edit_date
 * @property int $delete_by
 * @property string $delete_date
 * @property string $delete_reason
 * @property int $delete_status
 * @property int $check_purchase
 * @property string $check_purchase_remark
 * @property int $check_purchase_by
 * @property int $check_account_booking
 * @property string $check_account_booking_date
 * @property string $check_account_booking_remark
 * @property int $check_account_booking_by
 * @property int $check_account_sending
 * @property string $check_account_sending_date
 * @property string $check_account_sending_remark
 * @property int $check_account_sending_by
 * @property int $check_account_sending_com
 * @property string $stock_status
 * @property string $stock_car_status
 * @property string $stock_crl_status
 * @property int $company_id
 * @property int $company_ids
 * @property int $booking_place
 * @property int $sending_place
 * @property int $change_product
 * @property string $change_number
 * @property string $change_remark
 * @property string $booking_cancel_reason_remark
 * @property string $buy_reason
 * @property int $bond01
 * @property string $bond01_relate
 * @property string $bond01_relate_remark
 * @property string $bond01_remark
 * @property int $bond02
 * @property string $bond02_relate
 * @property string $bond02_relate_remark
 * @property string $bond02_remark
 * @property string $purchase_date
 */
class TOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_order';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_date', 'booking_date', 'booking_cashier_date', 'booking_cashier_time', 'booking_cancel_date', 'booking_mail_date', 'sending_date', 'sending_cancel_date', 'sending_mail_date', 'sending_cashier_date', 'sending_cashier_time', 'black_code_date', 'recieve_date', 'registry_send_date', 'registry_take_code_date', 'registry_take_book_date', 'registry_edit_date', 'registry_date_01', 'registry_date_02', 'registry_date_05', 'registry_date_07', 'registry_sign_date', 'prb_expire', 'insure_expire', 'discount_margin_date', 'finance_date', 'credit_date', 'credit_sending_date', 'tax_date', 'tax_duedate', 'estimate_finance_date', 'add_date', 'edit_date', 'delete_date', 'check_account_booking_date', 'check_account_sending_date', 'purchase_date'], 'safe'],
            [['order_status', 'booking_customer_id', 'booking_cashier_id', 'booking_car_color', 'booking_car_series', 'booking_car_type', 'booking_sale_id', 'booking_team_id', 'booking_mail', 'booking_thankyou', 'sending_customer_id', 'sending_sale_id', 'sending_team_id', 'sending_mail', 'sending_cashier_id', 'sending_thankyou', 'switch_sale', 'switch_car', 'switch_customer', 'stock_car_id', 'stock_red_id', 'no_red_code', 'black_code_status', 'registry_status', 'registry_tax_year', 'registry_tax_year_number', 'registry_take_code', 'registry_take_book', 'registry_condition_id', 'registry_edit_by', 'registry_person', 'buy_type', 'buy_company', 'buy_time', 'prb_company', 'insure_company', 'insure_year', 'insure_from', 'insure_import', 'other_product_qty', 'other_product_tmt', 'other_premium_qty', 'other_premium_tmt', 'discount_goa', 'tmb_number', 'car_margin_id', 'credit', 'dealer', 'add_by', 'edit_by', 'delete_by', 'delete_status', 'check_purchase', 'check_purchase_by', 'check_account_booking', 'check_account_booking_by', 'check_account_sending', 'check_account_sending_by', 'check_account_sending_com', 'company_id', 'company_ids', 'booking_place', 'sending_place', 'change_product', 'bond01', 'bond02'], 'integer'],
            [['recieve_number', 'booking_number_nohead', 'booking_cashier_date', 'booking_cashier_time', 'booking_cashier_id', 'booking_car_price', 'booking_car_remark', 'booking_team_id', 'booking_remark', 'booking_cancel_date', 'booking_cancel_number', 'booking_mail', 'booking_mail_date', 'booking_thankyou', 'sending_number_nohead', 'sending_date', 'sending_team_id', 'sending_remark', 'sending_car_remark', 'sending_cancel_date', 'sending_mail', 'sending_mail_date', 'sending_cashier_date', 'sending_cashier_time', 'sending_cashier_id', 'sending_thankyou', 'order_reserve_price01', 'order_reserve_price02', 'order_old_car', 'order_old_car_number', 'no_red_code', 'black_code_date', 'recieve_date', 'registry_status', 'registry_price', 'registry_price1', 'registry_tax_year', 'registry_tax_year_number', 'registry_send_date', 'registry_miss_document', 'registry_take_code', 'registry_take_code_date', 'registry_take_book', 'registry_take_book_date', 'registry_condition_id', 'registry_remark', 'registry_edit_by', 'registry_edit_date', 'registry_person', 'registry_date_01', 'registry_date_02', 'registry_date_05', 'registry_date_07', 'registry_text_03', 'registry_text_04', 'registry_text_05', 'registry_text_06', 'registry_text_07', 'registry_remark_01', 'registry_remark_02', 'registry_remark_03', 'registry_remark_04', 'registry_remark_05', 'registry_remark_06', 'registry_remark_07', 'registry_remark_08', 'registry_recieve_type_01', 'registry_recieve_type_02', 'registry_recieve_name_01', 'registry_recieve_name_02', 'registry_sign', 'registry_sign_date', 'buy_begin', 'buy_product', 'buy_engine', 'buy_engine_remark', 'prb_expire', 'insure_expire', 'insure_customer_pay', 'insure_import', 'discount_subdown_vat', 'discount_margin', 'discount_margin_date', 'discount_other', 'discount_other_price', 'discount_all', 'discount_over', 'discount_customer_pay', 'discount_customer_pay_reason', 'discount_tmt', 'tmt_free', 'tmt_free_remark', 'tmt_coupon', 'tmt_coupon_remark', 'tmt_other', 'tmt_other_remark', 'finance_date', 'finance_number', 'com_car', 'com_finance', 'com_insure', 'com_old_car', 'com_equipment', 'com_extra', 'com_extra_remark', 'finance_remain', 'finance_com', 'finance_com_insure', 'finance_face_car', 'finance_face_com', 'scan_01', 'scan_02', 'scan_03', 'scan_04', 'scan_05', 'scan_06', 'scan_07', 'scan_08', 'scan_09', 'scan_10', 'scan_11', 'car_margin_id', 'credit', 'credit_date', 'credit_sending_date', 'dealer', 'tax_date', 'tax_duedate', 'tax_price', 'estimate_finance_date', 'add_date', 'edit_date', 'delete_date', 'check_purchase', 'check_purchase_remark', 'check_purchase_by', 'check_account_booking', 'check_account_booking_date', 'check_account_booking_remark', 'check_account_booking_by', 'check_account_sending', 'check_account_sending_date', 'check_account_sending_remark', 'check_account_sending_by', 'check_account_sending_com', 'stock_status', 'stock_car_status', 'stock_crl_status', 'company_id', 'company_ids', 'booking_place', 'sending_place', 'change_product', 'change_number', 'change_remark', 'booking_cancel_reason_remark', 'buy_reason', 'bond01', 'bond01_relate', 'bond01_relate_remark', 'bond01_remark', 'bond02', 'bond02_relate', 'bond02_relate_remark', 'bond02_remark', 'purchase_date'], 'required'],
            [['booking_car_price', 'booking_cancel_price', 'sending_cancel_price', 'order_price', 'order_reserve_price', 'order_reserve_price01', 'order_reserve_price02', 'order_old_car', 'red_code_price', 'registry_price', 'registry_price1', 'buy_fee', 'buy_payment', 'buy_price', 'buy_down', 'buy_total', 'buy_begin', 'buy_product', 'buy_engine', 'prb_price', 'insure_price', 'insure_budget', 'insure_customer_pay', 'other_product_price', 'other_premium_price', 'discount_price', 'discount_subdown', 'discount_premium', 'discount_insurance', 'discount_product', 'discount', 'discount_subdown_vat', 'discount_subdown_vat_percent', 'discount_margin', 'discount_other_price', 'discount_all', 'discount_over', 'discount_customer_pay', 'discount_tmt', 'tmt_free', 'tmt_coupon', 'tmt_other', 'com_car', 'com_finance', 'com_insure', 'com_old_car', 'com_equipment', 'com_extra', 'finance_com', 'finance_com_insure', 'finance_face_car', 'finance_face_com', 'tax_price'], 'number'],
            [['booking_car_remark', 'booking_remark', 'sending_remark', 'sending_car_remark', 'registry_miss_document', 'registry_remark', 'registry_remark_01', 'registry_remark_02', 'registry_remark_03', 'registry_remark_04', 'registry_remark_05', 'registry_remark_06', 'registry_remark_07', 'registry_remark_08', 'buy_engine_remark', 'tmt_free_remark', 'tmt_coupon_remark', 'tmt_other_remark', 'com_extra_remark', 'check_purchase_remark', 'check_account_booking_remark', 'check_account_sending_remark', 'change_remark'], 'string'],
            [['order_number', 'recieve_number', 'registry_sign', 'insure_type', 'scan_06', 'stock_car_status', 'stock_crl_status'], 'string', 'max' => 50],
            [['booking_number', 'booking_number_nohead', 'sending_number', 'sending_number_nohead', 'order_old_car_number', 'change_number'], 'string', 'max' => 20],
            [['booking_car_gear', 'sending_cancel_number', 'black_code_number', 'registry_recieve_type_01', 'registry_recieve_type_02', 'bond01_relate', 'bond02_relate'], 'string', 'max' => 10],
            [['booking_cancel_number', 'booking_cancel_approve', 'sending_cancel_approve', 'order_location', 'registry_recieve_name_01', 'registry_recieve_name_02', 'other_product', 'finance_number', 'finance_remain', 'scan_01', 'scan_02', 'scan_03', 'scan_04', 'scan_05', 'scan_07', 'scan_08', 'scan_09', 'scan_10', 'scan_11', 'stock_status'], 'string', 'max' => 100],
            [['booking_cancel_reason', 'sending_cancel_reason', 'registry_text_03', 'registry_text_04', 'registry_text_05', 'registry_text_06', 'registry_text_07', 'buy_remark', 'insure_from_detail', 'insure_remark', 'other_premium', 'discount_other', 'discount_customer_pay_reason', 'delete_reason', 'booking_cancel_reason_remark', 'buy_reason', 'bond01_relate_remark', 'bond01_remark', 'bond02_relate_remark', 'bond02_remark'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'order_date' => 'Order Date',
            'order_status' => 'Order Status',
            'order_number' => 'Order Number',
            'recieve_number' => 'Recieve Number',
            'booking_number' => 'Booking Number',
            'booking_customer_id' => 'Booking Customer ID',
            'booking_number_nohead' => 'Booking Number Nohead',
            'booking_date' => 'Booking Date',
            'booking_cashier_date' => 'Booking Cashier Date',
            'booking_cashier_time' => 'Booking Cashier Time',
            'booking_cashier_id' => 'Booking Cashier ID',
            'booking_car_color' => 'Booking Car Color',
            'booking_car_series' => 'Booking Car Series',
            'booking_car_type' => 'Booking Car Type',
            'booking_car_gear' => 'Booking Car Gear',
            'booking_car_price' => 'Booking Car Price',
            'booking_car_remark' => 'Booking Car Remark',
            'booking_sale_id' => 'Booking Sale ID',
            'booking_team_id' => 'Booking Team ID',
            'booking_remark' => 'Booking Remark',
            'booking_cancel_date' => 'Booking Cancel Date',
            'booking_cancel_number' => 'Booking Cancel Number',
            'booking_cancel_reason' => 'Booking Cancel Reason',
            'booking_cancel_price' => 'Booking Cancel Price',
            'booking_cancel_approve' => 'Booking Cancel Approve',
            'booking_mail' => 'Booking Mail',
            'booking_mail_date' => 'Booking Mail Date',
            'booking_thankyou' => 'Booking Thankyou',
            'sending_number' => 'Sending Number',
            'sending_number_nohead' => 'Sending Number Nohead',
            'sending_customer_id' => 'Sending Customer ID',
            'sending_date' => 'Sending Date',
            'sending_sale_id' => 'Sending Sale ID',
            'sending_team_id' => 'Sending Team ID',
            'sending_remark' => 'Sending Remark',
            'sending_car_remark' => 'Sending Car Remark',
            'sending_cancel_date' => 'Sending Cancel Date',
            'sending_cancel_number' => 'Sending Cancel Number',
            'sending_cancel_reason' => 'Sending Cancel Reason',
            'sending_cancel_price' => 'Sending Cancel Price',
            'sending_cancel_approve' => 'Sending Cancel Approve',
            'sending_mail' => 'Sending Mail',
            'sending_mail_date' => 'Sending Mail Date',
            'sending_cashier_date' => 'Sending Cashier Date',
            'sending_cashier_time' => 'Sending Cashier Time',
            'sending_cashier_id' => 'Sending Cashier ID',
            'sending_thankyou' => 'Sending Thankyou',
            'switch_sale' => 'Switch Sale',
            'switch_car' => 'Switch Car',
            'switch_customer' => 'Switch Customer',
            'order_price' => 'Order Price',
            'order_reserve_price' => 'Order Reserve Price',
            'order_reserve_price01' => 'Order Reserve Price01',
            'order_reserve_price02' => 'Order Reserve Price02',
            'order_old_car' => 'Order Old Car',
            'order_old_car_number' => 'Order Old Car Number',
            'order_location' => 'Order Location',
            'stock_car_id' => 'Stock Car ID',
            'stock_red_id' => 'Stock Red ID',
            'red_code_price' => 'Red Code Price',
            'no_red_code' => 'No Red Code',
            'black_code_status' => 'Black Code Status',
            'black_code_number' => 'Black Code Number',
            'black_code_date' => 'Black Code Date',
            'recieve_date' => 'Recieve Date',
            'registry_status' => 'Registry Status',
            'registry_price' => 'Registry Price',
            'registry_price1' => 'Registry Price1',
            'registry_tax_year' => 'Registry Tax Year',
            'registry_tax_year_number' => 'Registry Tax Year Number',
            'registry_send_date' => 'Registry Send Date',
            'registry_miss_document' => 'Registry Miss Document',
            'registry_take_code' => 'Registry Take Code',
            'registry_take_code_date' => 'Registry Take Code Date',
            'registry_take_book' => 'Registry Take Book',
            'registry_take_book_date' => 'Registry Take Book Date',
            'registry_condition_id' => 'Registry Condition ID',
            'registry_remark' => 'Registry Remark',
            'registry_edit_by' => 'Registry Edit By',
            'registry_edit_date' => 'Registry Edit Date',
            'registry_person' => 'Registry Person',
            'registry_date_01' => 'Registry Date 01',
            'registry_date_02' => 'Registry Date 02',
            'registry_date_05' => 'Registry Date 05',
            'registry_date_07' => 'Registry Date 07',
            'registry_text_03' => 'Registry Text 03',
            'registry_text_04' => 'Registry Text 04',
            'registry_text_05' => 'Registry Text 05',
            'registry_text_06' => 'Registry Text 06',
            'registry_text_07' => 'Registry Text 07',
            'registry_remark_01' => 'Registry Remark 01',
            'registry_remark_02' => 'Registry Remark 02',
            'registry_remark_03' => 'Registry Remark 03',
            'registry_remark_04' => 'Registry Remark 04',
            'registry_remark_05' => 'Registry Remark 05',
            'registry_remark_06' => 'Registry Remark 06',
            'registry_remark_07' => 'Registry Remark 07',
            'registry_remark_08' => 'Registry Remark 08',
            'registry_recieve_type_01' => 'Registry Recieve Type 01',
            'registry_recieve_type_02' => 'Registry Recieve Type 02',
            'registry_recieve_name_01' => 'Registry Recieve Name 01',
            'registry_recieve_name_02' => 'Registry Recieve Name 02',
            'registry_sign' => 'Registry Sign',
            'registry_sign_date' => 'Registry Sign Date',
            'buy_type' => 'Buy Type',
            'buy_company' => 'Buy Company',
            'buy_fee' => 'Buy Fee',
            'buy_time' => 'Buy Time',
            'buy_payment' => 'Buy Payment',
            'buy_price' => 'Buy Price',
            'buy_down' => 'Buy Down',
            'buy_total' => 'Buy Total',
            'buy_remark' => 'Buy Remark',
            'buy_begin' => 'Buy Begin',
            'buy_product' => 'Buy Product',
            'buy_engine' => 'Buy Engine',
            'buy_engine_remark' => 'Buy Engine Remark',
            'prb_company' => 'Prb Company',
            'prb_expire' => 'Prb Expire',
            'prb_price' => 'Prb Price',
            'insure_company' => 'Insure Company',
            'insure_expire' => 'Insure Expire',
            'insure_price' => 'Insure Price',
            'insure_type' => 'Insure Type',
            'insure_year' => 'Insure Year',
            'insure_budget' => 'Insure Budget',
            'insure_from' => 'Insure From',
            'insure_from_detail' => 'Insure From Detail',
            'insure_remark' => 'Insure Remark',
            'insure_customer_pay' => 'Insure Customer Pay',
            'insure_import' => 'Insure Import',
            'other_product' => 'Other Product',
            'other_product_qty' => 'Other Product Qty',
            'other_product_price' => 'Other Product Price',
            'other_product_tmt' => 'Other Product Tmt',
            'other_premium' => 'Other Premium',
            'other_premium_qty' => 'Other Premium Qty',
            'other_premium_price' => 'Other Premium Price',
            'other_premium_tmt' => 'Other Premium Tmt',
            'discount_price' => 'Discount Price',
            'discount_subdown' => 'Discount Subdown',
            'discount_premium' => 'Discount Premium',
            'discount_insurance' => 'Discount Insurance',
            'discount_goa' => 'Discount Goa',
            'discount_product' => 'Discount Product',
            'discount' => 'Discount',
            'discount_subdown_vat' => 'Discount Subdown Vat',
            'discount_subdown_vat_percent' => 'Discount Subdown Vat Percent',
            'discount_margin' => 'Discount Margin',
            'discount_margin_date' => 'Discount Margin Date',
            'discount_other' => 'Discount Other',
            'discount_other_price' => 'Discount Other Price',
            'discount_all' => 'Discount All',
            'discount_over' => 'Discount Over',
            'discount_customer_pay' => 'Discount Customer Pay',
            'discount_customer_pay_reason' => 'Discount Customer Pay Reason',
            'discount_tmt' => 'Discount Tmt',
            'tmb_number' => 'Tmb Number',
            'tmt_free' => 'Tmt Free',
            'tmt_free_remark' => 'Tmt Free Remark',
            'tmt_coupon' => 'Tmt Coupon',
            'tmt_coupon_remark' => 'Tmt Coupon Remark',
            'tmt_other' => 'Tmt Other',
            'tmt_other_remark' => 'Tmt Other Remark',
            'finance_date' => 'Finance Date',
            'finance_number' => 'Finance Number',
            'com_car' => 'Com Car',
            'com_finance' => 'Com Finance',
            'com_insure' => 'Com Insure',
            'com_old_car' => 'Com Old Car',
            'com_equipment' => 'Com Equipment',
            'com_extra' => 'Com Extra',
            'com_extra_remark' => 'Com Extra Remark',
            'finance_remain' => 'Finance Remain',
            'finance_com' => 'Finance Com',
            'finance_com_insure' => 'Finance Com Insure',
            'finance_face_car' => 'Finance Face Car',
            'finance_face_com' => 'Finance Face Com',
            'scan_01' => 'Scan 01',
            'scan_02' => 'Scan 02',
            'scan_03' => 'Scan 03',
            'scan_04' => 'Scan 04',
            'scan_05' => 'Scan 05',
            'scan_06' => 'Scan 06',
            'scan_07' => 'Scan 07',
            'scan_08' => 'Scan 08',
            'scan_09' => 'Scan 09',
            'scan_10' => 'Scan 10',
            'scan_11' => 'Scan 11',
            'car_margin_id' => 'Car Margin ID',
            'credit' => 'Credit',
            'credit_date' => 'Credit Date',
            'credit_sending_date' => 'Credit Sending Date',
            'dealer' => 'Dealer',
            'tax_date' => 'Tax Date',
            'tax_duedate' => 'Tax Duedate',
            'tax_price' => 'Tax Price',
            'estimate_finance_date' => 'Estimate Finance Date',
            'add_by' => 'Add By',
            'add_date' => 'Add Date',
            'edit_by' => 'Edit By',
            'edit_date' => 'Edit Date',
            'delete_by' => 'Delete By',
            'delete_date' => 'Delete Date',
            'delete_reason' => 'Delete Reason',
            'delete_status' => 'Delete Status',
            'check_purchase' => 'Check Purchase',
            'check_purchase_remark' => 'Check Purchase Remark',
            'check_purchase_by' => 'Check Purchase By',
            'check_account_booking' => 'Check Account Booking',
            'check_account_booking_date' => 'Check Account Booking Date',
            'check_account_booking_remark' => 'Check Account Booking Remark',
            'check_account_booking_by' => 'Check Account Booking By',
            'check_account_sending' => 'Check Account Sending',
            'check_account_sending_date' => 'Check Account Sending Date',
            'check_account_sending_remark' => 'Check Account Sending Remark',
            'check_account_sending_by' => 'Check Account Sending By',
            'check_account_sending_com' => 'Check Account Sending Com',
            'stock_status' => 'Stock Status',
            'stock_car_status' => 'Stock Car Status',
            'stock_crl_status' => 'Stock Crl Status',
            'company_id' => 'Company ID',
            'company_ids' => 'Company Ids',
            'booking_place' => 'Booking Place',
            'sending_place' => 'Sending Place',
            'change_product' => 'Change Product',
            'change_number' => 'Change Number',
            'change_remark' => 'Change Remark',
            'booking_cancel_reason_remark' => 'Booking Cancel Reason Remark',
            'buy_reason' => 'Buy Reason',
            'bond01' => 'Bond01',
            'bond01_relate' => 'Bond01 Relate',
            'bond01_relate_remark' => 'Bond01 Relate Remark',
            'bond01_remark' => 'Bond01 Remark',
            'bond02' => 'Bond02',
            'bond02_relate' => 'Bond02 Relate',
            'bond02_relate_remark' => 'Bond02 Relate Remark',
            'bond02_remark' => 'Bond02 Remark',
            'purchase_date' => 'Purchase Date',
        ];
    }
}
