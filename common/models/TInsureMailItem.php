<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure_mail_item".
 *
 * @property int $insure_mail_item_id
 * @property string $mail_type
 * @property int $from_company
 * @property int $from_department
 * @property int $from_officer
 * @property string $from_date
 * @property string $from_remark
 * @property string $code
 * @property int $to_department
 * @property int $to_officer
 * @property string $to_check
 * @property string $to_date
 * @property string $to_remark
 */
class TInsureMailItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure_mail_item';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mail_type', 'from_company', 'from_department', 'from_officer', 'from_date'], 'required'],
            [['from_company', 'from_department', 'from_officer', 'to_department', 'to_officer'], 'integer'],
            [['from_date', 'to_date'], 'safe'],
            [['from_remark', 'to_remark'], 'string'],
            [['mail_type', 'code'], 'string', 'max' => 100],
            [['to_check'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insure_mail_item_id' => 'Insure Mail Item ID',
            'mail_type' => 'Mail Type',
            'from_company' => 'From Company',
            'from_department' => 'From Department',
            'from_officer' => 'From Officer',
            'from_date' => 'From Date',
            'from_remark' => 'From Remark',
            'code' => 'Code',
            'to_department' => 'To Department',
            'to_officer' => 'To Officer',
            'to_check' => 'To Check',
            'to_date' => 'To Date',
            'to_remark' => 'To Remark',
        ];
    }
}
