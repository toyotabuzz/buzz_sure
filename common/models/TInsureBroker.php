<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure_broker".
 *
 * @property int $insure_broker_id
 * @property string $title
 * @property string $rank
 */
class TInsureBroker extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure_broker';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['rank'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insure_broker_id' => 'Insure Broker ID',
            'title' => 'Title',
            'rank' => 'Rank',
        ];
    }
}
