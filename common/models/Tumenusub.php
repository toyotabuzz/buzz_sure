<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tumenusub".
 *
 * @property integer $id
 * @property integer $tumenu_id
 * @property integer $submenu_code
 * @property string $submenu_label
 * @property string $submenu_url
 * @property string $submenu_icon
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $isactive
 *
 * @property Tumenu $tumenu
 */
class Tumenusub extends \yii\db\ActiveRecord
{
    public $isactive_text;
    public $tumenu_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tumenusub';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->identity->username,
            ],
        ];
    }  

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['submenu_label'], 'required'],
            [['tumenu_id', 'submenu_code'], 'integer'],
            [['submenu_label', 'submenu_url', 'submenu_icon'], 'string', 'max' => 100],
            [['created_at', 'updated_at', 'isactive'], 'safe'],
            [['created_by', 'updated_by'], 'string', 'max' => 20],
            [['tumenu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tumenu::className(), 'targetAttribute' => ['tumenu_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tumenu_id' => 'เมนูหลัก',
            'submenu_code' => 'รหัส',
            'submenu_label' => 'ชื่อ',
            'submenu_url' => ' URL',
            'submenu_icon' => 'Icon',
            'isactive' => 'สถานะใช้งาน',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function attributeHints()
    {
        return [
            'submenu_url'   => 'ex. /controller/action',
            'submenu_icon'   => 'ex. icon-name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainmenu()
    {
        return $this->hasOne(Tumenu::className(), ['id' => 'tumenu_id']);
    }
     /**
     * Handles owner 'afterFind' event, ensuring attribute typecasting.
     * @param \yii\base\Event $event event instance.
     */
    public function afterFind()
    {
        $this->isactive_text = ($this->isactive == 'Y')?'ใช้งาน':'ไม่ใช้งาน';
        $this->isactive      = ($this->isactive == 'Y')?true:false;
        $this->tumenu_name   = (empty($this->mainmenu))?'':$this->mainmenu->menu_label;

        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->isactive = ($this->isactive)?'Y':'N';

            return true;
        } else {
            return false;
        }
    }
}
