<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_car_color".
 *
 * @property int $car_color_id
 * @property string $title
 * @property string $code
 */
class TCarColor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_car_color';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['title'], 'string', 'max' => 100],
            [['code'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'car_color_id' => 'Car Color ID',
            'title' => 'Title',
            'code' => 'Code',
        ];
    }
}
