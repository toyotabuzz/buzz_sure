<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\base\NotSupportedException;

/**
 * This is the model class for table "tuuser".
 *
 * @property integer $id
 * @property string $code
 * @property string $firstname
 * @property string $lastname
 * @property string $nickname
 * @property string $email
 * @property string $id_card
 * @property string $address
 * @property string $tel
 * @property integer $status
 * @property string $username
 * @property string $password write-only password
 * @property string $salt
 * @property string $department
 * @property string $position
 * @property integer $team
 * @property integer $level
 * @property string $picture
 * @property string $date_add
 * @property string $start_date
 * @property string $end_date
 * @property string $remark
 * @property string $default_program
 * @property integer $company_id
 * @property string $insure_license
 * @property string $insure_editor
 * @property integer $insure_status
 * @property string $phone_ext
 * @property string $emp_code
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class Tuuser  extends ActiveRecord implements IdentityInterface
{
    const STATUS_INACTIVE  = 'N';
    const STATUS_ACTIVE    = 'Y';
    const POS_SALE_MANAGER = 3;
    const POS_SALE_HEAD    = 2;
    const POS_SALE_EMP     = 1;
    // public $sale_team;
    // public $company_name;
    // public $company_title_en;
    // public $department_name;
    // public $position_name;
    // public $fullname;
    // public $textfullname;
    // public $company_type;
    // public $new_password;
    // public $confirm_password;
    // public $your_point_amt;
    // public $extension_no;
    // public $call_extension;
    // public $extension_ip;
    // public $extension;
    // public $fullname_emp;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tuuser';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => empty(Yii::$app->user->identity->id) ? null : Yii::$app->user->identity->id,
            ],
        ];
    } 

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username','emp_no'], 'required'],
            [['firstname', 'lastname','nickname','email'], 'trim'],
            // [['password','new_password', 'confirm_password'], 'required'],
            [['team', 'company_id', 'insure_status'], 'integer'],
            [['created_at', 'updated_at', 'status','date_add','start_date','end_date'], 'safe'],
            [['username', 'password','default_program','insure_editor'], 'string', 'max' => 20],
            [['id_card'], 'string', 'max' => 30],
            [['nickname', 'mobile_no'], 'string', 'max' => 50],
            [['password', 'new_password', 'confirm_password'], 'string', 'max' => 32],
            [['emp_no'], 'string', 'max' => 10],
            [['firstname', 'lastname','nickname','email','tel','salt','department','position','picture','insure_license'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 255],
            [['remark'], 'string'],
            [['username'], 'unique'],
            [['status'], 'default', 'value' => self::STATUS_ACTIVE],
            //[['branch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tucomp::className(), 'targetAttribute' => ['branch_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'code'            => 'Code',
            'firstname'       => 'ชื่อ',
            'lastname'        => 'นามสกุล',
            'nickname'        => 'ชื่อเล่น',
            'email'           => 'อีเมล',
            'id_card'         => 'บัตรประชาชน',
            'address'         => 'ที่อยู่',
            'tel'             => 'เบอร์ต่อ',
            'status'          => 'สถานะ',
            'username'        => 'Username',
            'password'        => 'รหัสผ่านใหม่',
            'salt'            => 'Salt',
            'department'      => 'หน่วยงาน',
            'position'        => 'ตำแหน่ง',
            'team'            => 'ทีม',
            'level'           => 'ระดับ',
            'picture'         => 'รูปภาพ',
            'date_add'        => 'วันที่บันทึก',
            'start_date'      => 'Start Date',
            'end_date'        => 'End Date',
            'remark'          => 'หมายเหตุ',
            'default_program' => 'Default Program',
            'company_id'      => 'Company ID',
            'insure_license'  => 'Insure License',
            'insure_editor'   => 'Insure Editor',
            'insure_status'   => 'Insure Status',
            'phone_ext'       => 'Phone Ext',
            'emp_code'        => 'Employee No',
            'created_at'      => 'วันที่บันทึก',
            'created_by'      => 'ผู้บันทึก',
            'updated_at'      => 'วันที่แก้ไข',
            'updated_by'      => 'ผู้แก้ไข',
            
        ];
    }

    // public function getCompany()
    // {
    //     return $this->hasOne(Tucomp::className(), ['id' => 'branch_id']);
    // }

    // public function getDepartment()
    // {
    //     return $this->hasOne(Tudep::className(), ['id' => 'department_id']);
    // }

    // public function getPosition()
    // {
    //     return $this->hasOne(Tuposition::className(), ['id' => 'position_id']);
    // }

    public function getUsercreate()
    {
        return $this->hasOne(Tuuser::className(), ['id' => 'created_by']);
    }

    public function getUserupdate()
    {
        return $this->hasOne(Tuuser::className(), ['id' => 'updated_by']);
    }

    // public function getTtextension()
    // {
    //     return $this->hasOne(Ttextension::className(), ['id' => 'call_extension_id']);
    // }

    // public function getUserRole()
    // {
    //     $arrRole = [];
    //     $model = TuuserRole::find()->select(['role_id'])->where(['user_id'=>$this->id])->andWhere(['isactive'=>'Y'])->asArray()->all();
    //     foreach ($model as $val) {
    //         array_push($arrRole, $val['role_id']);
    //     }
    //     return $arrRole;
    //     // return $this->hasMany(TuuserRole::className(), ['user_id' => 'id']);
    // }

    // public function afterFind()
    // {
    //     $this->company_name     = (empty($this->company->short_title))?'':$this->company->short_title;
    //     $this->company_title_en = (empty($this->company->short_title_en))?'':$this->company->short_title_en;
    //     $this->department_name  = (empty($this->department->dep_name))?'':$this->department->dep_name;
    //     $this->position_name    = (empty($this->position->position_name))?'':$this->position->position_name;
    //     $this->fullname         = $this->firstname.' '.$this->lastname;
    //     $this->textfullname     = $this->firstname.' '.$this->lastname.'('.$this->nickname.')';
    //     $this->fullname_emp     = $this->emp_no.' '.$this->firstname.' '.$this->lastname.'('.$this->nickname.')';
    //     $this->company_type     = (empty($this->company->type_comp))?'':$this->company->type_comp;
    //     $this->extension_no     = (empty($this->ttextension->extension_no))?'':$this->ttextension->extension_no;
    //     $this->extension_ip     = (empty($this->ttextension->extension_ip_id))?'': Yii::$app->Utilities->getDescTuref('TELEPHONY', 'IP', $this->ttextension->extension_ip_id);
    //     $this->extension     = $this->extension_no.' ('.$this->extension_ip.')';

    //     $this->status = ($this->status == 'Y');

    //     parent::afterFind();
    // }

    // public function beforeSave($insert)
    // {
    //     if (parent::beforeSave($insert)) {
            
    //         if ($this->isNewRecord) {
    //             if (!empty($this->birth_date)){
    //                 $arrDate        = explode('-', $this->birth_date);
    //                 $strDate        = $arrDate[2].$arrDate[1].$arrDate[0];
    //                 $this->password = md5($strDate);
    //             } else {
    //                  $this->password = md5('password');
    //             }
    //         } else {
    //             if (trim($this->password) == '') {
    //                 if (!empty($this->birth_date)){
    //                     $arrDate        = explode('-', $this->birth_date);
    //                     $strDate        = $arrDate[2].$arrDate[1].$arrDate[0];
    //                     $this->password = md5($strDate);
    //                 } else {
    //                      $this->password = md5('password');
    //                 }
    //             }
    //         }

    //         $this->status = ($this->status)?'Y':'N';
    //         //CHECK FOR DELETE TEAM SALE IF STATUS = N
    //         if ($this->status == 'N' && $this->department_id == 4) {
    //             $modelTeamSale = Tusaleteam::find()->where(['sale_id'=>$this->id])->one();
    //             if (!empty($modelTeamSale)) {
    //                 $modelTeamSale->delete();
    //             }
    //         }

    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return ($password == $this->password);
        // return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
