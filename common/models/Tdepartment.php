<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "t_department".
 *
 * @property integer $department_id
 * @property string $title
 * @property string $code
 */
class Tdepartment extends \yii\db\ActiveRecord
{
    public $status_second_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_department';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->identity->id,
            ],
        ];
    }  
     */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'],'required'],
            [['title'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'department_id' => 'ID',
            'title'         => 'ชื่อหน่วยงาน',
            'code'          => 'Code'
        ];
    }

    

    /**
    **
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->attribute       = $this->attribute;            
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        $this->attribute       = $this->attribute;        
    }
    *
    */
}
