<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tumenusubin".
 *
 * @property integer $id
 * @property integer $tumenusub_id
 * @property integer $subin_code
 * @property string $subin_label
 * @property string $subin_url
 * @property string $subin_icon
 * @property string $isactive
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property Tumenusub $tumenusub
 */
class Tumenusubin extends \yii\db\ActiveRecord
{
    public $tumenu_id; 
    public $isactive_text;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tumenusubin';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->identity->username,
            ],
        ];
    }  

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tumenusub_id', 'subin_label'], 'required'],
            [['tumenusub_id', 'subin_code'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['subin_label', 'subin_url', 'subin_icon'], 'string', 'max' => 100],
            [['isactive'], 'string', 'max' => 1],
            [['created_by', 'updated_by'], 'string', 'max' => 20],
            [['tumenusub_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tumenusub::className(), 'targetAttribute' => ['tumenusub_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tumenu_id' => 'เมนูหลัก',
            'tumenusub_id' => 'เมนูย่อย',
            'subin_code' => 'รหัส',
            'subin_label' => 'ชื่อเมนู',
            'subin_url' => 'Url',
            'subin_icon' => 'Icon',
            'isactive' => 'สถานะใช้งาน',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function attributeHints()
    {
        return [
            'subin_url'   => 'ex. /controller/action',
            'subin_icon'   => 'ex. icon-name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTumenusub()
    {
        return $this->hasOne(Tumenusub::className(), ['id' => 'tumenusub_id']);
    }
    
    public function afterFind()
    {
        $this->isactive_text = ($this->isactive == 'Y')?'ใช้งาน':'ไม่ใช้งาน';
        $this->isactive      = ($this->isactive == 'Y')?true:false;
        $this->tumenu_id     = $this->tumenusub->tumenu_id;
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->isactive = ($this->isactive)?'Y':'N';

            return true;
        } else {
            return false;
        }
    }
}
