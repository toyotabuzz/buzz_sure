<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TInsureForm;

/**
 * TInsureFormSearch represents the model behind the search form of `common\models\TInsureForm`.
 */
class TInsureFormSearch extends turole
{
    public $h_keyword_customer;
    public $h_keyword_car;
    public $h_keyword_insure;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['form_date', 'remark', 't11', 't12', 't13', 'sup_approve', 'sup_date', 'sup_id', 'sup_remark', 'admin_approve', 'admin_date', 'admin_id', 'admin_remark', 'admin_code', 'admin_code_date','form_date', 'sup_date', 'admin_date', 'admin_code_date','insure_id', 'sup_id', 'admin_id', 'integer','remark', 't11', 't12', 't13', 'sup_remark', 'admin_remark','frm_type', 'code', 'sup_approve', 'admin_approve','title', 't01', 't02', 't03', 't04', 't05', 't06', 't07', 't08', 't09', 't10', 't14', 't15', 't16', 't17', 't18', 't19', 't20', 't21', 't22', 't23', 't24', 't25', 't26', 't27', 't28', 't29', 't30', 't31', 't32', 't33', 't34', 't35', 't36', 't37', 't38', 't39', 't40', 't41', 't42', 't43', 't44', 't45', 't46', 't47', 't48', 't49', 't50', 't51', 't52', 't53', 't54', 't55', 't56', 't57', 't58', 't59', 't60', 't61', 't62', 't63', 't64', 't65', 't66', 't67', 't68', 't69', 't70', 't71', 't72', 't73', 't74', 't75', 't76', 't77', 't78', 't79', 't80', 't81', 't82', 't83', 't84', 't85', 't86', 't87', 't88', 't89', 't90', 't91', 't92', 't93', 't94', 't95', 't96', 't97', 't98', 't99', 't00','admin_code','h_keyword_customer','h_keyword_car','h_keyword_insure'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TInsureForm::find()
        ->joinWith('tInsure');

        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if(!empty($this->h_keyword_customer)){
            $query->joinWith('tInsure.tInsureCar.tInsureCustomer');
        }
        if(!empty($this->h_keyword_car)){
            $query->joinWith('tInsure.tInsureCar');
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->where(['t_insure_form.sup_approve'   => 'อนุมัติ'])
        ->andwhere(['t_insure_form.admin_approve' => 'อนุมัติ'])
        ->andwhere(['t_insure.k_stock_id' => 0])
        ->andwhere(['t_insure_form.frm_type' => '0901'])
        ->andwhere(['<>','t_insure.insure_date', '0000-00-00']);

        $query->andFilterWhere([
            'OR',
            ['like', 't_insure_customer01.firstname', $this->h_keyword_customer],
            ['like', 't_insure_customer01.lastname', $this->h_keyword_customer],
            ['like', 't_insure_customer01.id_card', $this->h_keyword_customer],
            ['like', 't_insure_customer01.home_tel', $this->h_keyword_customer],
            ['like', 't_insure_customer01.mobile', $this->h_keyword_customer],
            ['like', 't_insure_customer01.office_tel', $this->h_keyword_customer]
        ]);
        
        $query->andFilterWhere([
            'OR',
            ['like', 't_insure_car.code', $this->h_keyword_car],
            ['like', 't_insure_car.car_number', $this->h_keyword_car],
            ['like', 't_insure_car.engine_number', $this->h_keyword_car]
        ]);

        $query->andFilterWhere([
            'OR',
            ['like', 't_insure.p_number', $this->h_keyword_insure],
            ['like', 't_insure.p_rnum', $this->h_keyword_insure]
        ]);

        $query->orderBy(['t_insure.k_start_date' => SORT_ASC]);
        //echo $query->createCommand()->getRawSql();exit;
        return $dataProvider;
    }
}
