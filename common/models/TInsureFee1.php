<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure_fee1".
 *
 * @property int $insure_fee_id
 * @property string $title
 * @property string $code
 * @property string $use_drive
 * @property string $use_type
 * @property float|null $insure_fee
 * @property float|null $insure_vat
 * @property float|null $argon
 * @property float|null $total
 */
class TInsureFee1 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure_fee1';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insure_fee', 'insure_vat', 'argon', 'total'], 'number'],
            [['title'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 10],
            [['use_drive'], 'string', 'max' => 50],
            [['use_type'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insure_fee_id' => 'Insure Fee ID',
            'title' => 'Title',
            'code' => 'Code',
            'use_drive' => 'Use Drive',
            'use_type' => 'Use Type',
            'insure_fee' => 'Insure Fee',
            'insure_vat' => 'Insure Vat',
            'argon' => 'Argon',
            'total' => 'Total',
        ];
    }
}
