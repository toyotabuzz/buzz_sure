<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tumenusubin;

/**
 * TumenusubinSerach represents the model behind the search form about `common\models\Tumenusubin`.
 */
class TumenusubinSerach extends Tumenusubin
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tumenusub_id', 'subin_code'], 'integer'],
            [['subin_label', 'subin_url', 'subin_icon', 'isactive', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tumenusubin::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tumenusub_id' => $this->tumenusub_id,
            'subin_code' => $this->subin_code,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'subin_label', $this->subin_label])
            ->andFilterWhere(['like', 'subin_url', $this->subin_url])
            ->andFilterWhere(['like', 'subin_icon', $this->subin_icon])
            ->andFilterWhere(['like', 'isactive', $this->isactive])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
