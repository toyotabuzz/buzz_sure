<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "tuauth_submenu".
 *
 * @property integer $department_id
 * @property integer $position_id
 * @property integer $menu_id
 */
class TuauthSubMenu extends \yii\db\ActiveRecord
{
    public $listmenu;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tuauth_submenu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comp_type_id', 'department_id', 'role_id', 'menu_id'], 'required'],
            [['comp_type_id', 'department_id', 'role_id', 'position_id', 'menu_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'comp_type_id'  => 'บริษัท',
            'department_id' => 'หน่วยงาน',
            'position_id'   => 'ตำแหน่ง',
            'role_id'       => 'หน้าที่',
            'menu_id'       => 'เมนูหลัก',
        ];
    }
    
}
