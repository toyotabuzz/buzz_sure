<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_order_extra".
 *
 * @property int $order_extra_id
 * @property int $order_id
 * @property string $extra_date
 * @property int $extra_type
 * @property int $pr_id
 * @property string $name
 * @property string $address
 * @property string $booking_number
 * @property string $sending_number
 * @property string $sending_number_nohead
 * @property float $total_price
 * @property int $add_by
 * @property string $add_date
 * @property int $edit_by
 * @property string $edit_date
 * @property int $delete_by
 * @property string $delete_date
 * @property int $delete_status
 * @property string $delete_reason
 * @property int $company_id
 * @property int $num_time
 */
class TOrderExtra extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_order_extra';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'extra_type', 'pr_id', 'add_by', 'edit_by', 'delete_by', 'delete_status', 'company_id', 'num_time'], 'integer'],
            [['extra_date', 'pr_id', 'name', 'address', 'total_price', 'add_date', 'edit_date', 'delete_date', 'delete_reason', 'company_id', 'num_time'], 'required'],
            [['extra_date', 'add_date', 'edit_date', 'delete_date'], 'safe'],
            [['address', 'delete_reason'], 'string'],
            [['total_price'], 'number'],
            [['name'], 'string', 'max' => 100],
            [['booking_number', 'sending_number', 'sending_number_nohead'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_extra_id' => 'Order Extra ID',
            'order_id' => 'Order ID',
            'extra_date' => 'Extra Date',
            'extra_type' => 'Extra Type',
            'pr_id' => 'Pr ID',
            'name' => 'Name',
            'address' => 'Address',
            'booking_number' => 'Booking Number',
            'sending_number' => 'Sending Number',
            'sending_number_nohead' => 'Sending Number Nohead',
            'total_price' => 'Total Price',
            'add_by' => 'Add By',
            'add_date' => 'Add Date',
            'edit_by' => 'Edit By',
            'edit_date' => 'Edit Date',
            'delete_by' => 'Delete By',
            'delete_date' => 'Delete Date',
            'delete_status' => 'Delete Status',
            'delete_reason' => 'Delete Reason',
            'company_id' => 'Company ID',
            'num_time' => 'Num Time',
        ];
    }
}
