<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure_item_detail".
 *
 * @property int $order_price_id
 * @property int $order_id
 * @property int $order_extra_id
 * @property int $order_payment_id
 * @property int $payment_subject_id
 * @property int $qty
 * @property float $price
 * @property string $remark
 * @property int $status
 * @property int $vat
 * @property int $payin
 * @property float $price_acc
 */
class TInsureItemDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure_item_detail';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'order_extra_id', 'order_payment_id', 'payment_subject_id', 'qty', 'status', 'vat', 'payin'], 'integer'],
            [['price', 'price_acc'], 'number'],
            [['remark'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_price_id' => 'Order Price ID',
            'order_id' => 'Order ID',
            'order_extra_id' => 'Order Extra ID',
            'order_payment_id' => 'Order Payment ID',
            'payment_subject_id' => 'Payment Subject ID',
            'qty' => 'Qty',
            'price' => 'Price',
            'remark' => 'Remark',
            'status' => 'Status',
            'vat' => 'Vat',
            'payin' => 'Payin',
            'price_acc' => 'Price Acc',
        ];
    }
    public function getTPaymentSubject()
    {
        return $this->hasOne(TPaymentSubject::className(), ['payment_subject_id' => 'payment_subject_id']);
    }
}
