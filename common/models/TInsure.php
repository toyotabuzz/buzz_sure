<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure".
 *
 * @property int $insure_id
 * @property int $sale_id
 * @property int $team_id
 * @property int $car_id
 * @property int $customer_id
 * @property int $company_id
 * @property int $quotation_id
 * @property int $quotation_item_id
 * @property string $year_extend
 * @property string|null $date_protect
 * @property int $k_check
 * @property int $k_broker_id
 * @property int $k_prb_id
 * @property int $k_stock_id
 * @property string $k_number
 * @property string|null $k_start_date
 * @property string|null $k_end_date
 * @property int $k_frees
 * @property int $p_check
 * @property int $p_broker_id
 * @property int $p_prb_id
 * @property int $p_stock_id
 * @property string $p_number
 * @property string|null $p_start_date
 * @property string|null $p_end_date
 * @property int $p_frees
 * @property float|null $k_num01
 * @property float|null $k_num02
 * @property float|null $k_num03
 * @property float|null $k_num04
 * @property float|null $k_num05
 * @property float|null $k_num06
 * @property float|null $k_num07
 * @property float|null $k_num08
 * @property float|null $k_num09
 * @property float|null $k_num10
 * @property float|null $k_num11
 * @property float|null $k_num12
 * @property float|null $k_num13
 * @property string $k_niti
 * @property int $k_dis_type_01
 * @property int $k_dis_type_02
 * @property int $k_dis_type_03
 * @property float $k_dis_con_01
 * @property string $k_dis_con_02
 * @property string $k_free
 * @property string $k_type
 * @property string $k_type_remark
 * @property string $k_fix
 * @property int $p_car_type
 * @property float $p_prb_price
 * @property string $p_year
 * @property string|null $p_call_date
 * @property string|null $p_get_date
 * @property string $p_call_number
 * @property resource $p_remark
 * @property float $p_insure_vat
 * @property float $p_argon
 * @property float $p_total
 * @property int $pay_number
 * @property string $pay_type
 * @property string $status
 * @property int $k_send
 * @property string|null $k_send_date
 * @property string $k_send_code
 * @property int $k_send_officer_id
 * @property int $p_send
 * @property string|null $p_send_date
 * @property string $p_send_code
 * @property int $p_send_officer_id
 * @property int $bill_send
 * @property string $bill_send_date
 * @property string $bill_send_code
 * @property int $bill_send_officer_id
 * @property int $pasee_send
 * @property string $pasee_send_date
 * @property string $pasee_send_code
 * @property int $pasee_send_officer_id
 * @property int $saluk_send
 * @property string $saluk_send_date
 * @property string $saluk_send_code
 * @property int $saluk_send_officer_id
 * @property int $prepare
 * @property string|null $prepare_date
 * @property string $prepare_no
 * @property int $prepare_by
 * @property string $direct
 * @property int $p_acc_check
 * @property float $p_acc_com
 * @property string $p_acc_remark
 * @property int $p_acc_id
 * @property int $k_acc_check
 * @property float $k_acc_com
 * @property string $k_acc_remark
 * @property int $k_acc_id
 * @property string $p_rnum
 * @property string $p_rnum_date
 * @property string $insure_date
 * @property string $k_new
 * @property int $officer_id
 * @property string $officer_date
 * @property string $k_status
 * @property int $k_car_type
 * @property float $p_vat
 * @property int $request_cancel
 * @property int $request_edit
 * @property string $doc01
 * @property string $doc02
 * @property string $doc03
 * @property string $doc04
 * @property string $doc05
 * @property string $doc06
 * @property string $doc07
 * @property string $doc08
 * @property string $doc09
 * @property string $doc10
 * @property string $k_ins_date
 * @property string $k_acc_date
 * @property string $k_sale_date
 * @property string $k_ins_remark
 * @property string $p_status
 * @property int $k_ins_officer
 * @property int $k_acc_officer
 * @property int $k_sale_officer
 * @property string $k_kom_status
 * @property string $k_cancel_date
 * @property float $k_cancel_price
 * @property string $k_cancel_remark
 * @property string $product_type
 * @property string $ws_vendor_k
 * @property string $ws_vendor_p
 * @property string $ws_cus_k
 * @property string $ws_cus_p
 * @property int $acc_company_id
 * @property int $p_rnum_check
 */
class TInsure extends \yii\db\ActiveRecord
{
    public $policy_status_id;
    public $policy_status_second_id;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sale_id', 'team_id', 'car_id', 'customer_id', 'company_id', 'quotation_id', 'quotation_item_id', 'k_check', 'k_broker_id', 'k_prb_id', 'k_stock_id', 'k_frees', 'p_check', 'p_broker_id', 'p_prb_id', 'p_stock_id', 'p_frees', 'k_dis_type_01', 'k_dis_type_02', 'k_dis_type_03', 'p_car_type', 'pay_number', 'k_send', 'k_send_officer_id', 'p_send', 'p_send_officer_id', 'bill_send', 'bill_send_officer_id', 'pasee_send', 'pasee_send_officer_id', 'saluk_send', 'saluk_send_officer_id', 'prepare', 'prepare_by', 'p_acc_check', 'p_acc_id', 'k_acc_check', 'k_acc_id', 'officer_id', 'k_car_type', 'request_cancel', 'request_edit', 'k_ins_officer', 'k_acc_officer', 'k_sale_officer', 'acc_company_id', 'p_rnum_check'], 'integer'],
            [['team_id', 'quotation_item_id', 'k_check', 'k_frees', 'p_check', 'p_frees', 'p_insure_vat', 'p_argon', 'p_total', 'k_send_officer_id', 'p_send_officer_id', 'bill_send', 'bill_send_date', 'bill_send_officer_id', 'pasee_send', 'pasee_send_date', 'pasee_send_officer_id', 'saluk_send', 'saluk_send_date', 'saluk_send_officer_id', 'p_acc_check', 'p_acc_com', 'p_acc_id', 'k_acc_check', 'k_acc_com', 'k_acc_id', 'p_rnum', 'p_rnum_date', 'insure_date', 'officer_id', 'officer_date', 'k_status', 'k_car_type', 'p_vat', 'request_cancel', 'request_edit', 'k_ins_date', 'k_acc_date', 'k_sale_date', 'k_ins_officer', 'k_acc_officer', 'k_sale_officer', 'k_cancel_date', 'k_cancel_price', 'acc_company_id', 'p_rnum_check'], 'required'],
            [['date_protect', 'k_start_date', 'k_end_date', 'p_start_date', 'p_end_date', 'p_call_date', 'p_get_date', 'k_send_date', 'p_send_date', 'bill_send_date', 'pasee_send_date', 'saluk_send_date', 'prepare_date', 'p_rnum_date', 'insure_date', 'officer_date', 'k_ins_date', 'k_acc_date', 'k_sale_date', 'k_cancel_date'], 'safe'],
            [['k_num01', 'k_num02', 'k_num03', 'k_num04', 'k_num05', 'k_num06', 'k_num07', 'k_num08', 'k_num09', 'k_num10', 'k_num11', 'k_num12', 'k_num13', 'k_dis_con_01', 'p_prb_price', 'p_insure_vat', 'p_argon', 'p_total', 'p_acc_com', 'k_acc_com', 'p_vat', 'k_cancel_price','policy_status_id','policy_status_second_id'], 'number'],
            [['year_extend'], 'string', 'max' => 4],
            [['k_number', 'p_number', 'k_niti', 'prepare_no', 'doc01', 'doc02', 'doc03', 'doc04', 'k_kom_status'], 'string', 'max' => 50],
            [['k_dis_con_02', 'k_free', 'k_type_remark', 'k_status', 'doc05', 'doc06', 'doc07', 'doc08', 'doc09', 'doc10'], 'string', 'max' => 100],
            [['k_type', 'k_fix', 'p_year', 'p_call_number', 'pay_type', 'status', 'p_rnum', 'k_new'], 'string', 'max' => 20],
            [['p_remark'], 'string', 'max' => 150],
            [['k_send_code', 'p_send_code', 'bill_send_code', 'pasee_send_code', 'saluk_send_code', 'p_status', 'product_type'], 'string', 'max' => 30],
            [['direct'], 'string', 'max' => 10],
            [['p_acc_remark', 'k_acc_remark', 'k_cancel_remark'], 'string', 'max' => 255],
            [['k_ins_remark'], 'string', 'max' => 200],
            [['ws_vendor_k', 'ws_vendor_p', 'ws_cus_k', 'ws_cus_p'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'insure_id'               => 'Insure ID',
            'sale_id'                 => 'Sale ID',
            'team_id'                 => 'Team ID',
            'car_id'                  => 'Car ID',
            'customer_id'             => 'Customer ID',
            'company_id'              => 'Company ID',
            'quotation_id'            => 'Quotation ID',
            'quotation_item_id'       => 'Quotation Item ID',
            'year_extend'             => 'Year Extend',
            'date_protect'            => 'Date Protect',
            'k_check'                 => 'K Check',
            'k_broker_id'             => 'K Broker ID',
            'k_prb_id'                => 'K Prb ID',
            'k_stock_id'              => 'K Stock ID',
            'k_number'                => 'K Number',
            'k_start_date'            => 'K Start Date',
            'k_end_date'              => 'K End Date',
            'k_frees'                 => 'K Frees',
            'p_check'                 => 'P Check',
            'p_broker_id'             => 'P Broker ID',
            'p_prb_id'                => 'P Prb ID',
            'p_stock_id'              => 'P Stock ID',
            'p_number'                => 'P Number',
            'p_start_date'            => 'P Start Date',
            'p_end_date'              => 'P End Date',
            'p_frees'                 => 'P Frees',
            'k_num01'                 => 'K Num01',
            'k_num02'                 => 'K Num02',
            'k_num03'                 => 'K Num03',
            'k_num04'                 => 'K Num04',
            'k_num05'                 => 'K Num05',
            'k_num06'                 => 'K Num06',
            'k_num07'                 => 'K Num07',
            'k_num08'                 => 'K Num08',
            'k_num09'                 => 'K Num09',
            'k_num10'                 => 'K Num10',
            'k_num11'                 => 'K Num11',
            'k_num12'                 => 'K Num12',
            'k_num13'                 => 'K Num13',
            'k_niti'                  => 'K Niti',
            'k_dis_type_01'           => 'K Dis Type 01',
            'k_dis_type_02'           => 'K Dis Type 02',
            'k_dis_type_03'           => 'K Dis Type 03',
            'k_dis_con_01'            => 'K Dis Con 01',
            'k_dis_con_02'            => 'K Dis Con 02',
            'k_free'                  => 'K Free',
            'k_type'                  => 'K Type',
            'k_type_remark'           => 'K Type Remark',
            'k_fix'                   => 'K Fix',
            'p_car_type'              => 'P Car Type',
            'p_prb_price'             => 'P Prb Price',
            'p_year'                  => 'P Year',
            'p_call_date'             => 'P Call Date',
            'p_get_date'              => 'P Get Date',
            'p_call_number'           => 'P Call Number',
            'p_remark'                => 'P Remark',
            'p_insure_vat'            => 'P Insure Vat',
            'p_argon'                 => 'P Argon',
            'p_total'                 => 'P Total',
            'pay_number'              => 'Pay Number',
            'pay_type'                => 'Pay Type',
            'status'                  => 'Status',
            'k_send'                  => 'K Send',
            'k_send_date'             => 'K Send Date',
            'k_send_code'             => 'K Send Code',
            'k_send_officer_id'       => 'K Send Officer ID',
            'p_send'                  => 'P Send',
            'p_send_date'             => 'P Send Date',
            'p_send_code'             => 'P Send Code',
            'p_send_officer_id'       => 'P Send Officer ID',
            'bill_send'               => 'Bill Send',
            'bill_send_date'          => 'Bill Send Date',
            'bill_send_code'          => 'Bill Send Code',
            'bill_send_officer_id'    => 'Bill Send Officer ID',
            'pasee_send'              => 'Pasee Send',
            'pasee_send_date'         => 'Pasee Send Date',
            'pasee_send_code'         => 'Pasee Send Code',
            'pasee_send_officer_id'   => 'Pasee Send Officer ID',
            'saluk_send'              => 'Saluk Send',
            'saluk_send_date'         => 'Saluk Send Date',
            'saluk_send_code'         => 'Saluk Send Code',
            'saluk_send_officer_id'   => 'Saluk Send Officer ID',
            'prepare'                 => 'Prepare',
            'prepare_date'            => 'Prepare Date',
            'prepare_no'              => 'Prepare No',
            'prepare_by'              => 'Prepare By',
            'direct'                  => 'Direct',
            'p_acc_check'             => 'P Acc Check',
            'p_acc_com'               => 'P Acc Com',
            'p_acc_remark'            => 'P Acc Remark',
            'p_acc_id'                => 'P Acc ID',
            'k_acc_check'             => 'K Acc Check',
            'k_acc_com'               => 'K Acc Com',
            'k_acc_remark'            => 'K Acc Remark',
            'k_acc_id'                => 'K Acc ID',
            'p_rnum'                  => 'P Rnum',
            'p_rnum_date'             => 'P Rnum Date',
            'insure_date'             => 'Insure Date',
            'k_new'                   => 'K New',
            'officer_id'              => 'Officer ID',
            'officer_date'            => 'Officer Date',
            'k_status'                => 'K Status',
            'k_car_type'              => 'K Car Type',
            'p_vat'                   => 'P Vat',
            'request_cancel'          => 'Request Cancel',
            'request_edit'            => 'Request Edit',
            'doc01'                   => 'Doc01',
            'doc02'                   => 'Doc02',
            'doc03'                   => 'Doc03',
            'doc04'                   => 'Doc04',
            'doc05'                   => 'Doc05',
            'doc06'                   => 'Doc06',
            'doc07'                   => 'Doc07',
            'doc08'                   => 'Doc08',
            'doc09'                   => 'Doc09',
            'doc10'                   => 'Doc10',
            'k_ins_date'              => 'K Ins Date',
            'k_acc_date'              => 'K Acc Date',
            'k_sale_date'             => 'K Sale Date',
            'k_ins_remark'            => 'K Ins Remark',
            'p_status'                => 'P Status',
            'k_ins_officer'           => 'K Ins Officer',
            'k_acc_officer'           => 'K Acc Officer',
            'k_sale_officer'          => 'K Sale Officer',
            'k_kom_status'            => 'K Kom Status',
            'k_cancel_date'           => 'K Cancel Date',
            'k_cancel_price'          => 'K Cancel Price',
            'k_cancel_remark'         => 'K Cancel Remark',
            'product_type'            => 'Product Type',
            'ws_vendor_k'             => 'Ws Vendor K',
            'ws_vendor_p'             => 'Ws Vendor P',
            'ws_cus_k'                => 'Ws Cus K',
            'ws_cus_p'                => 'Ws Cus P',
            'acc_company_id'          => 'Acc Company ID',
            'p_rnum_check'            => 'P Rnum Check',
            'policy_status_id'        => 'สถานะ',
            'policy_status_second_id' => 'เหตุผล',  
        ];
    }
    public function getTInsureCar()
    {
        return $this->hasOne(TInsureCar::className(), ['car_id' => 'car_id']);
    }
    public function getSale()
    {
        return $this->hasOne(Tmember::className(), ['member_id' => 'sale_id']);
    }
    public function getTInsureCompanyP()
    {
        return $this->hasOne(TInsureCompany::className(), ['insure_company_id' => 'p_prb_id']);
    }
    public function getTInsureCompanyK()
    {
        return $this->hasOne(TInsureCompany::className(), ['insure_company_id' => 'k_prb_id']);
    }
    public function getTInsureBrokerK()
    {
        return $this->hasOne(TInsureBroker::className(), ['insure_broker_id' => 'k_broker_id']);
    }
    public function getTInsureBrokerP()
    {
        return $this->hasOne(TInsureBroker::className(), ['insure_broker_id' => 'p_broker_id']);
    }
    public function getTInsureFee1K()
    {
        return $this->hasOne(TInsureFee1::className(), ['insure_fee_id' => 'k_car_type']);
    }
    public function getTInsureFeeP()
    {
        return $this->hasOne(TInsureFee::className(), ['insure_fee_id' => 'p_car_type']);
    }
    public function getTInsureType()
    {
        return $this->hasOne(TInsureType::className(), ['insure_type_id' => 'k_type']);
    }
    public function getPolicyStockCode()
    {
        return $this->hasOne(JmpolicyStockCode::className(), ['insure_id' => 'insure_id']);
    }
}
