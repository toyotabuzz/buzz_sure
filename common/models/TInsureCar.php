<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_insure_car".
 *
 * @property int $car_id
 * @property int $insure_id
 * @property int $sale_id
 * @property string $team
 * @property int $team_id
 * @property string $status
 * @property string $source
 * @property string $car_type
 * @property int $insure_customer_id
 * @property int $customer_id
 * @property string $code
 * @property int $car_model_id
 * @property int $car_series_id
 * @property string $color
 * @property float $price
 * @property string $car_number
 * @property string $engine_number
 * @property string $register_year
 * @property string|null $date_verify
 * @property string $date_verify_prb
 * @property string $suggest_from
 * @property string|null $call_date
 * @property string $call_remark
 * @property string|null $date_add
 * @property int $order_id
 * @property int $junk
 * @property int $company_id
 * @property string|null $mail_date
 * @property string $operate
 * @property string $operate_prb
 * @property int $buy_type
 * @property int $buy_company
 * @property string|null $registry_date
 * @property float $registry_tax_year
 * @property string $registry_tax_year_number
 * @property string|null $get_car_date
 * @property int $insure_company_id
 * @property string $niti
 * @property int $info_type
 * @property int $import
 * @property int $checkc
 * @property string $province_code
 * @property int $insure_acard_id
 * @property int $recent_insure_id
 * @property string $import_date
 * @property int $lock_sale
 * @property string $admin_share
 * @property string $sup_share
 * @property int $fix_sale
 * @property string $emotion
 * @property string $duplicated
 * @property int $first_company_id
 * @property int $first_team_id
 * @property int $first_sale_id
 * @property int $temp_sale_id
 * @property resource $temp_remark
 * @property string $approve
 * @property int $approve_by
 * @property string $approve_date
 * @property int $edits_by
 * @property string $edits_date
 * @property string $inform_status
 * @property string $inform_date
 * @property int $inform_by
 * @property int $inform_sale
 * @property string $cus_code
 */
class TInsureCar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_insure_car';
    }
    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insure_id', 'sale_id', 'team_id', 'insure_customer_id', 'customer_id', 'car_model_id', 'car_series_id', 'order_id', 'junk', 'company_id', 'buy_type', 'buy_company', 'insure_company_id', 'info_type', 'import', 'checkc', 'insure_acard_id', 'recent_insure_id', 'lock_sale', 'fix_sale', 'first_company_id', 'first_team_id', 'first_sale_id', 'temp_sale_id', 'approve_by', 'edits_by', 'inform_by', 'inform_sale'], 'integer'],
            [['team', 'team_id', 'insure_customer_id', 'date_verify_prb', 'operate_prb', 'import', 'province_code', 'insure_acard_id', 'recent_insure_id', 'import_date', 'lock_sale', 'admin_share', 'sup_share', 'fix_sale', 'emotion', 'duplicated', 'first_company_id', 'first_team_id', 'first_sale_id', 'temp_sale_id', 'temp_remark', 'approve', 'approve_by', 'approve_date', 'edits_by', 'edits_date', 'inform_status', 'inform_date', 'inform_by', 'inform_sale', 'cus_code'], 'required'],
            [['price', 'registry_tax_year'], 'number'],
            [['date_verify', 'date_verify_prb', 'call_date', 'date_add', 'mail_date', 'registry_date', 'get_car_date', 'import_date', 'admin_share', 'sup_share', 'approve_date', 'edits_date', 'inform_date'], 'safe'],
            [['team', 'operate_prb', 'emotion'], 'string', 'max' => 30],
            [['status', 'car_type', 'code', 'inform_status'], 'string', 'max' => 10],
            [['source', 'suggest_from', 'temp_remark'], 'string', 'max' => 100],
            [['color', 'car_number', 'engine_number', 'cus_code'], 'string', 'max' => 50],
            [['register_year'], 'string', 'max' => 4],
            [['call_remark'], 'string', 'max' => 255],
            [['operate', 'registry_tax_year_number', 'niti'], 'string', 'max' => 20],
            [['province_code', 'duplicated', 'approve'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'car_id' => 'Car ID',
            'insure_id' => 'Insure ID',
            'sale_id' => 'Sale ID',
            'team' => 'Team',
            'team_id' => 'Team ID',
            'status' => 'Status',
            'source' => 'Source',
            'car_type' => 'Car Type',
            'insure_customer_id' => 'Insure Customer ID',
            'customer_id' => 'Customer ID',
            'code' => 'Code',
            'car_model_id' => 'Car Model ID',
            'car_series_id' => 'Car Series ID',
            'color' => 'Color',
            'price' => 'Price',
            'car_number' => 'Car Number',
            'engine_number' => 'Engine Number',
            'register_year' => 'Register Year',
            'date_verify' => 'Date Verify',
            'date_verify_prb' => 'Date Verify Prb',
            'suggest_from' => 'Suggest From',
            'call_date' => 'Call Date',
            'call_remark' => 'Call Remark',
            'date_add' => 'Date Add',
            'order_id' => 'Order ID',
            'junk' => 'Junk',
            'company_id' => 'Company ID',
            'mail_date' => 'Mail Date',
            'operate' => 'Operate',
            'operate_prb' => 'Operate Prb',
            'buy_type' => 'Buy Type',
            'buy_company' => 'Buy Company',
            'registry_date' => 'Registry Date',
            'registry_tax_year' => 'Registry Tax Year',
            'registry_tax_year_number' => 'Registry Tax Year Number',
            'get_car_date' => 'Get Car Date',
            'insure_company_id' => 'Insure Company ID',
            'niti' => 'Niti',
            'info_type' => 'Info Type',
            'import' => 'Import',
            'checkc' => 'Checkc',
            'province_code' => 'Province Code',
            'insure_acard_id' => 'Insure Acard ID',
            'recent_insure_id' => 'Recent Insure ID',
            'import_date' => 'Import Date',
            'lock_sale' => 'Lock Sale',
            'admin_share' => 'Admin Share',
            'sup_share' => 'Sup Share',
            'fix_sale' => 'Fix Sale',
            'emotion' => 'Emotion',
            'duplicated' => 'Duplicated',
            'first_company_id' => 'First Company ID',
            'first_team_id' => 'First Team ID',
            'first_sale_id' => 'First Sale ID',
            'temp_sale_id' => 'Temp Sale ID',
            'temp_remark' => 'Temp Remark',
            'approve' => 'Approve',
            'approve_by' => 'Approve By',
            'approve_date' => 'Approve Date',
            'edits_by' => 'Edits By',
            'edits_date' => 'Edits Date',
            'inform_status' => 'Inform Status',
            'inform_date' => 'Inform Date',
            'inform_by' => 'Inform By',
            'inform_sale' => 'Inform Sale',
            'cus_code' => 'Cus Code',
        ];
    }
    public function getTInsureCustomer()
    {
        return $this->hasOne(TInsureCustomer01::className(), ['insure_customer_id' => 'insure_customer_id']);
    }

    public function getTCarModel()
    {
        return $this->hasOne(TCarModel::className(), ['car_model_id' => 'car_model_id']);
    }

    public function getTCarSeries()
    {
        return $this->hasOne(TCarSeries::className(), ['car_series_id' => 'car_series_id']);
    }

    public function getTCarType()
    {
        return $this->hasOne(TCarType::className(), ['car_type_id' => 'car_type']);
    }

    public function getTCarColor()
    {
        return $this->hasOne(TCarColor::className(), ['car_color_id' => 'color']);
    }
    
    public function getTFundCompany()
    {
        return $this->hasOne(TFundCompany::className(), ['fund_company_id' => 'buy_company']);
    }
}
