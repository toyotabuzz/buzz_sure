<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\base\NotSupportedException;

/**
 * This is the model class for table "t_member".
 *
 * @property integer $id
 * @property string $code
 * @property string $firstname
 * @property string $lastname
 * @property string $nickname
 * @property string $email
 * @property string $id_card
 * @property string $address
 * @property string $tel
 * @property integer $status
 * @property string $username
 * @property string $password write-only password
 * @property string $salt
 * @property string $department
 * @property string $position
 * @property integer $team
 * @property integer $level
 * @property string $picture
 * @property string $date_add
 * @property string $start_date
 * @property string $end_date
 * @property string $remark
 * @property string $default_program
 * @property integer $company_id
 * @property string $insure_license
 * @property string $insure_editor
 * @property integer $insure_status
 * @property string $phone_ext
 * @property string $emp_code
 */
class Tmember  extends ActiveRecord implements IdentityInterface
{
    const STATUS_INACTIVE  = 0;
    const STATUS_ACTIVE    = 1;
    const POS_SALE_MANAGER = 3;
    const POS_SALE_HEAD    = 2;
    const POS_SALE_EMP     = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_member';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_buzz_control');
    }

    public function behaviors()
    {
        return [
            // [
            //     'class' => TimestampBehavior::className(),
            //     'createdAtAttribute' => 'created_at',
            //     'updatedAtAttribute' => 'updated_at',
            //     'value' => date('Y-m-d H:i:s'),
            // ],
            // [
            //     'class' => BlameableBehavior::className(),
            //     'createdByAttribute' => 'created_by',
            //     'updatedByAttribute' => 'updated_by',
            //     'value' => empty(Yii::$app->user->identity->id) ? null : Yii::$app->user->identity->id,
            // ],
        ];
    } 

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username','emp_no'], 'required'],
            [['firstname', 'lastname','nickname','email'], 'trim'],
            [['team', 'company_id', 'insure_status'], 'integer'],
            [['status','date_add','start_date','end_date'], 'safe'],
            [['username', 'password','default_program','insure_editor'], 'string', 'max' => 20],
            [['id_card'], 'string', 'max' => 30],
            [['nickname', 'mobile_no'], 'string', 'max' => 50],
            [['password', 'new_password', 'confirm_password'], 'string', 'max' => 32],
            [['emp_no'], 'string', 'max' => 10],
            [['firstname', 'lastname','nickname','email','tel','salt','department','position','picture','insure_license'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 255],
            [['remark'], 'string'],
            [['username'], 'unique'],
            [['status'], 'default', 'value' => self::STATUS_ACTIVE]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'code'            => 'Code',
            'firstname'       => 'ชื่อ',
            'lastname'        => 'นามสกุล',
            'nickname'        => 'ชื่อเล่น',
            'email'           => 'อีเมล',
            'id_card'         => 'บัตรประชาชน',
            'address'         => 'ที่อยู่',
            'tel'             => 'เบอร์ต่อ',
            'status'          => 'สถานะ',
            'username'        => 'Username',
            'password'        => 'รหัสผ่านใหม่',
            'salt'            => 'Salt',
            'department'      => 'หน่วยงาน',
            'position'        => 'ตำแหน่ง',
            'team'            => 'ทีม',
            'level'           => 'ระดับ',
            'picture'         => 'รูปภาพ',
            'date_add'        => 'วันที่บันทึก',
            'start_date'      => 'Start Date',
            'end_date'        => 'End Date',
            'remark'          => 'หมายเหตุ',
            'default_program' => 'Default Program',
            'company_id'      => 'Company ID',
            'insure_license'  => 'Insure License',
            'insure_editor'   => 'Insure Editor',
            'insure_status'   => 'Insure Status',
            'phone_ext'       => 'Phone Ext',
            'emp_code'        => 'Employee No',
            
        ];
    }

    public function getTdepartment()
    {
        return $this->hasOne(Tdepartment::className(), ['department_id' => 'department']);
    }

    public function getTteam()
    {
        return $this->hasOne(Tteam::className(), ['team_id' => 'team']);
    }

    public function getTposition()
    {
        if($this->position == 1) return "กรรมการผู้จัดการ"; 
		if($this->position == 2) return "รองกรรมการผู้จัดการ"; 
		if($this->position == 3) return "ผู้จัดการ"; 
		if($this->position == 4) return "ผู้ช่วยผู้จัดการ"; 
		if($this->position == 5) return "ผู้จัดการสาขา"; 
		if($this->position == 6) return "หัวหน้าแผนก/ทีม"; 
		if($this->position == 7) return "พนักงานขาย"; 
		if($this->position == 8) return "พนักงานธุรการ"; 
    }

    public function getTcompany()
    {
        return $this->hasOne(Tcompany::className(), ['company_id' => 'company_id']);
    }
    
    public function getUserRole()
    {
        $arrRole = [];
        $model = TuuserRole::find()->select(['role_id'])->where(['user_id'=>$this->id])->andWhere(['isactive'=>'Y'])->asArray()->all();
        foreach ($model as $val) {
            array_push($arrRole, $val['role_id']);
        }
        return $arrRole;
        // return $this->hasMany(TuuserRole::className(), ['user_id' => 'id']);
    }
    /**
    **
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->attribute       = $this->attribute;            
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        $this->attribute       = $this->attribute;        
    }

    public function afterFind()
    {
        $this->attribute       = $this->attribute;

        parent::afterFind();
    }
    *
    */

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['member_id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        //return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return ($password == $this->password);
        // return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
