<?php

namespace common\components;

// use ___PHPSTORM_HELPERS\object;
use Yii;
use yii\base\Component;
use yii\data\ArrayDataProvider;

class Helpers extends Component
{
    // ฟังก์ชันแยกข้อความ error จาก Model
    public static function GetErrorModel($model, $separate = ', ', $runNum = false)
    {
        $arrError = [];
        $errorNum = count($model->getErrors()); // จำนวนข้อมูลที่ error ทั้งหมด
        $rowNum = 0;
        foreach ($model->getErrors() as $key => $value) {
            $arrError[] = (($errorNum > 1) ? (empty($runNum) ? '' : ++$rowNum . '. ') : '') . $value[0];
        }
        if (!empty($arrError)) {
            return implode($separate, $arrError);
        }
        return $arrError;
    }
    public static function listMonth($language = 'TH', $format = 'L', $placeholder = false, $toChart = false)
    {
        $arrMonth = array();
        if ($language == 'TH') {

            if ($format == 'L') {
                $arrMonth = array('1' => 'มกราคม', '2' => 'กุมภาพันธ์', '3' => 'มีนาคม', '4' => 'เมษายน', '5' => 'พฤษภาคม', '6' => 'มิถุนายน', '7' => 'กรกฎาคม', '8' => 'สิงหาคม', '9' => 'กันยายน', '10' => 'ตุลาคม', '11' => 'พฤศจิกายน', '12' => 'ธันวาคม');
            }

            if ($format == 'S') {
                $arrMonth = array('1' => 'ม.ค.', '2' => 'ก.พ.', '3' => 'มี.ค.', '4' => 'เม.ย.', '5'      => 'พ.ค.', '6' => 'มิ.ย.', '7' => 'ก.ค.', '8' => 'ส.ค.', '9' => 'ก.ย.', '10' => 'ต.ค.', '11' => 'พ.ย.', '12' => 'ธ.ค.');
            }
        }

        if ($language == 'EN') {

            if ($format == 'L') {
                $arrMonth = array('1' => 'January', '2' => 'February', '3' => 'March', '4' => 'April', '5' => 'May', '6' => 'June', '7' => 'July', '8' => 'August', '9' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
            }

            if ($format == 'S') {
                $arrMonth = array('1' => 'Jan', '2' => 'Feb', '3' => 'Mar', '4' => 'Apr', '5' => 'May', '6' => 'Jun', '7' => 'Jul', '8' => 'Aug', '9' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
            }
        }

        if ($placeholder) {
            $arrMonth = ['00' => 'ระบุเดือน'] + $arrMonth;
        }

        if ($toChart) {
            $j = 0;
            for ($i = 0; $i < 12; $i++) {
                $j = $i + 1;
                $arrToChart[$i] = $arrMonth[$j];
            }
            unset($arrMonth);
            $arrMonth = $arrToChart;
        }

        return $arrMonth;
    }
    public static function getDateTHDMY($Date, $symbol)
    {
        $arrMonthTH = Helpers::listMonth('TH', 'L', false);

        if (($Date != "") && (strlen(trim($Date)) >= 8)) {
            $subDate = explode($symbol, $Date);
            $day     = $subDate[0];
            $month   = $arrMonthTH[intval($subDate[1])];
            $year    = intval($subDate[2]) + 543;

            return $day . " " . $month . " " . $year;
        } else {
            return "-";
        }
    }
}
