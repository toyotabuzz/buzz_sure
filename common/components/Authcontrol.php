<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\data\ArrayDataProvider;

use common\models\Tumenu;
use common\models\Tumenusub;
use common\models\Tumenusubin;
use common\models\TuauthMainMenu;
use common\models\TuauthSubMenu;
use common\models\TuauthSubinMenu;
use common\models\TuuserRole;

class Authcontrol extends Component
{
    public function getMainMenu()
    {
        return Tumenu::find()
            ->where(['isactive' => 'Y'])
            ->orderBy(['menu_code' => SORT_ASC]);
    }

    public function getSubMenu($intMainMenuID)
    {
        return Tumenusub::find()
            ->where(['tumenu_id' => $intMainMenuID])
            ->andWhere(['isactive' => 'Y'])
            ->orderBy(['submenu_code' => SORT_ASC]);
    }

    public function getSubInMenu($intSubMenuID)
    {
        return Tumenusubin::find()
            ->where(['tumenusub_id' => $intSubMenuID])
            ->andWhere(['isactive' => 'Y'])
            ->orderBy(['subin_code' => SORT_ASC]);
    }

    public function getListAuthMainMenu()
    {
        return TuauthMainMenu::find()
            ->select([
                'department_id',
                'position_id',
                "GROUP_CONCAT(menu_id SEPARATOR ', ') AS listmenu"
            ])
            ->groupBy(['department_id', 'position_id']);
    }

    public function getManageAuthMainMenu($compTypeID, $depID, $roleID)
    {
        $result = TuauthMainMenu::find()
            ->select('department_id, position_id, menu_id')
            ->where(['comp_type_id' => $compTypeID])
            ->andWhere(['department_id' => $depID])
            // ->andWhere(['position_id' => $posID])
            ->andWhere(['role_id' => $roleID])
            ->asArray()
            ->all();
        return $result;
    }

    public function getListAuthSubMenu()
    {
        return TuauthSubMenu::find()
            ->select([
                'department_id',
                'position_id',
                "GROUP_CONCAT(menu_id SEPARATOR ', ') AS listmenu"
            ])
            ->groupBy(['department_id', 'position_id']);
    }

    public function getManageAuthSubMenu($compTypeID, $depID, $roleID)
    {
        $result = TuauthSubMenu::find()
            ->select('department_id, position_id, menu_id')
            ->where(['comp_type_id' => $compTypeID])
            ->andWhere(['department_id' => $depID])
            // ->andWhere(['position_id' => $posID])
            ->andWhere(['role_id' => $roleID])
            ->asArray()
            ->all();
        return $result;
    }

    public function getListAuthSubinMenu()
    {
        return TuauthSubinMenu::find()
            ->select([
                'department_id',
                'position_id',
                "GROUP_CONCAT(menu_id SEPARATOR ', ') AS listmenu"
            ])
            ->groupBy(['department_id', 'position_id']);
    }

    public function getManageAuthSubinMenu($compTypeID, $depID, $roleID)
    {
        $result = TuauthSubinMenu::find()
            ->select('department_id, position_id, menu_id')
            ->where(['comp_type_id' => $compTypeID])
            ->andWhere(['department_id' => $depID])
            // ->andWhere(['position_id' => $posID])
            ->andWhere(['role_id' => $roleID])
            ->asArray()
            ->all();
        return $result;
    }

    public function getListAuthUserMainMenu()
    {
        $query = Tumenu::find()
            ->rightJoin('tuauth_mainmenu', 'tuauth_mainmenu.menu_id = tumenu.id')
            //->where(['tuauth_mainmenu.comp_type_id' => Yii::$app->user->identity->company_type])
            ->where(['tuauth_mainmenu.department_id' => Yii::$app->user->identity->department])
            ->andWhere(['tuauth_mainmenu.role_id'=> Yii::$app->user->identity->userRole])
            ->andWhere(['isactive' => 'Y'])
            ->orderBy(['menu_code' => SORT_ASC]);
        $result = $query->all();
        //print_r(Yii::$app->user->identity->userRole);
        //echo '<pre>';print_r($query->createCommand()->getRawSql());echo'</pre>';exit();
        return $result;
    }

    public function getListAuthUserSubMenu($intMainMenuID)
    {
        $query = Tumenusub::find()
            ->rightJoin('tuauth_submenu', 'tuauth_submenu.menu_id = tumenusub.id')
            ->where(['tumenu_id' => $intMainMenuID])
            //->andWhere(['tuauth_submenu.comp_type_id' => Yii::$app->user->identity->company_type])
            ->andWhere(['tuauth_submenu.department_id' => Yii::$app->user->identity->department])
            ->andWhere(['tuauth_submenu.role_id'=> Yii::$app->user->identity->userRole])
            ->andWhere(['isactive' => 'Y'])
            ->orderBy(['submenu_code' => SORT_ASC]);
        $result = $query->all();
        //echo '<pre>';print_r($query->createCommand()->getRawSql());echo'</pre>';exit();
        return $result;
    }

    public function getListAuthUserSubinMenu($intSubMenuID)
    {
        $query = Tumenusubin::find()
            ->rightJoin('tuauth_subinmenu', 'tuauth_subinmenu.menu_id = tumenusubin.id')
            ->where(['tumenusub_id' => $intSubMenuID])
            //->andWhere(['tuauth_subinmenu.comp_type_id' => Yii::$app->user->identity->company_type])
            ->andWhere(['tuauth_subinmenu.department_id' => Yii::$app->user->identity->department])
            ->andWhere(['tuauth_subinmenu.role_id'=> Yii::$app->user->identity->userRole])
            ->andWhere(['isactive' => 'Y'])
            ->orderBy(['subin_code' => SORT_ASC]);
        $result = $query->all();
        // echo '<pre>';print_r($query->createCommand()->getRawSql());echo'</pre>';exit();
        return $result;
    }

    public function getActionMenu()
    {
        $result = [];
        $url = str_replace("/buzz_sure/backend/web","",Yii::$app->request->getUrl());
        if($url != '/'){
            $query = Tumenusubin::find()
                ->select(['tumenu.id AS menu_id','tumenusub.id AS menusub_id','tumenusubin.id AS menusubin_id'])
                ->rightJoin('tumenusub', 'tumenusub.id = tumenusubin.tumenusub_id')
                ->rightJoin('tumenu', 'tumenu.id = tumenusub.tumenu_id')
                ->andFilterWhere([ 'OR',
                    ['LIKE','tumenusubin.subin_url',$url],
                    ['LIKE','tumenusub.submenu_url', $url],
                    ['LIKE','tumenu.menu_url',$url]
                ])
                ->orderBy(['subin_code' => SORT_ASC]);
            $result = $query->asArray()->one();
        }
        //echo '<pre>';print_r($query->createCommand()->getRawSql());echo'</pre>';exit();
        return $result;
    }
}
