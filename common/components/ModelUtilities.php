<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use common\models\Turef;
use common\models\Turun;

class ModelUtilities extends Component
{
    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];

        foreach ($datas as $value) {
            if (trim($value->{$fieldId}) != '') {
                $obj[$value->{$fieldId}] = $value->{$fieldName};
            }
        }

        return $obj;
    }

    public function GetErrorModel($model)
    {
        $strError = '';
        foreach ($model->getErrors() as $key => $value) {
            $strError .= $value[0] . ', ';
        }
        return $strError;
    }
    public function getItemReferance($ref_type, $ref_code, $isactive = null)
    {
        return Turef::find()
            ->where(['ref_type' => $ref_type])
            ->andWhere(['ref_code' => $ref_code])
            ->andFilterWhere(['isactive' => $isactive])
            ->orderBy('ref_seq ASC');
    }
    
    public function getListReferance($ref_type, $ref_code, $bolNoSelect = true, $isactive = null)
    {
        $arrData = ArrayHelper::map($this->getItemReferance($ref_type, $ref_code, $isactive)->asArray()->all(), 'ref_value', 'ref_desc');
        if ($bolNoSelect) {
            $arrayData = ['' => 'ระบุ..'] + $arrData;
        } else {
            $arrayData = $arrData;
        }
        return $arrayData;
    }

    public function getDescTuref($ref_type, $ref_code, $ref_value)
    {
        $model = Turef::find()
            ->where(['ref_type' => $ref_type])
            ->andWhere(['ref_code' => $ref_code])
            ->andWhere(['ref_value' => $ref_value])
            ->one();
        if (!empty($model)) {
            return $model->ref_desc;
        } else {
            return '';
        }
    }
    public function getValueTurun($run_type, $run_code)
    {
        $model = Turun::find()->where('run_type = "' . $run_type . '" and run_code = "' . $run_code . '" FOR UPDATE')->one();
        return $model;
    }

    public function getLateNumberTurun($run_type, $run_code)
    {
        $modelRun =  $this->getValueTurun($run_type, $run_code);

        if (empty($modelRun)) {
            $modelNew           = new Turun();
            $modelNew->run_type = $run_type;
            $modelNew->run_code = $run_code;
            $modelNew->run_no   = 1;
            $modelNew->save();
            return 1;
        } else {
            $intCurent        = (intval($modelRun->run_no) + 1);
            $modelRun->run_no = $intCurent;
            $modelRun->save();

            return $intCurent;
        }
    }

}
