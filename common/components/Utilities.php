<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

class Utilities extends Component
{
    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];

        foreach ($datas as $value) {
            if (trim($value->{$fieldId}) != '') {
                $obj[$value->{$fieldId}] = $value->{$fieldName};
            }
        }

        return $obj;
    }

    public function GetErrorModel($model)
    {
        $strError = '';
        foreach ($model->getErrors() as $key => $value) {
            $strError .= $value[0] . ', ';
        }
        return $strError;
    }

}
